package com.mediatek.rcs.messageservice.cloudbackup.modules;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;

import com.mediatek.rcs.messageservice.cloudbackup.utils.FileUtils;
import com.mediatek.rcs.messageservice.cloudbackup.utils.CloudBrUtils;
import com.mediatek.rcs.messageservice.cloudbackup.utils.CloudBrUtils.BackupConstant;
import com.mediatek.rcs.messageservice.cloudbackup.utils.CloudBrUtils.BackupDataFileType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * MessageRestoreComposer restore cloud data interface.
 */
public class MessageRestoreComposer {
    private static final String CLASS_TAG = CloudBrUtils.MODULE_TAG + "MessageRestoreComposer";

    private Context mContext;
    private ContentResolver mContentResolver;
    private ChatGroupDecomposer mChatGroupDecomposer;
    private FtMsgDecomposer mFtMsgDecomposer;
    private Chat1To1Decomposer mChat1To1Decomposer;
    private SmsDecomposer mSmsDecomposer;
    private boolean mCancel = false;
    private File mBackupDataFolder;

    /**
     * @param context.
     * @param backupDataFolder file folder put restore data.
     */
    public MessageRestoreComposer(Context context, File backupDataFolder) {
        mContext = context;
        mBackupDataFolder = backupDataFolder;
    }

    /**
     *
     * @return restore result, 0 is ok.
     */
    public int restoreData() {
        if (!createContentResolver()) {
            Log.e(CLASS_TAG, "createContentResolver error");
            return CloudBrUtils.ResultCode.BACKUP_FILE_ERROR;
        }
        if (!mBackupDataFolder.exists()) {
            Log.d(CLASS_TAG, "no file exited!");
            return CloudBrUtils.ResultCode.BACKUP_FILE_ERROR;
        }

       return restoreData(mBackupDataFolder);
    }

    private boolean createContentResolver() {
        if (mContentResolver != null) {
            Log.d(CLASS_TAG, "createContentResolver() Resolver exit!");
            return true;
        }
        if (mContext == null) {
            Log.e(CLASS_TAG, "cacheData mContext = null, return");
            return false;
        }
        mContentResolver = mContext.getContentResolver();
        if (mContentResolver == null) {
            Log.e(CLASS_TAG, "cacheData mContentResolver = null, return");
            return false;
        }
        return true;
    }

    protected int restoreData(File backupDataFolder) {
        Log.d(CLASS_TAG, "restoreData backupDataFolder = " + backupDataFolder.getAbsolutePath());
        int result = CloudBrUtils.ResultCode.OK;
        for (File file : backupDataFolder.listFiles()) {
            if (!file.isDirectory()) {
                Log.d(CLASS_TAG, "file name = " + file.getName());
                result = parserMsgFromFile(file);
                if (result != CloudBrUtils.ResultCode.OK) {
                    return result;
                }
            } else {
                Log.d(CLASS_TAG, "folder name = " + file.getName());
                result = restoreData(file);
                if (result != CloudBrUtils.ResultCode.OK) {
                    return result;
                }
            }
        }
        return CloudBrUtils.ResultCode.OK;
    }

    /**
     * This method will be called when restore service be cancel.
     *
     * @param cancel.
     */
    public void setCancel(boolean cancel) {
        mCancel = cancel;
        if (mFtMsgDecomposer != null) {
            mFtMsgDecomposer.setCancel(cancel);
        }

        if (mChatGroupDecomposer != null) {
            mChatGroupDecomposer.setCancel(cancel);
        }

        if (mChat1To1Decomposer != null) {
            mChat1To1Decomposer.setCancel(cancel);
        }

        if (mSmsDecomposer != null) {
            mSmsDecomposer.setCancel(cancel);
        }
    }

    /**
     * Get file type(ft/group chat/1-n msg), and call corresponding class to
     * parse the file.
     *
     * @param file
     *            backup file that get from server.
     * @return parse file result.
     */
    protected int parserMsgFromFile(File file) {
        Log.d(CLASS_TAG, "parserMsgFromFile filename  = " + file.getName());
        int result = CloudBrUtils.ResultCode.OK;
        String filePath = file.getAbsolutePath();
        Log.d(CLASS_TAG, "filePath = " + filePath);

        int fileType = FileUtils.anysisFileType(file);
        Log.d(CLASS_TAG, "parserMsgFromFile fileType = " + fileType);
        if (fileType == -1) {
            Log.e(CLASS_TAG, "anysisFileType error");
            return CloudBrUtils.ResultCode.OTHER_EXCEPTION;
        }

        if (fileType == BackupDataFileType.VMSG) {
            Log.d(CLASS_TAG, "parserMsgFromFile is a sms vmsg");
            if (mSmsDecomposer == null) {
                mSmsDecomposer = new SmsDecomposer(mContext);
            }
            result = mSmsDecomposer.retoreData(file);
            mSmsDecomposer = null;
            return result;
        } else if (fileType == BackupDataFileType.MMS_XML) {
            Log.d(CLASS_TAG, "this file is a mms xml, skip");
            return CloudBrUtils.ResultCode.OK;
        } else if (fileType == BackupDataFileType.PDU) {
            Log.d(CLASS_TAG, "parserMsgFromFile is pdu, skip");
            return result;
        }

        Log.d(CLASS_TAG, "parserMsgFromFile is ipmsg");
        InputStream instream = null;
        try {
            instream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return CloudBrUtils.ResultCode.OTHER_EXCEPTION;
        }

        Log.d(CLASS_TAG, "getMsgTypeFromFile file = " + file);
        InputStreamReader inreader = new InputStreamReader(instream);
        BufferedReader buffreader = new BufferedReader(inreader);
        String line = null;
        String content = null;
        try {
            while (((line = buffreader.readLine()) != null)) {
                if (!(line.startsWith("Content-type") || line.startsWith("Content-Type"))) {
                    Log.d(CLASS_TAG, "parserMsgFromFile line = " + line);
                    continue;
                } else if (line.equals(BackupConstant.BOUNDDARY_CPM)) {
                    Log.e(CLASS_TAG, "not found content_type, return error");
                    return CloudBrUtils.ResultCode.BACKUP_FILE_ERROR;
                } else {
                    Log.d(CLASS_TAG, "file " + file.getName() + " type = " + line);
                    content = line.substring(line.indexOf(BackupConstant.SEPRATOR) + 1).trim();
                    break;
                }
            }
            if (buffreader != null) {
                buffreader.close();
            }
            if (content == null) {
                Log.d(CLASS_TAG, "getMsgTypeFromFile not found content_type, error");
                return CloudBrUtils.ResultCode.OTHER_EXCEPTION;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return CloudBrUtils.ResultCode.IO_EXCEPTION;
        }

        if (content.equals(CloudBrUtils.ContentType.GROUP_CHAT_TYPE)) {
            Log.d(CLASS_TAG, "group chat");
            if (mChatGroupDecomposer == null) {
                mChatGroupDecomposer = new ChatGroupDecomposer(mContext);
            }
            return mChatGroupDecomposer.parseGroupMsg(file);
        } else if (content.equals(CloudBrUtils.ContentType.GROUP_FT_TYPE)) {
            Log.d(CLASS_TAG, "ft msg");
            if (mFtMsgDecomposer == null) {
                mFtMsgDecomposer = new FtMsgDecomposer(mContext);
            }
            return mFtMsgDecomposer.parseFtMsg(file);
        } else {
            Log.d(CLASS_TAG, "is a 1 to 1 text msg content = " + content);
            if (mChat1To1Decomposer == null) {
                mChat1To1Decomposer = new Chat1To1Decomposer(mContext);
            }
            return mChat1To1Decomposer.parseOneToOneMsg(file);
        }
    }
}
