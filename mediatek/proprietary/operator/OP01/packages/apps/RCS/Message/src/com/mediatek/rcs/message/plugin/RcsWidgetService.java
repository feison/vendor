package com.mediatek.rcs.message.plugin;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.mediatek.mms.ipmessage.DefaultIpWidgetServiceExt;
import com.mediatek.mms.ipmessage.IIpConversationExt;
import com.mediatek.rcs.common.provider.GroupChatCache;
import com.mediatek.rcs.common.provider.GroupChatCache.ChatInfo;

/**
 * class RcsWidgetService, plugin implements response MmsWidgetService.java.
 *
 */
public class RcsWidgetService extends DefaultIpWidgetServiceExt {
    private static final String TAG = "RcsMessage/RcsWidgetService";

    @Override
    public Cursor queryAllConversations(Context context, Uri uri, String[] projections) {
        Log.d(TAG, "queryAllConversations");
        return context.getContentResolver().query(RcsConversation.sRCSAllThreadsUri,
                        RcsConversation.sRCSThreadsProjections, null, null, null);
    }

    @Override
    public String getViewAt(IIpConversationExt conv) {
        String from = null;
        RcsConversation conversation = (RcsConversation) conv;
        if (conversation.mIsGroup && !TextUtils.isEmpty(conversation.mGroupChatId)) {
            ChatInfo info = GroupChatCache.getInstance().getInfoByChatId(conversation.mGroupChatId);
            if (info != null) {
                from = info.getNickName();
                Log.d(TAG, "group's nickName: " + from);
                if (TextUtils.isEmpty(from)) {
                    from = info.getSubject();
                }
            }
        }
        Log.d(TAG, "getViewAt: isGroup: " + conversation.mIsGroup +
                ", group id = " + conversation.mGroupChatId + ", from = " + from);
        return from;
    }
}
