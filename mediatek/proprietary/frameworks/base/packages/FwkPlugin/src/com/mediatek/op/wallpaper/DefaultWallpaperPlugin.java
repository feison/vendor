package com.mediatek.op.wallpaper;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.mediatek.common.PluginImpl;
import com.mediatek.common.wallpaper.IWallpaperPlugin;
import android.content.res.AssetManager;
import java.io.File;
import java.io.IOException;
import android.util.DisplayMetrics;
import android.content.res.Configuration;
/**
 * This is the default implementation of low storage operator plugin.
 */

@PluginImpl(interfaceName = "com.mediatek.common.wallpaper.IWallpaperPlugin")
public class DefaultWallpaperPlugin implements IWallpaperPlugin {

    private static final String TAG = "DefaultWallpaperPlugin";
    private final String CUSTOM_RES_APK_PATH="/custom/app-res/";
    private Resources mCustomSystemRes = null;
    private AssetManager mCustomSystemAsset = null;
    /**
     * @return Return the resources object of plug-in package.
     */
    public Resources getPluginResources(Context context) {
               Log.i("@M_" + TAG, "getPluginResources..");
		String resApkPathName ="android-res.apk";
		resApkPathName = CUSTOM_RES_APK_PATH+"android-res/"+resApkPathName;

		File file = new File(resApkPathName);
		boolean mCustSystemResExist = file.exists();
		if(!mCustSystemResExist)
		{
			Log.v(TAG, "===>file path:"+resApkPathName+ " not exist");
			return null;
		}
			
		if (mCustomSystemRes==null)
    		{
    			mCustomSystemAsset = new AssetManager();
			int result  = mCustomSystemAsset.addAssetPath(resApkPathName);
			Log.v(TAG, "=======>add "+resApkPathName+" resutl:"+result);
			mCustomSystemRes = new Resources(mCustomSystemAsset,new DisplayMetrics(),new Configuration());			
    		}
        return mCustomSystemRes;
    }

    /**
     * @return Return res id of default wallpaper resource.
     */
    public int getPluginDefaultImage() {        
	int resId = 0;
	if(mCustomSystemRes != null)
         	resId = mCustomSystemRes.getIdentifier("default_wallpaper", "drawable", "com.android.custom");
        Log.i("@M_" + TAG, "getPluginDefaultImage..resId: "+resId);
        return resId ;
    }

}
