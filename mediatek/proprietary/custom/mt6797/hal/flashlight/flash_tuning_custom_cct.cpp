#define LOG_TAG "flash_tuning_custom_cct.cpp"
#define MTK_LOG_ENABLE 1
#include "string.h"
#include "camera_custom_nvram.h"
#include "camera_custom_types.h"
#include "camera_custom_AEPlinetable.h"
#include <cutils/log.h>
#include "flash_feature.h"
#include "flash_param.h"
#include "flash_tuning_custom.h"
#include <kd_camera_feature.h>

//==============================================================================
//
//==============================================================================
int cust_fillDefaultStrobeNVRam_main (void* data)
{
    int i;
    NVRAM_CAMERA_STROBE_STRUCT* p;
    p = (NVRAM_CAMERA_STROBE_STRUCT*)data;

    static short engTab[]=
    {
          -1, 867,1692,2207,2966,3692,4150,4819,5465,6084,6498,7069,7438,7999,8521,9042,9363,
         718,1664,2504,3028,3801,4542,5010,5704,6361,6994,7404,8006,8386,8955,9517,  -1,  -1,
        1366,2349,3188,3705,4472,5211,5675,6366,7006,7650,8055,8668,9029,9605,  -1,  -1,  -1,
        1762,2771,3604,4122,4883,5621,6080,6759,7416,8053,8450,9043,9417,  -1,  -1,  -1,  -1,
        2337,3383,4208,4723,5483,6208,6670,7343,7995,8640,9022,9627,9999,  -1,  -1,  -1,  -1,
        2879,3958,4780,5292,6050,6779,7231,7921,8552,9181,9575,  -1,  -1,  -1,  -1,  -1,  -1,
        3220,4330,5144,5653,6403,7135,7584,8258,8903,9526,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        3707,4841,5660,6166,6914,7644,8094,8750,9415,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        4165,5343,6160,6655,7401,8123,8579,9241,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        4602,5818,6620,7129,7881,8592,9032,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        4881,6114,6918,7426,8172,8882,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        5277,6552,7350,7852,8592,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        5528,6828,7651,8139,8860,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        5905,7241,8010,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        6244,7655,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        6582,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
        6787,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,  -1,
    };
    //version
    p->u4Version = NVRAM_CAMERA_STROBE_FILE_VERSION;
    //eng tab
    memcpy(p->engTab.yTab, engTab, sizeof(engTab));

    //tuningPara[8];
    for(i=0;i<8;i++)
    {
        p->tuningPara[i].yTarget = 188;
        p->tuningPara[i].fgWIncreaseLevelbySize = 10;
        p->tuningPara[i].fgWIncreaseLevelbyRef = 5;//0;
        p->tuningPara[i].ambientRefAccuracyRatio = 5;
        p->tuningPara[i].flashRefAccuracyRatio = 0;//1;
        p->tuningPara[i].backlightAccuracyRatio = 18;
        p->tuningPara[i].backlightUnderY = 40;
        p->tuningPara[i].backlightWeakRefRatio = 32;
        p->tuningPara[i].safetyExp =66644;//33322;
        p->tuningPara[i].maxUsableISO = 1200;//680;
        p->tuningPara[i].yTargetWeight = 0;
        p->tuningPara[i].lowReflectanceThreshold = 13;
        p->tuningPara[i].flashReflectanceWeight = 0;
        p->tuningPara[i].bgSuppressMaxDecreaseEV = 20;
        p->tuningPara[i].bgSuppressMaxOverExpRatio = 6;
        p->tuningPara[i].fgEnhanceMaxIncreaseEV = 50;
        p->tuningPara[i].fgEnhanceMaxOverExpRatio = 2;//6;
        p->tuningPara[i].isFollowCapPline = 1;
        p->tuningPara[i].histStretchMaxFgYTarget = 300;//285;//266;
        p->tuningPara[i].histStretchBrightestYTarget = 480;//404;//328;
        p->tuningPara[i].fgSizeShiftRatio = 0;
        p->tuningPara[i].backlitPreflashTriggerLV = 90;
        p->tuningPara[i].backlitMinYTarget = 90;//100;
        p->tuningPara[i].minstameanpass = 80;
    }

    p->tuningPara[0].isFollowCapPline = 0;

    p->paraIdxForceOn[0] =1;    //default
    p->paraIdxForceOn[1] =0;    //LIB3A_AE_SCENE_OFF
    p->paraIdxForceOn[2] =0;    //LIB3A_AE_SCENE_AUTO
    p->paraIdxForceOn[3] =1;    //LIB3A_AE_SCENE_NIGHT
    p->paraIdxForceOn[4] =1;    //LIB3A_AE_SCENE_ACTION
    p->paraIdxForceOn[5] =1;    //LIB3A_AE_SCENE_BEACH
    p->paraIdxForceOn[6] =1;    //LIB3A_AE_SCENE_CANDLELIGHT
    p->paraIdxForceOn[7] =1;    //LIB3A_AE_SCENE_FIREWORKS
    p->paraIdxForceOn[8] =1;    //LIB3A_AE_SCENE_LANDSCAPE
    p->paraIdxForceOn[9] =1;    //LIB3A_AE_SCENE_PORTRAIT
    p->paraIdxForceOn[10] =1;   //LIB3A_AE_SCENE_NIGHT_PORTRAIT
    p->paraIdxForceOn[11] =1;   //LIB3A_AE_SCENE_PARTY
    p->paraIdxForceOn[12] =1;   //LIB3A_AE_SCENE_SNOW
    p->paraIdxForceOn[13] =1;   //LIB3A_AE_SCENE_SPORTS
    p->paraIdxForceOn[14] =1;   //LIB3A_AE_SCENE_STEADYPHOTO
    p->paraIdxForceOn[15] =1;   //LIB3A_AE_SCENE_SUNSET
    p->paraIdxForceOn[16] =1;   //LIB3A_AE_SCENE_THEATRE
    p->paraIdxForceOn[17] =1;   //LIB3A_AE_SCENE_ISO_ANTI_SHAKE
    p->paraIdxForceOn[18] =1;   //LIB3A_AE_SCENE_BACKLIGHT

    p->paraIdxAuto[0] =1;  //default
    p->paraIdxAuto[1] =0;  //LIB3A_AE_SCENE_OFF
    p->paraIdxAuto[2] =0;  //LIB3A_AE_SCENE_AUTO
    p->paraIdxAuto[3] =1;  //LIB3A_AE_SCENE_NIGHT
    p->paraIdxAuto[4] =1;  //LIB3A_AE_SCENE_ACTION
    p->paraIdxAuto[5] =1;  //LIB3A_AE_SCENE_BEACH
    p->paraIdxAuto[6] =1;  //LIB3A_AE_SCENE_CANDLELIGHT
    p->paraIdxAuto[7] =1;  //LIB3A_AE_SCENE_FIREWORKS
    p->paraIdxAuto[8] =1;  //LIB3A_AE_SCENE_LANDSCAPE
    p->paraIdxAuto[9] =1;  //LIB3A_AE_SCENE_PORTRAIT
    p->paraIdxAuto[10] =1; //LIB3A_AE_SCENE_NIGHT_PORTRAIT
    p->paraIdxAuto[11] =1; //LIB3A_AE_SCENE_PARTY
    p->paraIdxAuto[12] =1; //LIB3A_AE_SCENE_SNOW
    p->paraIdxAuto[13] =1; //LIB3A_AE_SCENE_SPORTS
    p->paraIdxAuto[14] =1; //LIB3A_AE_SCENE_STEADYPHOTO
    p->paraIdxAuto[15] =1; //LIB3A_AE_SCENE_SUNSET
    p->paraIdxAuto[16] =1; //LIB3A_AE_SCENE_THEATRE
    p->paraIdxAuto[17] =1; //LIB3A_AE_SCENE_ISO_ANTI_SHAKE
    p->paraIdxAuto[18] =1; //LIB3A_AE_SCENE_BACKLIGHT



    //--------------------
    //eng level
    //index mode
    //torch
    p->engLevel.torchDuty = 1;
    //af
    p->engLevel.afDuty = 0;
    //pf, mf, normal
    p->engLevel.pfDuty = 0;
    p->engLevel.mfDutyMax = 15;//-1到15共17段
    p->engLevel.mfDutyMin = -1;
    //low bat
    p->engLevel.IChangeByVBatEn=0;
    p->engLevel.vBatL = 3600;//mv
    p->engLevel.pfDutyL = 0;
    p->engLevel.mfDutyMaxL = 1;//低电时都是不能打闪的
    p->engLevel.mfDutyMinL = -1;
    //burst setting
    p->engLevel.IChangeByBurstEn=1;
    p->engLevel.pfDutyB = 0;
    p->engLevel.mfDutyMaxB = 1;//连拍时可能会灭灯
    p->engLevel.mfDutyMinB = -1;
    //high current setting
    p->engLevel.decSysIAtHighEn = 1;
    p->engLevel.dutyH = 10;

    //LT
    p->engLevelLT.torchDuty = 1;
    //af
    p->engLevelLT.afDuty = 0;
    //pf, mf, normal
    p->engLevelLT.pfDuty = 0;
    p->engLevelLT.mfDutyMax = 15;
    p->engLevelLT.mfDutyMin = -1;
    //low bat
    p->engLevelLT.pfDutyL = 0;
    p->engLevelLT.mfDutyMaxL = 1;
    p->engLevelLT.mfDutyMinL = -1;
    //burst setting
    p->engLevelLT.pfDutyB = 0;
    p->engLevelLT.mfDutyMaxB = 1;
    p->engLevelLT.mfDutyMinB = -1;

	//for copyTuningParaDualFlash
    p->dualTuningPara.toleranceEV_pos = 30; //0.1 EV
    p->dualTuningPara.toleranceEV_neg = 30; //0.1 EV 
    p->dualTuningPara.XYWeighting = 64;  //0.5  , 128 base
    p->dualTuningPara.useAwbPreferenceGain = 1; //the same with environment lighting condition	
    p->dualTuningPara.envOffsetIndex[0] = -200;
    p->dualTuningPara.envOffsetIndex[1] = -100;
    p->dualTuningPara.envOffsetIndex[2] = 50;
    p->dualTuningPara.envOffsetIndex[3] = 150;
    p->dualTuningPara.envXrOffsetValue[0] = 0;
    p->dualTuningPara.envXrOffsetValue[1] = 0;
    p->dualTuningPara.envXrOffsetValue[2] = 0;
    p->dualTuningPara.envXrOffsetValue[3] = 0;
    p->dualTuningPara.envYrOffsetValue[0] = 0;
    p->dualTuningPara.envYrOffsetValue[1] = 0;
    p->dualTuningPara.envYrOffsetValue[2] = 0;
    p->dualTuningPara.envYrOffsetValue[3] = 0;
    p->dualTuningPara.VarianceTolerance = 1;
    p->dualTuningPara.ChooseColdOrWarm = FLASH_CHOOSE_COLD;
	
    return 0;
}

int cust_fillDefaultFlashCalibrationNVRam_main (void* data)
{

    NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT* d;
    d = (NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT*)data;

    static short engTab[]=
         {1012,1922,2745,3495,4873,6114,7242,8280,9202,9999};
    memcpy(d->yTab, engTab, sizeof(engTab));


    //d->flashWBGain

    return 0;
}
