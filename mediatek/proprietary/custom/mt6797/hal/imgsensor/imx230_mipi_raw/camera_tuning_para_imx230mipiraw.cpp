#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>
#include <cutils/log.h>
#include <string.h>
#include "camera_custom_nvram.h"
#include "camera_custom_sensor.h"
#include "image_sensor.h"
#include "kd_imgsensor_define.h"
#include "camera_AE_PLineTable_imx230mipiraw.h"
#include "camera_info_imx230mipiraw.h"
#include "camera_custom_AEPlinetable.h"
#include "camera_custom_tsf_tbl.h"

#define NVRAM_TUNING_PARAM_NUM  5341001

const NVRAM_CAMERA_ISP_PARAM_STRUCT CAMERA_ISP_DEFAULT_VALUE =
{{
    //Version
    Version: NVRAM_CAMERA_PARA_FILE_VERSION,

    //SensorId
    SensorId: SENSOR_ID,
    ISPComm:{
      {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      }
    },
    ISPPca: {
#include INCLUDE_FILENAME_ISP_PCA_PARAM
    },
    ISPRegs:{
#include INCLUDE_FILENAME_ISP_REGS_PARAM
    },
    ISPMulitCCM:{
      Poly22:{
        80780,      // i4R_AVG
        25843,      // i4R_STD
        122900,      // i4B_AVG
        33940,      // i4B_STD
				  0, // i4R_MAX
				   0, // i4R_MIN
				  0, // i4G_MAX
				   0, // i4G_MIN
				  0, // i4B_MAX
				   0, // i4B_MIN
                { // i4P00[9]
            8606000, -3412000, -74000, -1132000, 6090000, 164000, -116000, -4652000, 9886000
                },
                { // i4P10[9]
            2656391, -3010410, 345737, -198526, -193661, 360157, 199958, 1173328, -1379201
                },
                { // i4P01[9]
            2246274, -2391654, 135481, -495429, -429240, 896398, -216730, -966805, 1182703
                },
                { // i4P20[9]
                0, 0, 0, 0, 0, 0, 0, 0, 0
                },
                { // i4P11[9]
                0, 0, 0, 0, 0, 0, 0, 0, 0
                },
                { // i4P02[9]
                0, 0, 0, 0, 0, 0, 0, 0, 0
                }

      },
      AWBGain:{
        // Strobe
        {
          512,    // i4R
          512,    // i4G
          512    // i4B
        },
        // A
        {
          603,    // i4R
            512,    // i4G
          1406    // i4B
        },
        // TL84
        {
          855,    // i4R
          512,    // i4G
          1082    // i4B
        },
        // CWF
        {
          974,    // i4R
          512,    // i4G
          1186    // i4B
        },
        // D65
        {
          1116,    // i4R
          512,    // i4G
          785    // i4B
        },
        // Reserved 1
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        },
        // Reserved 2
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        },
        // Reserved 3
        {
            512,    // i4R
            512,    // i4G
            512    // i4B
        }
      },
      Weight:{
        1, // Strobe
        1, // A
        1, // TL84
        1, // CWF
        1, // D65
        1, // Reserved 1
        1, // Reserved 2
        1  // Reserved 3
      }
    },

          MDPMulitCCM:{
                Poly22:{
                  125225, // i4R_AVG
                   32141, // i4R_STD
                  165675, // i4B_AVG
                   30249, // i4B_STD
                     646, // i4R_MAX
                     527, // i4R_MIN
                     789, // i4G_MAX
                     670, // i4G_MIN
                     856, // i4B_MAX
                     721, // i4B_MIN
                  {  // i4P00[9]
                     5995000,   -505000,   -370000,  -1387500,   7165000,   -657500,   -532500,  -2045000,   7697500
                  },
                  {  // i4P10[9]
                      725905,  -1081879,    355974,    627096,   -389018,   -238079,    626246,    -86040,   -540207
                  },
                  {  // i4P01[9]
                      257857,   -576218,    318361,    -96902,   -138571,    235473,     24959,    193148,   -218107
                  },
                  {  // i4P20[9]
                      0, 0, 0, 0, 0, 0, 0, 0, 0
                          },
                          { // i4P11[9]
                      0, 0, 0, 0, 0, 0, 0, 0, 0
                          },
                          { // i4P02[9]
                      0, 0, 0, 0, 0, 0, 0, 0, 0
                          }

                },
                AWBGain:{
                  // Strobe
                  {
                    1016,    // i4R
                    512,    // i4G
                    1101    // i4B
                  },
                  // A
                  {
                    878,    // i4R
                      512,    // i4G
                    1971    // i4B
                  },
                  // TL84
                  {
                    1192,    // i4R
                      512,    // i4G
                    1628    // i4B
                  },
                  // CWF
                  {
                    1280,    // i4R
                      512,    // i4G
                    1773    // i4B
                  },
                  // D65
                  {
                    1659,    // i4R
                      512,    // i4G
                    1255    // i4B
                  },
                  // Reserved 1
                  {
                      512,    // i4R
                      512,    // i4G
                      512    // i4B
                  },
                  // Reserved 2
                  {
                      512,    // i4R
                      512,    // i4G
                      512    // i4B
                  },
                  // Reserved 3
                  {
                      512,    // i4R
                      512,    // i4G
                      512    // i4B
                  }
                },
                Weight:{
                  1, // Strobe
                  1, // A
                  1, // TL84
                  1, // CWF
                  1, // D65
                  1, // Reserved 1
                  1, // Reserved 2
                  1  // Reserved 3
                }
              },
    isp_ccm_ratio: 1.000000,
    //bInvokeSmoothCCM
    bInvokeSmoothCCM: MTRUE,
    DngMetadata:{
        0,  //i4RefereceIlluminant1
        3,  //i4RefereceIlluminant2
      rNoiseProfile:{
        {
          S:{
            0.000004,      // a
            0.000336       // b
          },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
            },
            {
          S:{
            0.000004,      // a
            0.000336       // b
            },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        },
            {
          S:{
            0.000004,      // a
            0.000336       // b
            },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        },
            {
          S:{
            0.000004,      // a
            0.000336       // b
            },
          O:{
            0.000000,      // a
            -0.000234       // b
          }
        }
      }
    },
    rGmaParam:
    {
        {   // Normal Mode
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0,  0,  10,  20,  40,  60,  80,  80,  80,  80
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100
                }
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
            }
        },
        {   // HDR Mode
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0, 33,  66,  100, 100, 100,  100, 100, 100, 100
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0, 33,  66, 100, 100, 100, 100, 100, 100, 100, 100, 100
                }
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
            }
        },
        {   // Reserve 0
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0, 33,  66,  100, 100, 100,  100, 100, 100, 100
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0, 33,  66, 100, 100, 100, 100, 100, 100, 100, 100, 100
                }
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
            }
        },
        {   // Reserve 1
            eISP_DYNAMIC_GMA_MODE,  // eGMAMode
            8,                  // i4LowContrastThr
            {
                {   // i4ContrastWeightingTbl
                    //  0   1   2    3    4    5    6    7    8    9    10
                        0,  0, 33,  66,  100, 100, 100,  100, 100, 100, 100
                },
                {   // i4LVWeightingTbl
                    //LV0   1   2   3   4   5   6   7   8   9   10   11   12   13   14   15   16   17   18   19
                        0,  0,  0,  0,  0,  0,  0,  0,  0, 33,  66, 100, 100, 100, 100, 100, 100, 100, 100, 100
                }
            },
            {
                1,      // i4Enable
                1,      // i4WaitAEStable
                4       // i4Speed
            },
            {
                0,      // i4Enable
                2047,   // i4CenterPt
                50,     // i4LowPercent
                100000, // i4LowCurve100
                100000, // i4HighCurve100
                50,     // i4HighPercent
                100,    // i4SlopeH100
                100     // i4SlopeL100
            },
            {
                0       // rGMAFlare.i4Enable
            }
        }
    },
    rLceParam:
    {
        //   Normal Mode
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                2,  //  i4LCEBa
                29, //  i4LCEPa
                31  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  0 * N
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  1
                        {36,  36,  36,  36,  36,  36,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  2
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  3
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   36,   36,   36,   36,   36,   36},   //  4
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  5
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  6
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  7
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  8
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  9
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36}    // 10
                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  0 * N
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  1
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  2
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  3
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   31,   47,   47,   47,   47,   47,   47},   //  4
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  5
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  6
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  7
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  8
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  9
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47}    // 10
                    }
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
                    0   // i4Enable
                }
            }
        },
        //   HDR Mode
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                2,  //  i4LCEBa
                29, //  i4LCEPa
                31  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  0 * N
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  1
                        {36,  36,  36,  36,  36,  36,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  2
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  3
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   36,   36,   36,   36,   36,   36},   //  4
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  5
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  6
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  7
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  8
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  9
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36}    // 10
                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  0 * N
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  1
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  2
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  3
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   31,   47,   47,   47,   47,   47,   47},   //  4
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  5
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  6
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  7
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  8
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  9
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47}    // 10
                    }
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
                    0   // i4Enable
                }
            }
        },
        //   Reserve0
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                2,  //  i4LCEBa
                29, //  i4LCEPa
                31  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  0 * N
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  1
                        {36,  36,  36,  36,  36,  36,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  2
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  3
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   36,   36,   36,   36,   36,   36},   //  4
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  5
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  6
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  7
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  8
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  9
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36}    // 10
                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  0 * N
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  1
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  2
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  3
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   31,   47,   47,   47,   47,   47,   47},   //  4
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  5
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  6
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  7
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  8
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  9
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47}    // 10
                    }
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
                    0   // i4Enable
                }
            }
        },
        //   Reserve1
        {
            1,  // i4AutoLCEEnable
            // rDefLCEParam
            {
                2,  //  i4LCEBa
                29, //  i4LCEPa
                31  //  i4LCEPb
            },
            //  rAutoLCEParam
            {
                3,          //  i4LCESeg
                800,        //  i4LCEContrastRatio
                {
                    2,      // i4LCEBa
                    {
                        // i4LCEPa
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  0 * N
                        {36,  36,  36,  36,  36,  36,  36,  36,  36,  21,  21,   21,   29,   36,   36,   36,   36,   36,   36},   //  1
                        {36,  36,  36,  36,  36,  36,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  2
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  29,   29,   29,   36,   36,   36,   36,   36,   36},   //  3
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   36,   36,   36,   36,   36,   36},   //  4
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  5
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   36,   36,   36,   36,   36},   //  6
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  7
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  8
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36},   //  9
                        {29,  29,  29,  29,  29,  29,  29,  29,  29,  29,  36,   36,   29,   29,   29,   36,   36,   36,   36}    // 10
                    },
                    {
                    // i4LCEPb
                    //  LV0  LV1  LV2  LV3  LV4  LV5  LV6  LV7  LV8  LV9  LV10  LV11  LV12  LV13  LV14  LV15  LV16  LV17  LV18
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  0 * N
                        {134, 134, 134, 113,  91,  69,  47,  31,  31,  31,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  1
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  2
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  31,   31,   31,   47,   47,   47,   47,   47,   47},   //  3
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   31,   47,   47,   47,   47,   47,   47},   //  4
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  5
                        {134, 134, 134, 134, 134, 113,  91,  75,  63,  47,  47,   47,   47,   47,   47,   47,   47,   47,   47},   //  6
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  7
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  8
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47},   //  9
                        {134, 134, 134, 134, 134, 134, 113,  91,  91,  75,  63,   47,   47,   47,   47,   47,   47,   47,   47}    // 10
                    }
                },
                {           // rLCESmooth
                    1,      // i4Enable
                    0,      // i4WaitAEStable
                    4       // i4Speed
                },
                {
                    // rLCEFlare
                    0   // i4Enable
                }
            }
        }
    },
    ANR_TBL:
  {0}

}};

const NVRAM_CAMERA_3A_STRUCT CAMERA_3A_NVRAM_DEFAULT_VALUE =
{
    NVRAM_CAMERA_3A_FILE_VERSION, // u4Version
    SENSOR_ID, // SensorId

    // AE NVRAM
    {
        // rDevicesInfo
        {
            1136,    // u4MinGain, 1024 base = 1x
            8192,    // u4MaxGain, 16x
            100,    // u4MiniISOGain, ISOxx  
            128,    // u4GainStepUnit, 1x/8
            15812,    // u4PreExpUnit 
            30,     // u4PreMaxFrameRate
            10682,    // u4VideoExpUnit  
            30,     // u4VideoMaxFrameRate
            1024,   // u4Video2PreRatio, 1024 base = 1x
            10091,    // u4CapExpUnit 
            24,     // u4CapMaxFrameRate
            1024,   // u4Cap2PreRatio, 1024 base = 1x
            10091,    // u4Video1ExpUnit
            120,    // u4Video1MaxFrameRate
            1024,   // u4Video12PreRatio, 1024 base = 1x
            29971,    // u4Video2ExpUnit
            30,     // u4Video2MaxFrameRate
            1024,   // u4Video22PreRatio, 1024 base = 1x
            10091,    // u4Custom1ExpUnit
            24,    // u4Custom1MaxFrameRate
            1024,   // u4Custom12PreRatio, 1024 base = 1x
            10091,    // u4Custom2ExpUnit
            24,    // u4Custom2MaxFrameRate
            1024,   // u4Custom22PreRatio, 1024 base = 1x
            10091,    // u4Custom3ExpUnit
            24,    // u4Custom3MaxFrameRate
            1024,   // u4Custom32PreRatio, 1024 base = 1x
            10091,    // u4Custom4ExpUnit
            24,    // u4Custom4MaxFrameRate
            1024,   // u4Custom42PreRatio, 1024 base = 1x
            10091,    // u4Custom5ExpUnit
            24,    // u4Custom5MaxFrameRate
            1024,   // u4Custom52PreRatio, 1024 base = 1x
            20,    // u4LensFno, Fno = 2.8
            350     // u4FocusLength_100x
        },
        // rHistConfig
        {
            4, // 2,   // u4HistHighThres
            40,  // u4HistLowThres
            2,   // u4MostBrightRatio
            1,   // u4MostDarkRatio
            160, // u4CentralHighBound
            20,  // u4CentralLowBound
            {240, 230, 220, 210, 200}, // u4OverExpThres[AE_CCT_STRENGTH_NUM]
            {62, 70, 82, 108, 141},  // u4HistStretchThres[AE_CCT_STRENGTH_NUM]
            {18, 22, 26, 30, 34}       // u4BlackLightThres[AE_CCT_STRENGTH_NUM]
        },
        // rCCTConfig
        {
            TRUE,            // bEnableBlackLight
            TRUE,            // bEnableHistStretch
            TRUE,           // bEnableAntiOverExposure
            TRUE,            // bEnableTimeLPF
            TRUE,            // bEnableCaptureThres
            TRUE,            // bEnableVideoThres
            TRUE,            // bEnableVideo1Thres
            TRUE,            // bEnableVideo2Thres
            TRUE,            // bEnableCustom1Thres
            TRUE,            // bEnableCustom2Thres
            TRUE,            // bEnableCustom3Thres
            TRUE,            // bEnableCustom4Thres
            TRUE,            // bEnableCustom5Thres
            TRUE,            // bEnableStrobeThres
            47,                // u4AETarget
            47,                // u4StrobeAETarget

            50,                // u4InitIndex
            4,                 // u4BackLightWeight
            32,                // u4HistStretchWeight
            4,                 // u4AntiOverExpWeight
            2,                 // u4BlackLightStrengthIndex
            2,                 // u4HistStretchStrengthIndex
            2,                 // u4AntiOverExpStrengthIndex
            2,                 // u4TimeLPFStrengthIndex
            {1, 3, 5, 7, 8}, // u4LPFConvergeTable[AE_CCT_STRENGTH_NUM]
            90,                // u4InDoorEV = 9.0, 10 base
            -7,    // i4BVOffset delta BV = value/10
            128,    // u4PreviewFlareOffset
            128,    // u4CaptureFlareOffset
            3,                 // u4CaptureFlareThres
            128,    // u4VideoFlareOffset
            3,                 // u4VideoFlareThres
            128,    // u4CustomFlareOffset
            3,                 //  u4CustomFlareThres
            64,                 // u4StrobeFlareOffset //12 bits
            3,                 // u4StrobeFlareThres // 0.5%
            160,                 // u4PrvMaxFlareThres //12 bit
            0,                 // u4PrvMinFlareThres
            160,                 // u4VideoMaxFlareThres // 12 bit
            0,                 // u4VideoMinFlareThres
            18,                // u4FlatnessThres              // 10 base for flatness condition.
            75,    // u4FlatnessStrength
            //rMeteringSpec
            {
                //rHS_Spec
                {
                    TRUE,//bEnableHistStretch           // enable histogram stretch
                    1024,//u4HistStretchWeight          // Histogram weighting value
                    40, //50, //20,//u4Pcent                      // 1%=10, 0~1000
                    160, //166,//176,//u4Thd                        // 0~255
                    75, //54, //74,//u4FlatThd                    // 0~255

                    120,//u4FlatBrightPcent
                    120,//u4FlatDarkPcent
                    //sFlatRatio
                    {
                        1000,  //i4X1
                        1024,  //i4Y1
                        2400, //i4X2
                        0     //i4Y2
                    },
                    TRUE, //bEnableGreyTextEnhance
                    1800, //u4GreyTextFlatStart, > sFlatRatio.i4X1, < sFlatRatio.i4X2
                    {
                        10,     //i4X1
                        1024,   //i4Y1
                        80,     //i4X2
                        0       //i4Y2
                    }
                },
                //rAOE_Spec
                {
                    TRUE, //bEnableAntiOverExposure
                    1024, //u4AntiOverExpWeight
                    10,    //u4Pcent
                  220,//  200,  //u4Thd

                    TRUE, //bEnableCOEP
                    1,    //u4COEPcent
                    106,  //u4COEThd
                    0,  // u4BVCompRatio
                    //sCOEYRatio;     // the outer y ratio
                    {
                        23,   //i4X1
                        1024,  //i4Y1
                        47,   //i4X2
                        0     //i4Y2
                    },
                    //sCOEDiffRatio;  // inner/outer y difference ratio
                    {
                        1500, //i4X1
                        0,    //i4Y1
                        2100, //i4X2
                        1024   //i4Y2
                    }
                },
                //rABL_Spec
                {
                    TRUE,//bEnableBlackLigh
                    1024,//u4BackLightWeigh
                    400,//u4Pcent
                    22,//u4Thd,
                    255, // center luminance
                    246, //256, // final target limitation, 256/128 = 2x
                    //sFgBgEVRatio
                    {
                        2100,//2200, //i4X1
                        0,    //i4Y1
                        4000, //i4X2
                        1024   //i4Y2
                    },
                    //sBVRatio
                    {
                        3800,//i4X1
                        0,   //i4Y1
                        5000,//i4X2
                        1024  //i4Y2
                    }
                },
                //rNS_Spec
                {
                    TRUE, // bEnableNightScene
                    10, //5,    //u4Pcent
                   150, // 170,  //u4Thd
                    72, //52,   //u4FlatThd

					          180, //  200,  //u4BrightTonePcent
				            80, //85,//	82, //  95, //u4BrightToneThd

                    500,  //u4LowBndPcent
                    5,    //u4LowBndThd
                    26,    //u4LowBndThdLimit

                    50,  //u4FlatBrightPcent;
                    300,   //u4FlatDarkPcent;
                    //sFlatRatio
                    {
                        1200, //i4X1
                        1024, //i4Y1
                      2800,//  2400, //i4X2
                        0    //i4Y2
                    },
                    //sBVRatio
                    {
                        -500, //i4X1
                        1024,  //i4Y1
                        3000, //i4X2
                        0     //i4Y2
                    },
                    TRUE, // bEnableNightSkySuppresion
                    //sSkyBVRatio
                    {
                        -4000, //i4X1
                        1024, //i4X2
                        -2000,  //i4Y1
                        0     //i4Y2
                    }
                },
                // rTOUCHFD_Spec
                {
                    40, //uMeteringYLowBound;
                    50, //uMeteringYHighBound;
                    40, //uFaceYLowBound;
                    50, //uFaceYHighBound;
                    3,  //uFaceCentralWeight;
                    120,//u4MeteringStableMax;
                    80, //u4MeteringStableMin;
                }
            }, //End rMeteringSpec
            // rFlareSpec
            {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //uPrvFlareWeightArr[16];
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, //uVideoFlareWeightArr[16];
                96,                                               //u4FlareStdThrHigh;
                48,                                               //u4FlareStdThrLow;
                0,                                                //u4PrvCapFlareDiff;
                2,                                                //u4FlareMaxStepGap_Fast;
                0,//0,                                                //u4FlareMaxStepGap_Slow;
                1800,                                             //u4FlarMaxStepGapLimitBV;
                2,//0,                                                //u4FlareAEStableCount;
            },
            //rAEMoveRatio =
            {
                100,//100, //u4SpeedUpRatio 500
                100, //100, //u4GlobalRatio
                190, //u4Bright2TargetEnd  190 150
                10,//20,   //u4Dark2TargetStart
                90, //u4B2TEnd
                85,//70,  //u4B2TStart 70
                70,//60,  //u4D2TEnd 60
                85,  //u4D2TStart
            },

            //rAEVideoMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEVideo1MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEVideo2MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom1MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom2MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom3MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom4MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAECustom5MoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                150,  //u4Bright2TargetEnd
                20,    //u4Dark2TargetStart
                90, //u4B2TEnd
                40,  //u4B2TStart
                30,  //u4D2TEnd
                90,  //u4D2TStart
            },

            //rAEFaceMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                190,  //u4Bright2TargetEnd
                10,    //u4Dark2TargetStart
                80, //u4B2TEnd
                30,  //u4B2TStart
                20,  //u4D2TEnd
                60,  //u4D2TStart
            },

            //rAETrackingMoveRatio =
            {
                100, //u4SpeedUpRatio
                100, //u4GlobalRatio
                190,  //u4Bright2TargetEnd
                10,    //u4Dark2TargetStart
                80, //u4B2TEnd
                30,  //u4B2TStart
                20,  //u4D2TEnd
                60,  //u4D2TStart
            },
            //rAEAOENVRAMParam =
            {
                1,      // i4AOEStrengthIdx: 0 / 1 / 2
                130,    // u4BVCompRatio
                {
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        10,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        1,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    },
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        15, //20,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        3, //10,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    },
                    {
                        47,  //u4Y_Target
                        25,  //u4AOE_OE_percent
                        210,  //u4AOE_OEBound
                        25,    //u4AOE_DarkBound
                        950,    //u4AOE_LowlightPrecent
                        8,    //u4AOE_LowlightBound
                        145,    //u4AOESceneLV_L
                        180,    //u4AOESceneLV_H
                        40,    //u4AOE_SWHdrLE_Bound
                    }
                }
            }
        },
        // rAEHDRConfig
        {
            3072,   // i4RMGSeg
            35,     // i4HDRTarget_L;
            40,     // i4HDRTarget_H;
            100,    // i4HDRTargetLV_L;
            120,    // i4HDRTargetLV_H;
            20,     // i4OverExpoRatio;
            212,    // i4OverExpoTarget;
            120,    // i4OverExpoLV_L;
            180,    // i4OverExpoLV_H;
            4,      // i4UnderExpoContrastThr;
            {
             // Contrast:
             //  0   1   2   3   4   5   6   7   8   9  10
                 3,  3,  3,  3,  3,  2,  1,  1,  1,  1,  1    // i4UnderExpoTargetTbl[AE_HDR_UNDEREXPO_CONTRAST_TARGET_TBL_NUM];
            },
            950,        // i4UnderExpoRatio;
            1000,       // i4AvgExpoRatio;
            8, //10,       // i4AvgExpoTarget;
            768,  // i4HDRAESpeed
            2,          // i4HDRAEConvergeThr;
            40,         // i4SWHdrLEThr
            20,         // i4SWHdrSERatio
            180,        // i4SWHdrSETarget
            1024        // i4SWHdrBaseGain
        }
    },

        // AWB NVRAM
    AWB_NVRAM_START
    {
        {
            {
                // AWB calibration data
                {
                    // rUnitGain (unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rGoldenGain (golden sample gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rUnitGain TL84 (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain TL84 (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                     // rUnitGain Alight (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain Alight (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rTuningUnitGain (Tuning sample unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rD65Gain (D65 WB gain: 1.0 = 512)
                    {
                    1132,    // D65Gain_R
                    512,    // D65Gain_G
                    759    // D65Gain_B
                }
            },
            // Original XY coordinate of AWB light source
            {
                // Strobe
                {
                    0,    // i4X
                    0    // i4Y
                },
                // Horizon
                {
                    -439,    // OriX_Hor
                    -425    // OriY_Hor
                },
                // A
                {
                    -288,    // OriX_A
                    -435    // OriY_A
                },
                // TL84
                {
                    -84,    // OriX_TL84
                    -492    // OriY_TL84
                },
                // CWF
                {
                    -62,    // OriX_CWF
                    -574    // OriY_CWF
                },
                // DNP
                {
                    38,    // OriX_DNP
                    -430    // OriY_DNP
                        },
                        // D65
                        {
                    148,    // OriX_D65
                    -438    // OriY_D65
                },
                // DF
                {
                    0,    // OriX_DF
                    0    // OriY_DF
                }
            },
            // Rotated XY coordinate of AWB light source
            {
                // Strobe
                {
                    0,    // i4X
                    0    // i4Y
                },
                // Horizon
                {
                    -439,    // RotX_Hor
                    -425    // RotY_Hor
                },
                // A
                {
                    -288,    // RotX_A
                    -435    // RotY_A
                },
                // TL84
                {
                    -84,    // RotX_TL84
                    -492    // RotY_TL84
                },
                // CWF
                {
                    -62,    // RotX_CWF
                    -574    // RotY_CWF
                },
                // DNP
                {
                    38,    // RotX_DNP
                    -430    // RotY_DNP
                },
                // D65
                {
                    148,    // RotX_D65
                    -438    // RotY_D65
                },
                // DF
                {
                    138,    // RotX_DF
                    -558    // RotY_DF
                }
            },
            // AWB gain of AWB light source
            {
                // Strobe 
                {
                    512,    // i4R
                    512,    // i4G
                    512    // i4B
                },
                // Horizon 
                {
                    512,    // AWBGAIN_HOR_R
                    522,    // AWBGAIN_HOR_G
                    1681    // AWBGAIN_HOR_B
                },
                // A 
                {
                    625,    // AWBGAIN_A_R
                    512,    // AWBGAIN_A_G
                    1363    // AWBGAIN_A_B
                },
                // TL84 
                {
                    890,    // AWBGAIN_TL84_R
                    512,    // AWBGAIN_TL84_G
                    1116    // AWBGAIN_TL84_B
                },
                // CWF 
                {
                    1024,    // AWBGAIN_CWF_R
                    512,    // AWBGAIN_CWF_G
                    1210    // AWBGAIN_CWF_B
                },
                // DNP 
                {
                    964,    // AWBGAIN_DNP_R
                    512,    // AWBGAIN_DNP_G
                    870    // AWBGAIN_DNP_B
                },
                // D65 
                {
                    1132,    // AWBGAIN_D65_R
                    512,    // AWBGAIN_D65_G
                    759    // AWBGAIN_D65_B
                },
                // DF 
                {
                    512,    // AWBGAIN_DF_R
                    512,    // AWBGAIN_DF_G
                    512    // AWBGAIN_DF_B
                }
            },
            // Rotation matrix parameter
            {
                0,    // RotationAngle
                256,    // Cos
                0    // Sin
            },
            // Daylight locus parameter
            {
                -128,    // i4SlopeNumerator
                128    // i4SlopeDenominator
            },
            // Predictor gain
            {
                101, // i4PrefRatio100
                // DaylightLocus_L
                {
                    1102,    // i4R
                    530,    // i4G
                    779    // i4B
                },
                // DaylightLocus_H
                {
                    956,    // i4R
                    512,    // i4G
                    898    // i4B
                },
                // Temporal General
                {
                    1132,    // i4R
                    512,    // i4G
                    759    // i4B
                },
                // AWB LSC Gain
                {
                    955,        // i4R
                    512,        // i4G
                    899        // i4B
                }
            },
            // AWB light area
            {
                // Strobe:FIXME
                {
                    0,    // i4RightBound
                    0,    // i4LeftBound
                    0,    // i4UpperBound
                    0    // i4LowerBound
                },
                // Tungsten
                {
                    -270,    // TungRightBound
                    -839,    // TungLeftBound
                    -370,    // TungUpperBound
                    -420    // TungLowerBound
                },
                // Warm fluorescent
                {
                    -270,    // WFluoRightBound
                    -839,    // WFluoLeftBound
                    -420,    // WFluoUpperBound
                    -664    // WFluoLowerBound
                },
                // Fluorescent
                {
                    -2,    // FluoRightBound
                    -270,    // FluoLeftBound
                    -378,    // FluoUpperBound
                    -523    // FluoLowerBound
                },
                // CWF
                {
                26,    // CWFRightBound
                -270,    // CWFLeftBound
                -523,    // CWFUpperBound
                -629    // CWFLowerBound
                },
                // Daylight
                {
                    198,    // DayRightBound
                    -2,    // DayLeftBound
                    -378,    // DayUpperBound
                    -523    // DayLowerBound
                },
                // Shade
                {
                    508,    // ShadeRightBound
                    198,    // ShadeLeftBound
                    -378,    // ShadeUpperBound
                    -492    // ShadeLowerBound
                },
                // Daylight Fluorescent
                {
                    198,    // DFRightBound
                    26,    // DFLeftBound
                    -523,    // DFUpperBound
                    -629    // DFLowerBound
                }
            },
            // PWB light area
            {
                // Reference area
                {
                    508,    // PRefRightBound
                    -839,    // PRefLeftBound
                    0,    // PRefUpperBound
                    -664    // PRefLowerBound
                },
                // Daylight
                {
                    223,    // PDayRightBound
                    -2,    // PDayLeftBound
                    -378,    // PDayUpperBound
                    -523    // PDayLowerBound
                },
                // Cloudy daylight
                {
                    323,    // PCloudyRightBound
                    148,    // PCloudyLeftBound
                    -378,    // PCloudyUpperBound
                    -523    // PCloudyLowerBound
                },
                // Shade
                {
                    423,    // PShadeRightBound
                    148,    // PShadeLeftBound
                    -378,    // PShadeUpperBound
                    -523    // PShadeLowerBound
                },
                // Twilight
                {
                    -2,    // PTwiRightBound
                    -162,    // PTwiLeftBound
                    -378,    // PTwiUpperBound
                    -523    // PTwiLowerBound
                },
                // Fluorescent
                {
                    198,    // PFluoRightBound
                    -184,    // PFluoLeftBound
                    -388,    // PFluoUpperBound
                    -624    // PFluoLowerBound
                },
                // Warm fluorescent
                {
                    -188,    // PWFluoRightBound
                    -388,    // PWFluoLeftBound
                    -388,    // PWFluoUpperBound
                    -624    // PWFluoLowerBound
                },
                // Incandescent
                {
                    -188,    // PIncaRightBound
                    -388,    // PIncaLeftBound
                    -378,    // PIncaUpperBound
                    -523    // PIncaLowerBound
                },
                // Gray World
                {
                    5000,    // PGWRightBound
                    -5000,    // PGWLeftBound
                    5000,    // PGWUpperBound
                    -5000    // PGWLowerBound
                }
            },
            // PWB default gain	
            {
                // Daylight
                {
                    1094,    // PWB_Day_R
                    512,    // PWB_Day_G
                    811    // PWB_Day_B
                },
                // Cloudy daylight
                {
                    1296,    // PWB_Cloudy_R
                    512,    // PWB_Cloudy_G
                    685    // PWB_Cloudy_B
                },
                // Shade
                {
                    1387,    // PWB_Shade_R
                    512,    // PWB_Shade_G
                    640    // PWB_Shade_B
                },
                // Twilight
                {
                    843,    // PWB_Twi_R
                    512,    // PWB_Twi_G
                    1053    // PWB_Twi_B
                },
                // Fluorescent
                {
                    1025,    // PWB_Fluo_R
                    512,    // PWB_Fluo_G
                    1006    // PWB_Fluo_B
                },
                // Warm fluorescent
                {
                    688,    // PWB_WFluo_R
                    512,    // PWB_WFluo_G
                    1500    // PWB_WFluo_B
                },
                // Incandescent
                {
                    638,    // PWB_Inca_R
                    512,    // PWB_Inca_G
                    1391    // PWB_Inca_B
                },
                // Gray World
                {
                    512,    // PWB_GW_R
                    512,    // PWB_GW_G
			512	// PWB_GW_B
                        }
                },
                // AWB preference color
                {
                    // Tungsten
                        {
                    40,    // TUNG_SLIDER
                    5776    // TUNG_OFFS
                },
                // Warm fluorescent	
                {
                    40,    // WFluo_SLIDER
                    5895    // WFluo_OFFS
                },
                // Shade
                {
                    50,    // Shade_SLIDER
                    681    // Shade_OFFS
                },
                // Sunset Area
                {
                    68,   // i4Sunset_BoundXr_Thr
                    -430    // i4Sunset_BoundYr_Thr
                },
                // Shade F Area
                {
                    -270,   // i4BoundXrThr
                    -496    // i4BoundYrThr
                },
                // Shade F Vertex
                {
                    -84,   // i4BoundXrThr
                    -515    // i4BoundYrThr
                },
                // Shade CWF Area
                {
                    -270,   // i4BoundXrThr
                    -578    // i4BoundYrThr
                },
                // Shade CWF Vertex
                {
                    -70,   // i4BoundXrThr
                    -604    // i4BoundYrThr
                },
                },
                // CCT estimation
                {
	                // CCT
	                {
		                2300,	// i4CCT[0]
		                2850,	// i4CCT[1]
		                3750,	// i4CCT[2]
		                5100,	// i4CCT[3]
		                6500 	// i4CCT[4]
	                },
	                // Rotated X coordinate
	                {
                -587,    // i4RotatedXCoordinate[0]
                -436,    // i4RotatedXCoordinate[1]
                -232,    // i4RotatedXCoordinate[2]
                -110,    // i4RotatedXCoordinate[3]
			                0 	    // i4RotatedXCoordinate[4]
	                }
                }
            },

            // Algorithm Tuning Paramter
            {
                // AWB Backup Enable
                0,

                // Daylight locus offset LUTs for tungsten
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                    {0, 350,  800, 1222, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778,  5000} // i4LUTOut
                },

                // Daylight locus offset LUTs for WF
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 700, 1000, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
                },

                // Daylight locus offset LUTs for shade
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000} // i4LUTOut
                },
                // Preference gain for each light source
                {
                    //        LV0              LV1              LV2              LV3              LV4              LV5              LV6              LV7              LV8              LV9
                    //        LV10             LV11             LV12             LV13             LV14             LV15             LV16             LV17             LV18
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
           	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // TUNGSTEN
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // WARM F
        	        {
                    {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, 
                    {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}, {513, 512, 513}
                    }, // F
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                }, // F
                {
                    {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, 
                    {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}, {512, 512, 526}
                    }, // CWF
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // DAYLIGHT
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // SHADE
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    } // DAYLIGHT F
                },
                // Parent block weight parameter
                {
                    1,      // bEnable
                    6           // i4ScalingFactor: [6] 1~12, [7] 1~6, [8] 1~3, [9] 1~2, [>=10]: 1
                },
                // AWB LV threshold for predictor
                {
                115,    // i4InitLVThr_L
                155,    // i4InitLVThr_H
                100      // i4EnqueueLVThr
                },
                // AWB number threshold for temporal predictor
                {
                        65,     // i4Neutral_ParentBlk_Thr
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   2,   2,   2,   2,   2,   2,   2}  // (%) i4CWFDF_LUTThr
                },
                // AWB light neutral noise reduction for outdoor
                {
                    //LV0  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    // Non neutral
	                { 3,   3,   3,   3,   3,   3,   3,   3,    3,   3,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Flurescent
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // CWF
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Daylight
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   2,   2,   2,   2,   2,   2,   2,   2},  // (%)
	                // DF
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
                },
                // AWB feature detection
                {
                    // Sunset Prop
                    {
                        1,          // i4Enable
                        120,        // i4LVThr_L
                        130,        // i4LVThr_H
                        10,         // i4SunsetCountThr
                        0,          // i4SunsetCountRatio_L
                        171         // i4SunsetCountRatio_H
                    },

                    // Shade F Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        128         // i4DaylightProb
                    },

                    // Shade CWF Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        90,        // i4LVThr_H
                        192         // i4DaylightProb
                    }

                },

                // AWB non-neutral probability for spatial and temporal weighting look-up table (Max: 100; Min: 0)
                {
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    {   0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100,  70,  30,  20,  10,   0,   0,   0,   0}
                },

                // AWB daylight locus probability look-up table (Max: 100; Min: 0)
                {
                    //LV0  1    2    3    4    5    6    7    8    9     10    11   12   13   14   15  16   17   18
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100,  50,  25,   0,  0,   0,   0}, // Strobe
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0}, // Tungsten
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,   75,  50,  25,  25,  25,  0,   0,   0}, // Warm fluorescent
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  95,  75,  50,  25,  25,  25,   0,   0,   0}, // Fluorescent
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}, // CWF
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100, 100,  75,  50, 40,  30,  20}, // Daylight
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100,  75,  50,  25,   0,  0,   0,   0}, // Shade
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}  // Daylight fluorescent
                },

                // AWB tuning information
                {
                    0,		// project code
                    0,		// model
                    0,		// date
                    0,          // reserved 0
                    0,		// reserved 1
                    0,		// reserved 2
                    0,		// reserved 3
                    0,		// reserved 4
                }
            }
        },
        {
            {
                // AWB calibration data
                {
                    // rUnitGain (unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rGoldenGain (golden sample gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rUnitGain TL84 (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain TL84 (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                     // rUnitGain Alight (unit gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rGoldenGain Alight (golden sample gain: 1.0 = 512)
                    {
                        0,    // i4R
                        0,    // i4G
                        0     // i4B
                    },
                    // rTuningUnitGain (Tuning sample unit gain: 1.0 = 512)
                    {
                            0,    // i4R
                            0,    // i4G
                            0    // i4B
                    },
                    // rD65Gain (D65 WB gain: 1.0 = 512)
                    {
                    1135,    // D65Gain_R
                                512,    // i4G
                    718    // D65Gain_B
                    }
                },
                // Original XY coordinate of AWB light source
                {
                    // Strobe
                        {
                                0,    // i4X
                                0    // i4Y
                        },
                        // Horizon
                        {
                    -439,    // OriX_Hor
                    -431    // OriY_Hor
                        },
                        // A
                        {
                    -299,    // OriX_A
                    -440    // OriY_A
                        },
                        // TL84
                        {
                    -91,    // OriX_TL84
                    -474    // OriY_TL84
                        },
                        // CWF
                        {
                    -60,    // OriX_CWF
                    -538    // OriY_CWF
                        },
                        // DNP
                        {
                    50,    // OriX_DNP
                    -450    // OriY_DNP
                        },
                        // D65
                        {
                    169,    // OriX_D65
                    -419    // OriY_D65
                        },
                        // DF
                        {
                    154,    // OriX_DF
                    -505    // OriY_DF
                        }
                },
                // Rotated XY coordinate of AWB light source
                {
                    // Strobe
                        {
                                0,    // i4X
                                0    // i4Y
                        },
                        // Horizon
                        {
                    -461,    // RotX_Hor
                    -409    // RotY_Hor
                        },
                        // A
                        {
                    -321,    // RotX_A
                    -425    // RotY_A
                        },
                        // TL84
                        {
                    -115,    // RotX_TL84
                    -469    // RotY_TL84
                        },
                        // CWF
                        {
                    -87,    // RotX_CWF
                    -535    // RotY_CWF
                        },
                        // DNP
                        {
                    27,    // RotX_DNP
                    -453    // RotY_DNP
                        },
                        // D65
                        {
                    148,    // RotX_D65
                    -428    // RotY_D65
                        },
                        // DF
                        {
                    128,    // RotX_DF
                    -513    // RotY_DF
                        }
                },
                // AWB gain of AWB light source
                {
                    // Strobe
                        {
                                512,    // i4R
                                512,    // i4G
                                512    // i4B
                        },
                        // Horizon
                        {
                                512,    // i4G
                    518,    // AWBGAIN_HOR_G
                    1682    // AWBGAIN_HOR_B
                        },
                        // A
                        {
                    620,    // AWBGAIN_A_R
                                512,    // i4G
                    1393    // AWBGAIN_A_B
                        },
                        // TL84
                        {
                    860,    // AWBGAIN_TL84_R
                                512,    // i4G
                    1100    // AWBGAIN_TL84_B
                        },
                        // CWF
                        {
                    978,    // AWBGAIN_CWF_R
                                512,    // i4G
                    1152    // AWBGAIN_CWF_B
                        },
                        // DNP
                        {
                    1007,    // AWBGAIN_DNP_R
                                512,    // i4G
                    880    // AWBGAIN_DNP_B
                        },
                        // D65
                        {
                    1135,    // AWBGAIN_D65_R
                                512,    // i4G
                    718    // AWBGAIN_D65_B
                        },
                        // DF
                        {
                    1250,    // AWBGAIN_DF_R
                                512,    // i4G
                    823    // AWBGAIN_DF_B
                        }
                },
                // Rotation matrix parameter
                {
                3,    // RotationAngle
                        256,    // i4Cos
                13    // Sin
                },
                // Daylight locus parameter
                {
                -140,    // i4SlopeNumerator
                        128    // i4SlopeDenominator
                },
	            // Predictor gain
                {
                    // i4PrefRatio100
                        101,

                        // DaylightLocus_L
                        {
                    1105,    // i4R
                    530,    // i4G
                    738    // i4B
                        },
                        // DaylightLocus_H
                        {
                    898,    // i4R
                            512, // i4G
                    928    // i4B
                        },
                        // Temporal General
                        {
                    1135,    // i4R
                            512, // i4G
                    718    // i4B
                        },
                    // AWB LSC Gain
                    {
                    952,        // i4R
                            512, // i4G
                    874        // i4B
                    }
                },
                // AWB light area
                {
                    // Strobe
                        {
			0,	// StrobeRightBound
			0,	// StrobeLeftBound
			0,	// StrobeUpperBound
			0	// StrobeLowerBound
                        },
                        // Tungsten
                        {
                    -197,    // TungRightBound
                    -861,    // TungLeftBound
                    -354,    // TungUpperBound
                    -447    // TungLowerBound
                        },
                        // Warm fluorescent
                        {
                    -197,    // WFluoRightBound
                    -861,    // WFluoLeftBound
                    -447,    // WFluoUpperBound
                    -625    // WFluoLowerBound
                        },
                        // Fluorescent
                        {
			-19,	// FluoRightBound
                    -197,    // FluoLeftBound
                    -368,    // FluoUpperBound
                    -502    // FluoLowerBound
                        },
                        // CWF
                        {
                8,    // CWFRightBound
                -197,    // CWFLeftBound
                -502,    // CWFUpperBound
                -590    // CWFLowerBound
                        },
                        // Daylight
                        {
                    178,    // DayRightBound
			-19,	// DayLeftBound
                    -368,    // DayUpperBound
                    -502    // DayLowerBound
                        },
                        // Shade
                        {
                    508,    // ShadeRightBound
                    178,    // ShadeLeftBound
                    -368,    // ShadeUpperBound
                    -471    // ShadeLowerBound
                        },
                        // Daylight Fluorescent
                        {
                    178,    // DFRightBound
                    8,    // DFLeftBound
                    -502,    // DFUpperBound
                    -590    // DFLowerBound
                        }
                },
                // PWB light area
                {
                    // Reference area
                        {
                    508,    // PRefRightBound
                    -861,    // PRefLeftBound
                    -329,    // PRefUpperBound
                    -625    // PRefLowerBound
                        },
                        // Daylight
                        {
                    203,    // PDayRightBound
			-19,	// PDayLeftBound
                    -368,    // PDayUpperBound
                    -502    // PDayLowerBound
                        },
                        // Cloudy daylight
                        {
                    303,    // PCloudyRightBound
                    128,    // PCloudyLeftBound
                    -368,    // PCloudyUpperBound
                    -502    // PCloudyLowerBound
                        },
                        // Shade
                        {
                    403,    // PShadeRightBound
                    128,    // PShadeLeftBound
                    -368,    // PShadeUpperBound
                    -502    // PShadeLowerBound
                        },
                        // Twilight
                        {
			-19,	// PTwiRightBound
			-179,	// PTwiLeftBound
                    -368,    // PTwiUpperBound
                    -502    // PTwiLowerBound
                        },
                        // Fluorescent
                        {
                    198,    // PFluoRightBound
                    -215,    // PFluoLeftBound
                    -378,    // PFluoUpperBound
                    -585    // PFluoLowerBound
                        },
                        // Warm fluorescent
                        {
                    -221,    // PWFluoRightBound
                    -421,    // PWFluoLeftBound
                    -378,    // PWFluoUpperBound
                    -585    // PWFluoLowerBound
                        },
                        // Incandescent
                        {
                    -221,    // PIncaRightBound
                    -421,    // PIncaLeftBound
                    -368,    // PIncaUpperBound
                    -502    // PIncaLowerBound
                        },
                        // Gray World
                        {
			5000,	// PGWRightBound
			-5000,	// PGWLeftBound
			5000,	// PGWUpperBound
			-5000	// PGWLowerBound
                        }
                },
                // PWB default gain
                {
                    // Daylight
                        {
                    1068,    // PWB_Day_R
			512,	// PWB_Day_G
                    785    // PWB_Day_B
                        },
                        // Cloudy daylight
                        {
                    1251,    // PWB_Cloudy_R
			512,	// PWB_Cloudy_G
                    659    // PWB_Cloudy_B
                        },
                        // Shade
                        {
                    1334,    // PWB_Shade_R
			512,	// PWB_Shade_G
                    613    // PWB_Shade_B
                        },
                        // Twilight
                        {
                    836,    // PWB_Twi_R
			512,	// PWB_Twi_G
                    1029    // PWB_Twi_B
                        },
                        // Fluorescent
                        {
                    1003,    // PWB_Fluo_R
			512,	// PWB_Fluo_G
                    961    // PWB_Fluo_B
                        },
                        // Warm fluorescent
                        {
                    672,    // PWB_WFluo_R
			512,	// PWB_WFluo_G
                    1497    // PWB_WFluo_B
                        },
                        // Incandescent
                        {
                    629,    // PWB_Inca_R
			512,	// PWB_Inca_G
                    1410    // PWB_Inca_B
                        },
                        // Gray World
                        {
			512,	// PWB_GW_R
			512,	// PWB_GW_G
			512	// PWB_GW_B
                        }
                },
                // AWB preference color
                {
                    // Tungsten
                        {
                    40,    // TUNG_SLIDER
                    4905    // TUNG_OFFS
                        },
                        // Warm fluorescent
                        {
                    40,    // WFluo_SLIDER
                    4905    // WFluo_OFFS
                        },
                        // Shade
                        {
			50,	// Shade_SLIDER
			909	// Shade_OFFS
                        },
                    // Sunset Area
                    {
                    63,   // i4Sunset_BoundXr_Thr
                    -453    // i4Sunset_BoundYr_Thr
                    },
                    // Shade F Area
                    {

                    -197,   // i4BoundXrThr
                    -473    // i4BoundYrThr
                    },
                    // Shade F Vertex
                    {

                    -108,   // i4BoundXrThr
                    -488    // i4BoundYrThr
                    },
                    // Shade CWF Area
                    {
                    -197,   // i4BoundXrThr
                    -539    // i4BoundYrThr
                    },
                    // Shade CWF Vertex
                    {
                    -95,   // i4BoundXrThr
                    -565    // i4BoundYrThr
                }
                },
                // CCT estimation
                {
	                // CCT
	                {
		                2300,	// i4CCT[0]
		                2850,	// i4CCT[1]
		                3750,	// i4CCT[2]
		                5100,	// i4CCT[3]
		                6500 	// i4CCT[4]
	                },
	                // Rotated X coordinate
	                {
                -609,    // i4RotatedXCoordinate[0]
                -469,    // i4RotatedXCoordinate[1]
                -263,    // i4RotatedXCoordinate[2]
                -121,    // i4RotatedXCoordinate[3]
			                0 	    // i4RotatedXCoordinate[4]
	                }
                }
            },

            // Algorithm Tuning Paramter
            {
                // AWB Backup Enable
                0,

                // Daylight locus offset LUTs for tungsten
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                    {0, 350,  800, 1222, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778,  5000} // i4LUTOut
                    //{0, 500, 1000, 1333, 1667, 2000, 2333, 2667, 3000, 3333, 3667, 4000, 4333, 4667, 5000, 5333, 5667, 6000, 6333, 6667,  7000} // i4LUTOut
                    //{0, 500, 1000, 1500, 2000, 2313, 2625, 2938, 3250, 3563, 3875, 4188, 4500, 4813, 5125, 5438, 5750, 6063, 6375, 6688,  7000} // i4LUTOut
                },

                // Daylight locus offset LUTs for WF
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 350, 700, 1000, 1444, 1667, 1889, 2111, 2333, 2556, 2778, 3000, 3222, 3444, 3667, 3889, 4111, 4333, 4556, 4778, 5000} // i4LUTOut
                },

                // Daylight locus offset LUTs for shade
                {
                    21, // i4Size: LUT dimension
                    {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000}, // i4LUTIn
                {0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000} // i4LUTOut
                },
                // Preference gain for each light source
                {
                    //        LV0              LV1              LV2              LV3              LV4              LV5              LV6              LV7              LV8              LV9
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                    //        LV10             LV11             LV12             LV13             LV14             LV15             LV16             LV17             LV18
          	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // STROBE
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
           	            {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // TUNGSTEN
        	        {
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, 
                    {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
        	        }, // WARM F
        	        {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // F
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // CWF
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}, {502, 512, 512}
                    }, // DAYLIGHT
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    }, // SHADE
                    {
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512},
                        {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}, {512, 512, 512}
                    } // DAYLIGHT F
                },
                // Parent block weight parameter
                {
                    1,      // bEnable
                    6           // i4ScalingFactor: [6] 1~12, [7] 1~6, [8] 1~3, [9] 1~2, [>=10]: 1
                },
                // AWB LV threshold for predictor
                {
                    115, //100,    // i4InitLVThr_L
                    155, //140,    // i4InitLVThr_H
                    100 //80      // i4EnqueueLVThr
                },
                // AWB number threshold for temporal predictor
                {
                        65,     // i4Neutral_ParentBlk_Thr
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  50,  25,   2,   2,   2,   2,   2,   2,   2}  // (%) i4CWFDF_LUTThr
                },
                // AWB light neutral noise reduction for outdoor
                {
                    //LV0  1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    // Non neutral
	                { 3,   3,   3,   3,   3,   3,   3,   3,    3,   3,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Flurescent
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // CWF
                    {   0,   0,   0,   0,   0,   3,   5,   5,   5,   5,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
	                // Daylight
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   0,   2,   2,   2,   2,   2,   2,   2,   2},  // (%)
	                // DF
	                { 0,   0,   0,   0,   0,   0,   0,   0,    0,   0,   5,  10,  10,  10,  10,  10,  10,  10,  10},  // (%)
                },
                // AWB feature detection
                {
                    // Sunset Prop
                    {
                        1,          // i4Enable
                        120,        // i4LVThr_L
                        130,        // i4LVThr_H
                        10,         // i4SunsetCountThr
                        0,          // i4SunsetCountRatio_L
                        171         // i4SunsetCountRatio_H
                    },

                    // Shade F Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        70,        // i4LVThr_H
                        128         // i4DaylightProb
                    },

                    // Shade CWF Detection
                    {
                        1,          // i4Enable
                        50,        // i4LVThr_L
                        70,        // i4LVThr_H
                        192         // i4DaylightProb
                    },

                },

                // AWB non-neutral probability for spatial and temporal weighting look-up table (Max: 100; Min: 0)
                {
                    //LV0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15   16   17   18
                    {   0,  33,  66, 100, 100, 100, 100, 100, 100, 100, 100,  70,  30,  20,  10,   0,   0,   0,   0}
                },

                // AWB daylight locus probability look-up table (Max: 100; Min: 0)
                {
                    //LV0  1    2    3    4    5    6    7    8    9     10    11   12   13   14   15  16   17   18
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100,  50,  25,   0,  0,   0,   0}, // Strobe
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  75,  50,  25,   0,   0,   0}, // Tungsten
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,   75,  50,  25,  25,  25,  0,   0,   0}, // Warm fluorescent
                    { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  95,  75,  50,  25,  25,  25,   0,   0,   0}, // Fluorescent
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}, // CWF
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100, 100, 100,  75,  50, 40,  30,  20}, // Daylight
                    {100, 100, 100, 100, 100, 100, 100, 100, 100, 100,  100,  100,  75,  50,  25,   0,  0,   0,   0}, // Shade
                    { 90,  90,  90,  90,  90,  90,  90,  90,  90,  90,   80,   55,  30,  30,  30,  30,  0,   0,   0}  // Daylight fluorescent
                },

                // AWB tuning information
                {
                    6735,       // project code
                    5588,       // model
                    20150624,   // date
                    0,          // reserved 0
                    1,          // reserved 1
                    2,          // reserved 2
                    3,          // reserved 3
                    4,          // reserved 4
                }
            }
        }
    },

    // Flash AWB NVRAM
    {
#include INCLUDE_FILENAME_FLASH_AWB_PARA
    },

    {0}
};

#include INCLUDE_FILENAME_ISP_LSC_PARAM
//};  //  namespace

const CAMERA_TSF_TBL_STRUCT CAMERA_TSF_DEFAULT_VALUE =
{
    {
        1,  // isTsfEn
        2,  // tsfCtIdx
        {20, 2000, -110, -110, 512, 512, 512, 0}    // rAWBInput[8]
    },

#include INCLUDE_FILENAME_TSF_PARA
#include INCLUDE_FILENAME_TSF_DATA
};

const NVRAM_CAMERA_FEATURE_STRUCT CAMERA_FEATURE_DEFAULT_VALUE =
{
#include INCLUDE_FILENAME_FEATURE_PARA
};

typedef NSFeature::RAWSensorInfo<SENSOR_ID> SensorInfoSingleton_T;


namespace NSFeature {
  template <>
  UINT32
  SensorInfoSingleton_T::
  impGetDefaultData(CAMERA_DATA_TYPE_ENUM const CameraDataType, VOID*const pDataBuf, UINT32 const size) const
  {
    UINT32 dataSize[CAMERA_DATA_TYPE_NUM] = {sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT),
        sizeof(NVRAM_CAMERA_3A_STRUCT),
        sizeof(NVRAM_CAMERA_SHADING_STRUCT),
        sizeof(NVRAM_LENS_PARA_STRUCT),
        sizeof(AE_PLINETABLE_T),
        0,
        sizeof(CAMERA_TSF_TBL_STRUCT),
        0,
        sizeof(NVRAM_CAMERA_FEATURE_STRUCT)
    };

    if (CameraDataType > CAMERA_NVRAM_DATA_FEATURE || NULL == pDataBuf || (size < dataSize[CameraDataType]))
    {
      return 1;
    }

    switch(CameraDataType)
    {
      case CAMERA_NVRAM_DATA_ISP:
        memcpy(pDataBuf,&CAMERA_ISP_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_3A:
        memcpy(pDataBuf,&CAMERA_3A_NVRAM_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_3A_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_SHADING:
        memcpy(pDataBuf,&CAMERA_SHADING_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_SHADING_STRUCT));
        break;
      case CAMERA_DATA_AE_PLINETABLE:
        memcpy(pDataBuf,&g_PlineTableMapping,sizeof(AE_PLINETABLE_T));
        break;
      case CAMERA_DATA_TSF_TABLE:
        memcpy(pDataBuf,&CAMERA_TSF_DEFAULT_VALUE,sizeof(CAMERA_TSF_TBL_STRUCT));
        break;
      case CAMERA_NVRAM_DATA_FEATURE:
        memcpy(pDataBuf,&CAMERA_FEATURE_DEFAULT_VALUE,sizeof(NVRAM_CAMERA_FEATURE_STRUCT));
        break;
      default:
        break;
    }
    return 0;
  }};  //  NSFeature


