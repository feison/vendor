#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <driver_api.h>
#include <platform.h>
#include <FreeRTOS.h>
#include <semphr.h>
#include <scp_sem.h>
#include <interrupt.h>
#include <task.h>

#include <spi.h>

#define SPI_TRANSFER_POLLING

/*#define SPI_AUTO_SELECT_MODE*/
#ifdef SPI_AUTO_SELECT_MODE
#define SPI_DATA_SIZE 32
#endif
#define IDLE 0
#define INPROGRESS 1
#define PAUSED 2
#define SPI_FIFO_SIZE 32

#define PACKET_SIZE 0x400

/*use this flag to sync every data transfer*/

#define SPITAG                  "[SPI-SCP] "
#define SPI_MSG(fmt, arg...)    printf(SPITAG fmt, ##arg)
#define SPI_ERR(fmt, arg...)    printf(SPITAG "%d: "fmt, __LINE__, ##arg)

static void Spi_enable_clk(uint32_t id)
{
    SPI_SET_BITS(SPI_TOPCG_CLR, SPI_TOPCLK);

    switch (id) {
        case 0:
            SPI_SET_BITS(SPI0_CG_CTRL_CLR, SPI0_CLK);
            break;
        case 4:
            SPI_SET_BITS(SPI_CG_CTRL_CLR, SPI4_CLK);
            break;
        case 5:
            SPI_SET_BITS(SPI_CG_CTRL_CLR, SPI5_CLK);
            break;
        default:
            SPI_ERR("Spi_enable_clk: invalid chip_config: id=%d\n\r", id);
            break;
    }
    SPI_MSG("Spi_enable_clk\n");
}

static void Spi_disable_clk(uint32_t id)
{
    SPI_SET_BITS(SPI_TOPCG_SET, SPI_TOPCLK);

    switch (id) {
        case 0:
            SPI_SET_BITS(SPI0_CG_CTRL_SET, SPI0_CLK);
            break;
        case 4:
            SPI_SET_BITS(SPI_CG_CTRL_SET, SPI4_CLK);
            break;
        case 5:
            SPI_SET_BITS(SPI_CG_CTRL_SET, SPI5_CLK);
            break;
        default:
            SPI_ERR("Spi_enable_clk: invalid chip_config: id=%d\n\r", id);
            break;
    }
    SPI_MSG("Spi_disable_clk\n");
}

static void spi_gpio_set(uint32_t id)
{
    switch (id) {
        case 0:
            SPI_REG_SET32(0xA0005370, (SPI_REG_GET32(0xA0005370) & 0xFFF0000F) | 0x00011110); //pin57 58 59 60
            break;
        case 4:
            SPI_REG_SET32(0xA0005360, (SPI_REG_GET32(0xA0005360) & 0xF0000FFF) | 0x02222000); //pin51 52 53 54
            break;
        case 5:
            SPI_REG_SET32(0xA0005330, (SPI_REG_GET32(0xA0005330) & 0xFF00FFFF) | 0x00110000); //pin28 29
            SPI_REG_SET32(0xA0005340, (SPI_REG_GET32(0xA0005340) & 0xFFFFFF00) | 0x00000011); //pin32 33
            break;
        default:
            SPI_ERR("Spi_enable_clk: invalid chip_config: id=%d\n\r", id);
            break;
    }
}

static struct mt_chip_conf chip_config_default;
struct mt_chip_conf* chip_config0 = &chip_config_default;
struct mt_chip_conf* chip_config1 = &chip_config_default;
struct mt_chip_conf* chip_config2 = &chip_config_default;

//#define SPI_DMA
#if SPI_DMA
static struct mt_chip_conf mt_chip_conf_dma_test = {
    .setuptime = 50,
    .holdtime = 50,
    .high_time = 5,
    .low_time = 5,
    .cs_idletime = 10,
    .ulthgh_thrsh = 0,
    .cpol = 0,
    .cpha = 0,
    .rx_mlsb = 1,
    .tx_mlsb = 1,
    .tx_endian = 0,
    .rx_endian = 0,
    .com_mod = DMA_TRANSFER,
    .pause = 0,
    .finish_intr = 1,
    .deassert = 0,
    .ulthigh = 0,
    .tckdly = 0
};
#endif

static struct mt_chip_conf mt_chip_conf_fifo_test = {
    .setuptime = 50,
    .holdtime = 50,
    .high_time = 5,
    .low_time = 5,
    .cs_idletime = 10,
    .ulthgh_thrsh = 0,
    .cpol = 0,
    .cpha = 0,
    .rx_mlsb = 1,
    .tx_mlsb = 1,
    .tx_endian = 0,
    .rx_endian = 0,
    .com_mod = FIFO_TRANSFER,
    .pause = 0,
    .finish_intr = 1,
    .deassert = 0,
    .ulthigh = 0,
    .tckdly = 0
};

struct mt_chip_conf* GetChipConfig(uint32_t id)
{
    struct mt_chip_conf* chip_config = NULL;

    switch (id) {
        case 0:
            chip_config = chip_config0;
            break;
        case 4:
            chip_config = chip_config1;
            break;
        case 5:
            chip_config = chip_config2;
            break;
        default:
            SPI_ERR("GetChipConfig: invalid chip_config: id=%d\n\r", id);
            break;
    }
    return chip_config;

}

void SetChipConfig(uint32_t id, struct mt_chip_conf* chip_config)
{
    switch (id) {
        case 0:
            chip_config0 = chip_config;
            break;
        case 4:
            chip_config1 = chip_config;
            break;
        case 5:
            chip_config2 = chip_config;
            break;
        default:
            SPI_ERR("SetChipConfig: invalid chip_config: id=%d\n\r", id);
            break;
    }
}

static struct spi_transfer* xfer0 = NULL;
static struct spi_transfer* xfer1 = NULL;
static struct spi_transfer* xfer2 = NULL;
struct spi_transfer* GetSpiTransfer(uint32_t id)
{
    struct spi_transfer* xfer = NULL;

    switch (id) {
        case 0:
            xfer = xfer0;
            break;
        case 4:
            xfer = xfer1;
            break;
        case 5:
            xfer = xfer2;
            break;
        default:
            SPI_ERR("invalid xfer: spi->id=%d\n\r", id);
            break;
    }
    return xfer;
}
void SetSpiTransfer(struct spi_transfer* ptr)
{
    if (NULL == ptr) {
        SPI_ERR("spi_transfer is NULL !!!\n");
    } else {
        switch (ptr->id) {
            case 0:
                xfer0 = ptr;
                break;
            case 4:
                xfer1 = ptr;
                break;
            case 5:
                xfer2 = ptr;
                break;
            default:
                SPI_ERR("invalid ptr: ptr->id=%d\n\r", ptr->id);
                break;
        }
    }
}

/*
* 1: wait interrupt to handle
* 2: finish interrupt handle turn to idle state
*/
static int32_t irq_flag = IRQ_IDLE;
static int32_t irq_flag0 = IRQ_IDLE;
static int32_t irq_flag1 = IRQ_IDLE;
static int32_t irq_flag2 = IRQ_IDLE;
int32_t GetIrqFlag(uint32_t id)
{
    switch (id) {
        case 0:
            irq_flag = irq_flag0;
            break;
        case 4:
            irq_flag = irq_flag1;
            break;
        case 5:
            irq_flag = irq_flag2;
            break;
        default:
            SPI_ERR("GetIrqFlag: invalid irq_flag : id=%d\n\r", id);
            break;
    }

    return irq_flag;
}
void SetIrqFlag(uint32_t id, enum spi_irq_flag flag)
{
    switch (id) {
        case 0:
            irq_flag0 = flag;
            break;
        case 4:
            irq_flag1 = flag;
            break;
        case 5:
            irq_flag2 = flag;
            break;
        default:
            SPI_ERR("SetIrqFlag: invalid irq_flag : id=%d\n\r", id);
            break;
    }
}
static int32_t running = 0;
static int32_t running0 = 0;
static int32_t running1 = 0;
static int32_t running2 = 0;
void SetPauseStatus(uint32_t id, int32_t status)
{
    switch (id) {
        case 0:
            running0 = status;
            break;
        case 4:
            running1 = status;
            break;
        case 5:
            running2 = status;
            break;
        default:
            SPI_ERR("SetPauseStatus: invalid running : id=%d\n\r", id);
            break;
    }
}
int32_t GetPauseStatus(uint32_t id)
{
    switch (id) {
        case 0:
            running = running0;
            break;
        case 4:
            running = running1;
            break;
        case 5:
            running = running2;
            break;
        default:
            SPI_ERR("GetPauseStatus: invalid running : id=%d\n\r", id);
            break;
    }
    return running;
}

void dump_chip_config(struct mt_chip_conf *chip_config)
{
    if (chip_config != NULL) {
        SPI_MSG("setuptime=%d\n", chip_config->setuptime);
        SPI_MSG("holdtime=%d\n", chip_config->holdtime);
        SPI_MSG("high_time=%d\n", chip_config->high_time);
        SPI_MSG("low_time=%d\n", chip_config->low_time);
        SPI_MSG("cs_idletime=%d\n", chip_config->cs_idletime);
        SPI_MSG("ulthgh_thrsh=%d\n", chip_config->ulthgh_thrsh);
        SPI_MSG("cpol=%d\n", chip_config->cpol);
        SPI_MSG("cpha=%d\n", chip_config->cpha);
        SPI_MSG("tx_mlsb=%d\n", chip_config->tx_mlsb);
        SPI_MSG("rx_mlsb=%d\n", chip_config->rx_mlsb);
        SPI_MSG("tx_endian=%d\n", chip_config->tx_endian);
        SPI_MSG("rx_endian=%d\n", chip_config->rx_endian);
        SPI_MSG("com_mod=%d\n", chip_config->com_mod);
        SPI_MSG("pause=%d\n", chip_config->pause);
        SPI_MSG("finish_intr=%d\n", chip_config->finish_intr);
        SPI_MSG("deassert=%d\n", chip_config->deassert);
        SPI_MSG("ulthigh=%d\n", chip_config->ulthigh);
        SPI_MSG("tckdly=%d\n", chip_config->tckdly);
    } else {
        SPI_ERR("dump chip_config is NULL !!\n");
    }
}

void dump_reg_config(uint32_t base)
{
    SPI_MSG("SPI_REG_CFG0=0x%x\n", SPI_READ(base, SPI_REG_CFG0));
    SPI_MSG("SPI_REG_CFG1=0x%x\n", SPI_READ(base, SPI_REG_CFG1));
    SPI_MSG("SPI_REG_CFG2=0x%x\n", SPI_READ(base, SPI_REG_CFG2));
    SPI_MSG("SPI_REG_TX_SRC=0x%x\n", SPI_READ(base, SPI_REG_TX_SRC));
    SPI_MSG("SPI_REG_RX_DST=0x%x\n", SPI_READ(base, SPI_REG_RX_DST));
    SPI_MSG("SPI_REG_CMD=0x%x\n", SPI_READ(base, SPI_REG_CMD));
    SPI_MSG("SPI_REG_PAD_SEL=0x%x\n", SPI_READ(base, SPI_REG_PAD_SEL));
}

static uint32_t IsInterruptEnable(uint32_t base)
{
    uint32_t cmd;
    cmd = SPI_READ(base, SPI_REG_CMD);
    return (cmd >> SPI_CMD_FINISH_IE_OFFSET) & 1;
}

static void inline clear_pause_bit(uint32_t base)
{
    uint32_t reg_val;

    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val &= ~SPI_CMD_PAUSE_EN_MASK;
    SPI_WRITE(base, SPI_REG_CMD, reg_val);
}

static void inline SetPauseBit(uint32_t base)
{
    uint32_t reg_val;

    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val |= 1 << SPI_CMD_PAUSE_EN_OFFSET;
    SPI_WRITE(base, SPI_REG_CMD, reg_val);
}

static void inline ClearResumeBit(uint32_t base)
{
    uint32_t reg_val;

    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val &= ~SPI_CMD_RESUME_MASK;
    SPI_WRITE(base, SPI_REG_CMD, reg_val);
}

/*
*  SpiSetupPacket: used to define per data length and loop count
* @ ptr : data structure from User
*/
static uint32_t inline SpiSetupPacket(struct spi_transfer *xfer)
{
    uint32_t packet_size = 0;
    uint32_t packet_loop = 0;
    uint32_t cfg1 = 0;
    uint32_t base = xfer->base;

    /*set transfer packet and loop*/
    if (xfer->len < PACKET_SIZE)
        packet_size = xfer->len;
    else
        packet_size = PACKET_SIZE;

    if (xfer->len % packet_size) {
        ///packet_loop = xfer->len/packet_size + 1;
        /*parameter not correct, there will be more data transfer,notice user to change*/
        SPI_ERR("The lens are not a multiple of %d, your len %u\n\n", PACKET_SIZE, xfer->len);
    }
    packet_loop = (xfer->len) / packet_size;

    cfg1 = SPI_READ(base, SPI_REG_CFG1);
    cfg1 &= ~(SPI_CFG1_PACKET_LENGTH_MASK + SPI_CFG1_PACKET_LOOP_MASK);
    cfg1 |= (packet_size - 1) << SPI_CFG1_PACKET_LENGTH_OFFSET;
    cfg1 |= (packet_loop - 1) << SPI_CFG1_PACKET_LOOP_OFFSET;
    SPI_WRITE(base, SPI_REG_CFG1, cfg1);

    return 0;
}

static void inline SpiStartTransfer(uint32_t base)
{
    uint32_t reg_val;
    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val |= 1 << SPI_CMD_ACT_OFFSET;

    /*All register must be prepared before setting the start bit [SMP]*/
    SPI_WRITE(base, SPI_REG_CMD, reg_val);

    return;
}

/*
*  SpiChipConfig: used to define per data length and loop count
* @ ptr : HW config setting from User
*/
void SpiChipConfig(uint32_t id, struct mt_chip_conf *ptr)
{
    struct mt_chip_conf *ChipConfig = &chip_config_default;

    if (NULL == ptr) { //default
        ChipConfig->setuptime = 3;
        ChipConfig->holdtime = 3;
        ChipConfig->high_time = 10;
        ChipConfig->low_time = 10;
        ChipConfig->cs_idletime = 2;
        ChipConfig->ulthgh_thrsh = 0;

        ChipConfig->cpol = 0;
        ChipConfig->cpha = 0;
        ChipConfig->cs_pol = 0;
        ChipConfig->sample_sel = 0;

        ChipConfig->rx_mlsb = 1;
        ChipConfig->tx_mlsb = 1;

        ChipConfig->tx_endian = 0;
        ChipConfig->rx_endian = 0;

        ChipConfig->com_mod = FIFO_TRANSFER;
        ChipConfig->pause = 0;
        ChipConfig->finish_intr = 1;
        ChipConfig->deassert = 0;
        ChipConfig->ulthigh = 0;
        ChipConfig->tckdly = 0;
    } else {
        ChipConfig->setuptime = ptr->setuptime;
        ChipConfig->holdtime = ptr->holdtime;
        ChipConfig->high_time = ptr->high_time;
        ChipConfig->low_time = ptr->low_time;
        ChipConfig->cs_idletime = ptr->cs_idletime;
        ChipConfig->ulthgh_thrsh = ptr->ulthgh_thrsh;

        ChipConfig->cpol = ptr->cpol;
        ChipConfig->cpha = ptr->cpha;

        ChipConfig->rx_mlsb = ptr->rx_mlsb;
        ChipConfig->tx_mlsb = ptr->tx_mlsb ;

        ChipConfig->tx_endian = ptr->tx_endian;
        ChipConfig->rx_endian = ptr->rx_endian;

        ChipConfig->com_mod = ptr->com_mod;
        ChipConfig->pause = ptr->pause;
        ChipConfig->finish_intr = ptr->finish_intr;
        ChipConfig->deassert = ptr->deassert;
        ChipConfig->ulthigh = ptr->ulthigh;
        ChipConfig->tckdly = ptr->tckdly ;

    }

    //dump_chip_config(ChipConfig);
    SetChipConfig(id, ChipConfig);
}

static void inline SpiDisableDma(uint32_t base)
{
    uint32_t  cmd;

    cmd = SPI_READ(base, SPI_REG_CMD);
    cmd &= ~SPI_CMD_TX_DMA_MASK;
    cmd &= ~SPI_CMD_RX_DMA_MASK;
    SPI_WRITE(base, SPI_REG_CMD, cmd);
}

#define INVALID_DMA_ADDRESS 0xffffffff

static inline void SpiEnableDma(struct spi_transfer *xfer, uint32_t mode)
{
    uint32_t cmd;
    uint32_t base = xfer->base;

    cmd = SPI_READ(base, SPI_REG_CMD);
#define SPI_4B_ALIGN 0x4
    /* set up the DMA bus address */
    if ((mode == DMA_TRANSFER) || (mode == OTHER1)) {
        if ((xfer->tx_buf != NULL) || ((xfer->tx_dma != INVALID_DMA_ADDRESS) && (xfer->tx_dma != 0))) {
            if (xfer->tx_dma & (SPI_4B_ALIGN - 1)) {
                SPI_ERR("Warning!Tx_DMA address should be 4Byte alignment,buf:%p,dma:%x\n",
                        xfer->tx_buf, xfer->tx_dma);
            }
            SPI_WRITE(base, SPI_REG_TX_SRC, xfer->tx_dma);
            cmd |= 1 << SPI_CMD_TX_DMA_OFFSET;
        }
    }
    if ((mode == DMA_TRANSFER) || (mode == OTHER2)) {
        if ((xfer->rx_buf != NULL) || ((xfer->rx_dma != INVALID_DMA_ADDRESS) && (xfer->rx_dma != 0))) {
            if (xfer->rx_dma & (SPI_4B_ALIGN - 1)) {
                SPI_ERR("Warning!Rx_DMA address should be 4Byte alignment,buf:%p,dma:%x\n",
                        xfer->rx_buf, xfer->rx_dma);
            }
            SPI_WRITE(base, SPI_REG_RX_DST, xfer->rx_dma);
            cmd |= 1 << SPI_CMD_RX_DMA_OFFSET;
        }
    }
    SPI_WRITE(base, SPI_REG_CMD, cmd);
}

static void  inline SpiResumeTransfer(uint32_t base)
{
    uint32_t reg_val;

    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val &= ~SPI_CMD_RESUME_MASK;
    reg_val |= 1 << SPI_CMD_RESUME_OFFSET;
    /*All register must be prepared before setting the start bit [SMP]*/
    SPI_WRITE(base, SPI_REG_CMD, reg_val);

    return;
}

void ResetSpi(uint32_t base)
{
    uint32_t reg_val;

    /*set the software reset bit in SPI_REG_CMD.*/
    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val &= ~SPI_CMD_RST_MASK;
    reg_val |= 1 << SPI_CMD_RST_OFFSET;
    SPI_WRITE(base, SPI_REG_CMD, reg_val);

    reg_val = SPI_READ(base, SPI_REG_CMD);
    reg_val &= ~SPI_CMD_RST_MASK;
    SPI_WRITE(base, SPI_REG_CMD, reg_val);

    SPI_MSG("ResetSpi!\n");

    return;
}

/*
* Write chip configuration to HW register
*/
int32_t SpiSetup(struct spi_transfer *xfer)
{
    uint32_t reg_val, base;
    struct mt_chip_conf *chip_config = xfer->chip_config;

    base = xfer->base;

    if (chip_config == NULL) {
        SPI_MSG("SpiSetup chip_config is NULL !!\n");
    } else {
        /*set the timing*/
        reg_val = SPI_READ(base, SPI_REG_CFG2);
        reg_val &= ~(SPI_CFG2_SCK_HIGH_MASK | SPI_CFG2_SCK_LOW_MASK);
        reg_val |= ((chip_config->high_time - 1) << SPI_CFG2_SCK_HIGH_OFFSET);
        reg_val |= ((chip_config->low_time - 1) << SPI_CFG2_SCK_LOW_OFFSET);
        SPI_WRITE(base, SPI_REG_CFG2, reg_val);

        reg_val = SPI_READ(base, SPI_REG_CFG0);
        reg_val &= ~(SPI_CFG0_CS_HOLD_MASK | SPI_CFG0_CS_SETUP_MASK);
        reg_val |= ((chip_config->holdtime - 1) << SPI_CFG0_CS_HOLD_OFFSET);
        reg_val |= ((chip_config->setuptime - 1) << SPI_CFG0_CS_SETUP_OFFSET);
        SPI_WRITE(base, SPI_REG_CFG0, reg_val);

        reg_val = SPI_READ(base, SPI_REG_CFG1);
        reg_val &= ~(SPI_CFG1_CS_IDLE_MASK);
        reg_val |= ((chip_config->cs_idletime - 1) << SPI_CFG1_CS_IDLE_OFFSET);

        reg_val &= ~(SPI_CFG1_GET_TICK_DLY_MASK);
        reg_val |= ((chip_config->tckdly) << SPI_CFG1_GET_TICK_DLY_OFFSET);
        SPI_WRITE(base, SPI_REG_CFG1, reg_val);

        /*config CFG1 bit[28:26] is 0*/
        reg_val = SPI_READ(base, SPI_REG_CFG1);
        reg_val &= ~(0x7 << 26);
        SPI_WRITE(base, SPI_REG_CFG1, reg_val);

        SPI_WRITE(base, SPI_REG_PAD_SEL, 0x0);

        /*set the mlsbx and mlsbtx*/
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~(SPI_CMD_TX_ENDIAN_MASK | SPI_CMD_RX_ENDIAN_MASK);
        reg_val &= ~(SPI_CMD_TXMSBF_MASK | SPI_CMD_RXMSBF_MASK);
        reg_val &= ~(SPI_CMD_CPHA_MASK | SPI_CMD_CPOL_MASK);
        reg_val &= ~(SPI_CMD_SAMPLE_SEL_MASK | SPI_CMD_CS_POL_MASK);
        reg_val |= (chip_config->tx_mlsb << SPI_CMD_TXMSBF_OFFSET);
        reg_val |= (chip_config->rx_mlsb << SPI_CMD_RXMSBF_OFFSET);
        reg_val |= (chip_config->tx_endian << SPI_CMD_TX_ENDIAN_OFFSET);
        reg_val |= (chip_config->rx_endian << SPI_CMD_RX_ENDIAN_OFFSET);
        reg_val |= (chip_config->cpha << SPI_CMD_CPHA_OFFSET);
        reg_val |= (chip_config->cpol << SPI_CMD_CPOL_OFFSET);
        reg_val |= (chip_config->sample_sel << SPI_CMD_SAMPLE_SEL_OFFSET);
        reg_val |= (chip_config->cs_pol << SPI_CMD_CS_POL_OFFSET);
        SPI_WRITE(base, SPI_REG_CMD, reg_val);

        /*set pause mode*/
#ifdef SPI_TRANSFER_POLLING
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~SPI_CMD_PAUSE_EN_MASK;
        reg_val &= ~ SPI_CMD_PAUSE_IE_MASK; /*disable pause IE in polling mode*/

        reg_val |= (chip_config->pause << SPI_CMD_PAUSE_EN_OFFSET);
        SPI_WRITE(base, SPI_REG_CMD, reg_val);
#else
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~SPI_CMD_PAUSE_EN_MASK;
        reg_val &= ~ SPI_CMD_PAUSE_IE_MASK;
        reg_val |= (chip_config->pause << SPI_CMD_PAUSE_EN_OFFSET);
        reg_val |= (chip_config->pause << SPI_CMD_PAUSE_IE_OFFSET);
        SPI_WRITE(base, SPI_REG_CMD, reg_val);
#endif

#ifdef SPI_TRANSFER_POLLING
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~ SPI_CMD_FINISH_IE_MASK;/*disable finish IE in polling mode*/
        SPI_WRITE(base, SPI_REG_CMD, reg_val);
#else
        /*set finish interrupt always enable*/
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~ SPI_CMD_FINISH_IE_MASK;
        reg_val |= (1 << SPI_CMD_FINISH_IE_OFFSET);
        SPI_WRITE(base, SPI_REG_CMD, reg_val);
#endif

        /*set the communication of mode*/
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~ SPI_CMD_TX_DMA_MASK;
        reg_val &= ~ SPI_CMD_RX_DMA_MASK;
        SPI_WRITE(base, SPI_REG_CMD, reg_val);

        /*set deassert mode*/
        reg_val = SPI_READ(base, SPI_REG_CMD);
        reg_val &= ~SPI_CMD_DEASSERT_MASK;
        reg_val |= (chip_config->deassert << SPI_CMD_DEASSERT_OFFSET);
        SPI_WRITE(base, SPI_REG_CMD, reg_val);
    }
    return 0;
}

#ifdef SPI_TRANSFER_POLLING
static void SpiRecvCheck(struct spi_transfer *xfer)
{
    uint32_t cnt, i;

    cnt = (xfer->len % 4) ? (xfer->len / 4 + 1) : (xfer->len / 4);
    for (i = 0; i < cnt; i++) {
        if (*((uint32_t *) xfer->rx_buf + i) != *((uint32_t *) xfer->tx_buf + i)) {
            SPI_ERR("Error!! tx xfer %d is:%x\n", i,  *((uint32_t *) xfer->tx_buf + i));
            SPI_ERR("Error!! rx xfer %d is:%x\n", i,  *((uint32_t *) xfer->rx_buf + i));
        }
    }
}

static void ScpSpi0HandleIRQ(void)
{

    struct spi_transfer xfer_intr00;
    struct mt_chip_conf chip_config_intr00;
    struct spi_transfer *xfer_intr = &xfer_intr00;
    struct mt_chip_conf *chip_config_intr = &chip_config_intr00;
    uint32_t reg_val, cnt;
    uint32_t i;
    uint32_t base = SPI_BASE0;
    uint32_t id = 0 ;

    SPI_MSG(" SpiHandleIRQ\n");

    xfer_intr = GetSpiTransfer(id);
    chip_config_intr = GetChipConfig(id);

    if (NULL == chip_config_intr) {
        SPI_ERR("chip_config is NULL!!\n");
        return;
    }

    /*pause mode*/
    if (chip_config_intr->pause) {
        if (GetPauseStatus(id) == INPROGRESS) {
            SetPauseStatus(id, PAUSED);
            SPI_MSG("IRQ set PAUSED state\n");
        } else {
            SPI_ERR("Wrong spi status.\n");
        }
    } else {
        SetPauseStatus(id, IDLE);
        SPI_MSG("IRQ set IDLE state\n");
    }

    SPI_MSG("start to read data !!\n");
    /*FIFO*/
    if ((chip_config_intr->com_mod == FIFO_TRANSFER) && xfer_intr->rx_buf) {
        SPI_MSG("xfer->len:%d\n", xfer_intr->len);
        cnt = (xfer_intr->len % 4) ? (xfer_intr->len / 4 + 1) : (xfer_intr->len / 4);
        for (i = 0; i < cnt; i++) {
            reg_val = SPI_READ(base, SPI_REG_RX_DATA); /*get the data from rx*/
            SPI_MSG("SPI_RX_DATA_REG:0x%x\n", reg_val);
            *((uint32_t *)xfer_intr->rx_buf + i) = reg_val;
        }
    }

    if (chip_config_intr->com_mod == DMA_TRANSFER)
        SpiRecvCheck(xfer_intr);

    if (xfer_intr->is_last_xfer == 1 && xfer_intr->is_transfer_end == 1) {
        SetPauseStatus(id, IDLE);
        ResetSpi(base);
        SPI_MSG("Pause set IDLE state & disable clk\n");
    }

    /*set irq flag to ask SpiNextMessage continue to run*/

    SetIrqFlag(id, IRQ_IDLE);

    SPI_MSG("ScpSpi0HandleIRQ-----------xfer end-------\n");
    return;
}

static void ScpSpi4HandleIRQ(void)
{

    struct spi_transfer xfer_intr00;
    struct mt_chip_conf chip_config_intr00;
    struct spi_transfer *xfer_intr = &xfer_intr00;
    struct mt_chip_conf *chip_config_intr = &chip_config_intr00;
    uint32_t reg_val, cnt;
    uint32_t i;
    uint32_t base = SPI_BASE4;
    uint32_t id = 4 ;

    SPI_MSG(" SpiHandleIRQ\n");

    xfer_intr = GetSpiTransfer(id);
    chip_config_intr = GetChipConfig(id);

    if (NULL == chip_config_intr) {
        SPI_ERR("chip_config is NULL!!\n");
        return;
    }

    /*pause mode*/
    if (chip_config_intr->pause) {
        if (GetPauseStatus(id) == INPROGRESS) {
            SetPauseStatus(id, PAUSED);
            SPI_MSG("IRQ set PAUSED state\n");
        } else {
            SPI_ERR("Wrong spi status.\n");
        }
    } else {
        SetPauseStatus(id, IDLE);
        SPI_MSG("IRQ set IDLE state\n");
    }

    SPI_MSG("start to read data !!\n");
    /*FIFO*/
    if ((chip_config_intr->com_mod == FIFO_TRANSFER) && xfer_intr->rx_buf) {
        SPI_MSG("xfer->len:%d\n", xfer_intr->len);
        cnt = (xfer_intr->len % 4) ? (xfer_intr->len / 4 + 1) : (xfer_intr->len / 4);
        for (i = 0; i < cnt; i++) {
            reg_val = SPI_READ(base, SPI_REG_RX_DATA); /*get the data from rx*/
            SPI_MSG("SPI_RX_DATA_REG:0x%x\n", reg_val);
            *((uint32_t *)xfer_intr->rx_buf + i) = reg_val;
        }
    }

    if (chip_config_intr->com_mod == DMA_TRANSFER)
        SpiRecvCheck(xfer_intr);

    if (xfer_intr->is_last_xfer == 1 && xfer_intr->is_transfer_end == 1) {
        SetPauseStatus(id, IDLE);
        ResetSpi(base);
        SPI_MSG("Pause set IDLE state & disable clk\n");
    }

    /*set irq flag to ask SpiNextMessage continue to run*/

    SetIrqFlag(id, IRQ_IDLE);

    SPI_MSG("ScpSpi4HandleIRQ-----xfer end-------\n");
    return;
}

static void ScpSpi5HandleIRQ(void)
{
    struct spi_transfer xfer_intr00;
    struct mt_chip_conf chip_config_intr00;
    struct spi_transfer *xfer_intr = &xfer_intr00;
    struct mt_chip_conf *chip_config_intr = &chip_config_intr00;
    uint32_t reg_val, cnt;
    uint32_t i;
    uint32_t base = SPI_BASE5;
    uint32_t id = 5 ;

    SPI_MSG(" SpiHandleIRQ\n");

    xfer_intr = GetSpiTransfer(id);
    chip_config_intr = GetChipConfig(id);

    if (NULL == chip_config_intr) {
        SPI_ERR("chip_config is NULL!!\n");
        return;
    }

    /*pause mode*/
    if (chip_config_intr->pause) {
        if (GetPauseStatus(id) == INPROGRESS) {
            SetPauseStatus(id, PAUSED);
            SPI_MSG("IRQ set PAUSED state\n");
        } else {
            SPI_ERR("Wrong spi status.\n");
        }
    } else {
        SetPauseStatus(id, IDLE);
        SPI_MSG("IRQ set IDLE state\n");
    }

    SPI_MSG("start to read data !!\n");
    /*FIFO*/
    if ((chip_config_intr->com_mod == FIFO_TRANSFER) && xfer_intr->rx_buf) {
        SPI_MSG("xfer->len:%d\n", xfer_intr->len);
        cnt = (xfer_intr->len % 4) ? (xfer_intr->len / 4 + 1) : (xfer_intr->len / 4);
        for (i = 0; i < cnt; i++) {
            reg_val = SPI_READ(base, SPI_REG_RX_DATA); /*get the data from rx*/
            SPI_MSG("SPI_RX_DATA_REG:0x%x\n", reg_val);
            *((uint32_t *)xfer_intr->rx_buf + i) = reg_val;
        }
    }

    if (chip_config_intr->com_mod == DMA_TRANSFER)
        SpiRecvCheck(xfer_intr);

    if (xfer_intr->is_last_xfer == 1 && xfer_intr->is_transfer_end == 1) {
        SetPauseStatus(id, IDLE);
        ResetSpi(base);
        SPI_MSG("Pause set IDLE state & disable clk\n");
    }

    /*set irq flag to ask SpiNextMessage continue to run*/

    SetIrqFlag(id, IRQ_IDLE);

    SPI_MSG("ScpSpi5HandleIRQ-----xfer end-------\n");
    return;
}

static void SpiIrqHandler(struct spi_transfer *xfer)
{
    volatile uint32_t irqStatus = 0;
    uint32_t base = xfer->base;

    do {
        irqStatus = SPI_READ(base, SPI_REG_STATUS1) & 0x00000001;
    } while (irqStatus == 0);

    SPI_MSG("IRQ: irqStatus[0x%08x],xfer->id =%d\n", irqStatus, xfer->id);

    switch (xfer->id) {
        case 0:
            ScpSpi0HandleIRQ();
            break;
        case 4:
            ScpSpi4HandleIRQ();
            break;
        case 5:
            ScpSpi5HandleIRQ();
            break;
        default:
            SPI_ERR("invalid para: xfer->id=%d\n\r", xfer->id);
            break;
    }

}
#endif

int32_t SpiNextXfer(struct spi_transfer *xfer)
{
    uint32_t  mode, cnt, i;
    int ret = 0;

    struct mt_chip_conf *chip_config = GetChipConfig(xfer->id);

    if (chip_config == NULL) {
        SPI_ERR("SpiNextXfer get chip_config is NULL\n");
        return -EINVAL;
    }

    if ((!IsInterruptEnable(xfer->base))) {
        SPI_MSG("interrupt is disable\n");
    }

    mode = chip_config->com_mod;

    if ((mode == FIFO_TRANSFER)) {
        if (xfer->len > SPI_FIFO_SIZE) {
            ret = -ELEN;
            SPI_ERR("xfer len is invalid over fifo size\n");
            goto fail;
        }
    }

    /*
       * cannot 1K align & FIFO->DMA need used pause mode
       * this is to clear pause bit (CS turn to idle after data transfer done)
    */
    if (mode == DMA_TRANSFER) {
        if ((xfer->is_last_xfer == 1) && (xfer->is_transfer_end == 1))
            clear_pause_bit(xfer->base);
    } else if (mode == FIFO_TRANSFER) {
        if (xfer->is_transfer_end == 1)
            clear_pause_bit(xfer->base);
    } else {
        SPI_ERR("xfer mode is invalid !\n");
        ret = -EMODE;
        goto fail;
    }

    //SetPauseStatus(IDLE); //runing status, nothing about pause mode
    //disable DMA
    SpiDisableDma(xfer->base);

    /*need setting transfer data length & loop count*/
    ret = SpiSetupPacket(xfer);

    /*Using FIFO to send data*/
    if ((mode == FIFO_TRANSFER)) {
        cnt = (xfer->len % 4) ? (xfer->len / 4 + 1) : (xfer->len / 4);
        for (i = 0; i < cnt; i++) {
            SPI_WRITE(xfer->base, SPI_REG_TX_DATA, *((uint32_t *) xfer->tx_buf + i));
            SPI_MSG("tx_buf data is:%x\n", *((uint32_t *) xfer->tx_buf + i));
            SPI_MSG("tx_buf addr is:%p\n", (uint32_t *)xfer->tx_buf + i);
        }
    }
    /*Using DMA to send data*/
    if ((mode == DMA_TRANSFER)) {
        SpiEnableDma(xfer, mode);
    }
    SetSpiTransfer(xfer);

    SPI_MSG("xfer->id = %d, running=%d.\n", xfer->id, GetPauseStatus(xfer->id));

    if (GetPauseStatus(xfer->id) == PAUSED) { //running
        SPI_MSG("pause status to resume.\n");
        SetPauseStatus(xfer->id, INPROGRESS);   //set running status
        SpiResumeTransfer(xfer->base);
    } else if (GetPauseStatus(xfer->id) == IDLE) {
        SPI_MSG("The xfer start\n");
        /*if there is only one transfer, pause bit should not be set.*/
        if ((chip_config->pause)) { // need to think whether is last  msg <&& !is_last_xfer(msg,xfer)>
            SPI_MSG("set pause mode.\n");
            SetPauseBit(xfer->base);
        }
        /*All register must be prepared before setting the start bit [SMP]*/

        SetPauseStatus(xfer->id, INPROGRESS);   //set running status
        SpiStartTransfer(xfer->base);
        SPI_MSG("SpiStartTransfer\n");
    } else {
        SPI_ERR("Wrong status\n");
        ret = -ESTATUS;
        goto fail;
    }
    dump_reg_config(xfer->base);

#ifdef SPI_TRANSFER_POLLING
    SpiIrqHandler(xfer);
#endif

    /*exit pause mode*/
    if (GetPauseStatus(xfer->id) == PAUSED && xfer->is_last_xfer == 1) {
        ClearResumeBit(xfer->base);
    }
    return 0;
fail:
    SetPauseStatus(xfer->id, IDLE); //set running status
    ResetSpi(xfer->base);
    SetIrqFlag(xfer->id, IRQ_IDLE);
    return ret;
}

static void SpiNextMessage(struct spi_transfer *xfer)
{
    struct mt_chip_conf *chip_config;

    chip_config = GetChipConfig(xfer->id);
    if (chip_config == NULL) {
        SPI_ERR("SpiNextMessage get chip_config is NULL\n");
        return;
    }
    SpiSetup(xfer);
    SpiNextXfer(xfer);
}

int32_t SpiTransfer(struct spi_transfer *xfer)
{
    //struct mt_chip_conf *chip_config;

    /*wait intrrupt had been clear*/
    while (IRQ_BUSY == GetIrqFlag(xfer->id)) {
        /*need a pause instruction to avoid unknow exception*/
        SPI_MSG("wait IRQ handle finish\n");
    }

    /*set flag to block next transfer*/
    SetIrqFlag(xfer->id, IRQ_BUSY);

    /*Through xfer->len to select DMA Or FIFO mode*/
#ifdef SPI_AUTO_SELECT_MODE
    if (xfer) {
        /*if transfer len > 16, used DMA mode*/
        if (xfer->len > SPI_DATA_SIZE) {
            xfer->chip_config = GetChipConfig(xfer->id);
            xfer->chip_config->com_mod = DMA_TRANSFER;
            SetChipConfig(xfer.id, xfer.chip_config);
            SPI_MSG("data size > 32, select DMA mode !\n");
        }
    } else {
        SPI_ERR("xfer is NULL pointer. \n");
    }
#endif
    if ((!xfer)) {
        SPI_ERR("xfer is NULL pointer. \n");
        goto out;
    }
    if ((NULL == xfer)) {
        SPI_ERR("the xfer is NULL.\n");
        goto out;
    }

    if (!((xfer->tx_buf || xfer->rx_buf) && xfer->len)) {
        SPI_ERR("missing tx %p or rx %p buf, len%d\n", xfer->tx_buf, xfer->rx_buf, xfer->len);
        goto out;
    }

    if (xfer) {
        SpiNextMessage(xfer);
    }
    return 0;
out:
    return -EINVAL;

}

void config_spi_base(struct spi_transfer* spiData)
{
    switch (spiData->id) {
        case 0:
            spiData->base = SPI_BASE0;
            break;
        case 4:
            spiData->base = SPI_BASE4;
            break;
        case 5:
            spiData->base = SPI_BASE5;
            break;
        default:
            SPI_ERR("invalid para: spi->id=%d\n\r", spiData->id);
            break;
    }
}

int32_t spi_handle(struct spi_transfer* spiData)
{

    struct spi_transfer     xfer;
    struct spi_transfer    *xfer_p = &xfer;

    uint32_t packet_loop, rest_size;

    config_spi_base(spiData);

    xfer_p->tx_buf = spiData->tx_buf;
    xfer_p->rx_buf = spiData->rx_buf;

    xfer.is_last_xfer = 1;

    xfer.id = spiData->id;
    xfer.base = spiData->base;

    xfer.rx_buf = xfer_p->rx_buf;
    xfer.tx_buf = xfer_p->tx_buf;
    xfer.tx_dma = spiData->tx_dma;
    xfer.rx_dma = spiData->rx_dma;
    xfer.len = spiData->len;
    xfer.is_dma_used = spiData->is_dma_used;
    xfer.is_transfer_end = spiData->is_transfer_end;

    Spi_enable_clk(xfer.id);
    spi_gpio_set(xfer.id);

    SPI_MSG("tx_dma=0x%x,rx_dma=0x%x\n", xfer.tx_dma , xfer.rx_dma);
    SPI_MSG("xfer.len=%d, xfer.is_dma_used=%d\n ", xfer.len, xfer.is_dma_used);
    SPI_MSG("IRQ status=%d\n ", GetIrqFlag(xfer.id));
    while (IRQ_BUSY == GetIrqFlag(xfer.id)) {
        /*need a pause instruction to avoid unknow exception*/
        SPI_MSG("IPC wait IRQ handle finish\n");
    }

    xfer.chip_config = spiData->chip_config;

    SpiChipConfig(xfer.id, xfer.chip_config);


#ifndef NO_DMA_LENGTH_LIMIT
    packet_loop = spiData->len / 1024;
    rest_size = spiData->len % 1024;
    SPI_MSG("packet_loop=%d,rest_size=%d\n", packet_loop, rest_size);

    if (xfer.len <= 1024 || rest_size == 0) {
        SpiTransfer(&xfer);
    } else {
        xfer.chip_config->pause = 1;
        SetChipConfig(xfer.id, xfer.chip_config);
        xfer.is_last_xfer = 0;
        xfer.len = 1024 * packet_loop;
        SpiTransfer(&xfer);

        xfer.chip_config->pause = 0;
        SetChipConfig(xfer.id, xfer.chip_config);
        xfer.is_last_xfer = 1;
        xfer.len = rest_size;
        xfer.rx_buf = xfer_p->rx_buf + 1024 * packet_loop ;
        xfer.tx_buf = xfer_p->tx_buf + 1024 * packet_loop ;

        SpiTransfer(&xfer);
        xfer.rx_buf = xfer_p->rx_buf;
    }
#else
    SpiTransfer(&xfer);
#endif
    Spi_disable_clk(xfer.id);

    return 0;
}

uint32_t SpiSend(struct spi_transfer* spiData)
{
    uint32_t ret;

    static struct spi_transfer* xfer;

    xfer->id = spiData->id;
    xfer->len = spiData->len;
    xfer->tx_buf = spiData->tx_buf;
    xfer->rx_buf = spiData->rx_buf;
    xfer->tx_dma = spiData->tx_dma;
    xfer->rx_dma = spiData->rx_dma;
    xfer->is_transfer_end = spiData->is_transfer_end;
    xfer->chip_config = spiData->chip_config;
    xfer->is_dma_used = spiData->is_dma_used;

    if (spiData->chip_config != NULL) {
        if (DMA_TRANSFER == (xfer->chip_config->com_mod)) {
            xfer->is_dma_used = 1;
        } else {
            xfer->is_dma_used = 0;
        }
    }
    xfer->is_dma_used = 0;
    xfer->is_transfer_end = 1;
    xfer->chip_config = &mt_chip_conf_fifo_test;

    ret = spi_handle(xfer);
    return ret;
}


#if SPI_DMA
#define SPI_DATA_LENGTH     16642
static uint32_t spi_tx_buf[SPI_DATA_LENGTH];
static uint32_t spi_rx_buf[SPI_DATA_LENGTH];
#endif

void Spitest(void)
{
    uint32_t ret = 0;
    struct spi_transfer xfer;

    uint32_t send_data = 0x12341234;
    uint32_t recv_data = 0;

    SPI_MSG("Spitest Start\n");

    xfer.tx_buf = &send_data;
    xfer.rx_buf = &recv_data;
    xfer.len = 4;
    xfer.id = 0;
    xfer.is_dma_used = 0;
    xfer.is_transfer_end = 1;
    xfer.chip_config = &mt_chip_conf_fifo_test;
    ret = spi_handle(&xfer);

    xfer.tx_buf = &send_data;
    xfer.rx_buf = &recv_data;
    xfer.len = 4;
    xfer.id = 5;
    xfer.is_dma_used = 0;
    xfer.is_transfer_end = 1;
    xfer.chip_config = &mt_chip_conf_fifo_test;
    ret = spi_handle(&xfer);

    SPI_MSG("Spitest End!! ret = %d\n", ret);
}

void mt_init_spi()
{
    spi_gpio_set(0);
    spi_gpio_set(4);
    spi_gpio_set(5);
}

