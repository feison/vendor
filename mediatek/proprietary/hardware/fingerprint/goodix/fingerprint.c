/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define LOG_TAG "FingerprintHal"

#include <errno.h>
#include <malloc.h>
#include <string.h>
#include <cutils/log.h>
#include <hardware/hardware.h>
#include <hardware/fingerprint.h>

#include "gf_hal.h"

#define MAX_FAILED_ATTEMPTS 5

static int fingerprint_close(hw_device_t *dev) {
    if (dev) {
        gf_hal_exit(dev);
        free(dev);
        return 0;
    } else {
        return -1;
    }
}


static uint64_t fingerprint_pre_enroll(struct fingerprint_device *dev) {
    return gf_hal_pre_enroll(dev);
}

static int fingerprint_enroll(struct fingerprint_device *dev,
                                const hw_auth_token_t *hat,
                                uint32_t gid,
                                uint32_t timeout_sec) {
    return gf_hal_enroll(dev, hat, gid, timeout_sec);
}

static int fingerprint_post_enroll(struct fingerprint_device *dev) {
    return gf_hal_post_enroll(dev);
}

static uint64_t fingerprint_get_auth_id(struct fingerprint_device *dev) {
    return gf_hal_get_auth_id(dev);
}

static int fingerprint_cancel(struct fingerprint_device *dev) {
    return gf_hal_cancel(dev);
}

static int fingerprint_remove(struct fingerprint_device *dev,
                                uint32_t gid, uint32_t fid) {
    return gf_hal_remove(dev, gid, fid);
}

static int fingerprint_set_active_group(struct fingerprint_device *dev,
                                        uint32_t gid, const char __unused *store_path) {
    return gf_hal_set_active_group(dev, gid);
}

static int fingerprint_authenticate(struct fingerprint_device *dev,
                                    uint64_t operation_id, uint32_t gid) {
    return gf_hal_authenticate(dev, operation_id, gid);
}

static int set_notify_callback(struct fingerprint_device *dev, fingerprint_notify_t notify) {
    /* Decorate with locks */
    dev->notify = notify;
    return 0;
}

static int fingerprint_open(const hw_module_t* module, const char __unused *id,
                            hw_device_t** device) {
    if (device == NULL) {
        ALOGE("NULL device on open");
        return -EINVAL;
    }

    fingerprint_device_t *dev = malloc(sizeof(fingerprint_device_t));
    memset(dev, 0, sizeof(fingerprint_device_t));

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = FINGERPRINT_MODULE_API_VERSION_2_0;
    dev->common.module = (struct hw_module_t*) module;
    dev->common.close = fingerprint_close;

    dev->pre_enroll = fingerprint_pre_enroll;
    dev->enroll = fingerprint_enroll;
    dev->post_enroll = fingerprint_post_enroll;
    dev->get_authenticator_id = fingerprint_get_auth_id;
    dev->cancel = fingerprint_cancel;
    dev->remove = fingerprint_remove;
    dev->set_active_group = fingerprint_set_active_group;
    dev->authenticate = fingerprint_authenticate;
    dev->set_notify = set_notify_callback;
    dev->notify = NULL;

    *device = (hw_device_t *) dev;

    gf_error_t err = gf_hal_init(dev, MAX_FAILED_ATTEMPTS);
    if (GF_SUCCESS != err) {
        return -EINVAL;
    }

    return 0;
}

static struct hw_module_methods_t fingerprint_module_methods = {
    .open = fingerprint_open,
};

fingerprint_module_t HAL_MODULE_INFO_SYM = {
    .common = {
        .tag                = HARDWARE_MODULE_TAG,
        .module_api_version = FINGERPRINT_MODULE_API_VERSION_2_0,
        .hal_api_version    = HARDWARE_HAL_API_VERSION,
        .id                 = FINGERPRINT_HARDWARE_MODULE_ID,
        .name               = "Demo Fingerprint HAL",
        .author             = "The Android Open Source Project",
        .methods            = &fingerprint_module_methods,
    },
};
