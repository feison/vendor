/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

//*****************************************************************************
// [File] mtk_gps_auto_debug.h
// [Version] v1.0
// [Revision Date] 2013-11-04
// [Author]  xxxx@mediatek.com, 67612
// [Description]
//*****************************************************************************

#ifndef MTK_GPS_AUTO_DEBUG_H
#define MTK_GPS_AUTO_DEBUG_H


#ifdef SUPPORT_AUTO_DEBUG_SYSTEM
/*macro define start here*/
#define MAX_GENERAL_TYPE_LENGTH           4
#define MAX_MSG_TYPE_LENGTH                   3
#define MAX_GER_TYPE_LENGTH                    3
#define GPS_SV_ID    32
#define MAX_COLLECT_COUNT  10
#define SV_EXSIT_BITMASK  0x00000000
#define PMTKADB_RELATION_BITMASK  0x0000
#define HIGH_LEVEL_SNR_STATE  42
#define NORMAL_LEVEL_SNR_STATE  35
#define LOW_LEVEL_SNR_STATE  30
#define POOR_LEVEL_SNR_STATE  25
#define PMTKADB_OUTPUT_FMT  "PMTKADB,%c,%s,%d,%d,%d,%d"
#define NEED_SHOW_WARNING_RETRY_COUNT  2
#define TESTER_MAX_MOVING_SPEED       6  // 6m/s  it means  21.6km/h
#define GOOD_CLK_DRFIT_RATE  2.5      //means 2.5 ppb/s
#define POOR_CLK_DRFIT_RATE  10       //means 10ppb/s
#define RESERVED_NO_USING ' '
#define INIT_SNR_VALUE  99


/*enum struct  define start here*/
/*add new test case between start and end in general type */
/*carefor the start and end item */
typedef enum
{
    GPS_AUTO_DEBUG_CASE_SAT_START= 0,
    GPS_AUTO_DEBUG_CASE_SAT_CNR = GPS_AUTO_DEBUG_CASE_SAT_START,            // 0
    GPS_AUTO_DEBUG_CASE_SAT_DBCR,                                                                           //  1
    GPS_AUTO_DEBUG_CASE_SAT_SUBFRAME,                                                                  // 2
    GPS_AUTO_DEBUG_CASE_SAT_END = GPS_AUTO_DEBUG_CASE_SAT_SUBFRAME,    // 2

    GPS_AUTO_DEBUG_CASE_TRA_START,                                                                                // 3
    GPS_AUTO_DEBUG_CASE_TRA_BOOT_RETRY = GPS_AUTO_DEBUG_CASE_TRA_START,    // 3
    GPS_AUTO_DEBUG_CASE_TRA_STP_LOSE_DATA_FROM_DSP_2_MNL,                                // 4
    GPS_AUTO_DEBUG_CASE_TRA_MNL_LOSE_DBTT,                                                                // 5
    GPS_AUTO_DEBUG_CASE_TRA_STP_LOSE_DATA,                                                                // 6
    GPS_AUTO_DEBUG_CASE_TRA_END = GPS_AUTO_DEBUG_CASE_TRA_STP_LOSE_DATA,   //6

    GPS_AUTO_DEBUG_CASE_CLK_START,                                                                                  // 7
    GPS_AUTO_DEBUG_CASE_CLK_DRIFT_RATE = GPS_AUTO_DEBUG_CASE_CLK_START,       //7
    GPS_AUTO_DEBUG_CASE_CLK_END =  GPS_AUTO_DEBUG_CASE_CLK_DRIFT_RATE,          //7
    GPS_AUTO_DEBUG_CASE_MAX
}GPS_AUTO_DEBUG_TOTAL_ITEM_CASE;

typedef enum
{
    GPS_AUTO_DEBUG_EXIT_INIT_VALUE = 0,
    GPS_AUTO_DEBUG_EXIT_ERR_PMTKADB_NULL_POINT = 0xF0,
    GPS_AUTO_DEBUG_EXIT_ERR_GET_PVT_FAILED = 0xF1,
    GPS_AUTO_DEBUG_EXIT_ERR_OUT_US_PERMISSION=0xF2,
    GPS_AUTO_DEBUG_EXIT_INFO_TURN_OFF=0xF3,
    GPS_AUTO_DEBUG_EXIT_INFO_PROCESS_FAILED= 0xF4,
    GPS_AUTO_DEBUG_EXIT_INFO_MOVING_TOO_FAST= 0xF5,
    GPS_AUTO_DEBUG_EXIT_INFO_TEST_CASE_GOOD= 0xF6,
    GPS_AUTO_DEBUG_EXIT_MAX = 0xFF
}GPS_AUTO_DEBUG_TOTAL_EXIT_CASE;

typedef enum
{
    GPS_AUTO_DEBUG_INFO_ERR = 0,
    GPS_AUTO_DEBUG_INFO_WARNING,
    GPS_AUTO_DEBUG_INFO_INFO,
    GPS_AUTO_DEBUG_INFO_UNKN,    //add new iNFOTYPE before it
    GPS_AUTO_DEBUG_INFO_MAX
}GPS_AUTO_DEBUG_TOTAL_INFO_CASE;

typedef enum
{
    GPS_AUTO_DEBUG_GERTYPE_SAT= 0,
    GPS_AUTO_DEBUG_GERTYPE_TRA,
    GPS_AUTO_DEBUG_GERTYPE_CLK,
    GPS_AUTO_DEBUG_GERTYPE_UNK,   //add new GERTYPE before it
    GPS_AUTO_DEBUG_GERTYPE_MAX
}GPS_AUTO_DEBUG_TOTAL_GERTYPE_CASE;

typedef enum
{
    GPS_AUTO_DEBUG_HOT_RESTART= 0,
    GPS_AUTO_DEBUG_WARM_RESTART,
    GPS_AUTO_DEBUG_COLD_RESTART,
    GPS_AUTO_DEBUG_FULL_RESTART,   //add new GERTYPE before it
    GPS_AUTO_DEBUG_RESTART_MAX
}GPS_AUTO_DEBUG_TOTAL_RESTART_CASE;

typedef enum
{
    GPS_AUTO_DEBUG_RECORD_QUIET= 0,            // run quiet,  left no record
    GPS_AUTO_DEBUG_RECORD_UI_AND_DEBUG,  // issue happend, need  record by ui+ debug log
    GPS_AUTO_DEBUG_RECORD_DEBUG,                // issue happend, need  debug log
    GPS_AUTO_DEBUG_RECORD_MAX
}GPS_AUTO_DEBUG_TOTAL_RECORD_CASE;


/*data struct  define start here*/
typedef struct mtk_gps_auto_debug_pmtk_fmt
{
    char  msg_type;  // E = Error,  W= Warning I= Information, U= unkown
    char  *general_type;  //SAT, TRA, CLK, UNK
    mtk_int8 detail_type;   //detial type
    mtk_int16 debug_result;
    mtk_int8 reserved;
    GPS_AUTO_DEBUG_TOTAL_RECORD_CASE record_type; // 1 UI show, 0 UI disappear
    //mtk_int8 chek_sum;
}GPS_AUTO_DEBUG_PMTK_FMT;

typedef struct mtk_gps_auto_debug_sv_info
{
    mtk_int8 sv_id;
    mtk_int8 snr;
    mtk_int8 fg_record;
    //mtk_int8 gnss_type;
}GPS_AUTO_DEBUG_SV_INFO;


/*global symbols define start here*/
static char g_msg_type_arr[GPS_AUTO_DEBUG_INFO_MAX] ={'E','W','I','U'};
static char g_general_type_arr[GPS_AUTO_DEBUG_GERTYPE_MAX][MAX_GENERAL_TYPE_LENGTH] = {"STA","TRN","CLK","UNK"};

mtk_int8  g_count_auto_debug_snr;
mtk_int8  g_count_auto_debug_dbcr;
mtk_int8  g_count_auto_debug_subframe;
mtk_int8  g_count_dsp_p1_boot_retry;
mtk_int8  g_count_auto_debug_clk_drift;
float g_last_clk_drift;
float g_last_clk_drift_rate[MAX_COLLECT_COUNT];
mtk_int32 g_sv_exsit_bitmask;
//mtk_int8 g_fg_start_init;

GPS_AUTO_DEBUG_SV_INFO g_gps_sv_info[MAX_COLLECT_COUNT][GPS_SV_ID];
GPS_AUTO_DEBUG_SV_INFO g_gps_sv_process[GPS_SV_ID];

/* fucntion define start here*/
void mtk_gps_auto_debug_system_init(void);
void mtk_gps_auto_debug_system_run(void);
void mtk_gps_auto_debug_system_uninit(void);
void mtk_gps_auto_debug_sate_snr(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 *pmtkadb_bitmask);
void mtk_gps_auto_debug_sate_dbcr(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 *pmtkadb_bitmask);
void mtk_gps_auto_debug_sate_subfame(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 *pmtkadb_bitmask);
void mtk_gps_auto_debug_tra_boot_retry(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 *pmtkadb_bitmask);
void mtk_gps_auto_debug_clk_clk_drift_rate(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 *pmtkadb_bitmask);
void mtk_gps_auto_debug_check_relationship(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 pmtkadb_bitmask);
void mtk_gps_auto_debug_send_PMTKADB(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX], mtk_int16 pmtkadb_bitmask);
void mtk_gps_auto_debug_exit_hdlr(GPS_AUTO_DEBUG_PMTK_FMT pmtkadb[GPS_AUTO_DEBUG_CASE_MAX],  mtk_int8 case_idx, mtk_int8 exit_hdlr);
void mtk_gps_auto_debug_get_cur_gpgsv(mtk_int8  gpgsv[GPS_SV_ID], mtk_gps_position *pvt );
void mtk_gps_auto_debug_get_cur_clk_drift(mtk_gps_position *pvt , float *cur_clk_drift);
void mtk_gps_auto_debug_collect_snr(mtk_int8  gpgsv[GPS_SV_ID], mtk_int8 restart_type);
void mtk_gps_auto_debug_collect_clk_drift_rate(float cur_clk_drift);
mtk_int8  mtk_gps_auto_debug_get_restart_type(void);
mtk_int8  mtk_gps_auto_debug_process_snr( void);
mtk_int8  mtk_gps_auto_debug_process_clk_drift_rate( void);
mtk_bool  mtk_gps_auto_debug_check_sv_healthy(mtk_int8 index);
mtk_int8 mtk_gps_auto_debug_set_case_value(GPS_AUTO_DEBUG_TOTAL_ITEM_CASE case_idx, mtk_int8 case_value);
mtk_int8 mtk_gps_auto_debug_get_case_value(GPS_AUTO_DEBUG_TOTAL_ITEM_CASE case_idx, mtk_int8 *case_value);
#endif

#endif 

