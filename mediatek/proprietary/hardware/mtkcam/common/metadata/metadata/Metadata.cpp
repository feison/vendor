/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/******************************************************************************
 *
 ******************************************************************************/
#define LOG_TAG "MtkCam/Metadata"

#include <Log.h>
#include <utils/include/common.h>
//
#include <utils/Vector.h>
#include <utils/KeyedVector.h>
#include <stdio.h>
#include <stdlib.h>
using namespace NSCam;
using namespace android;
using namespace std;

//
#include <metadata/mtk_metadata_types.h>
//
#define get_mtk_metadata_tag_type(x) mType
//
/******************************************************************************
 *
 ******************************************************************************/
#if MTKCAM_HAVE_AEE_FEATURE == '1'
#include <aee.h>
#define AEE_ASSERT(String) \
    do { \
        CAM_LOGE("ASSERT("#String") fail"); \
        aee_system_exception( \
            "mtkcam/Metadata", \
            NULL, \
            DB_OPT_DEFAULT, \
            String); \
    } while(0)
#else
#define AEE_ASSERT(String)
#endif
/******************************************************************************
 *
 ******************************************************************************/
class IMetadata::IEntry::Implementor
{
private:
    Tag_t                           mTag;
    int                             mType;

public:     ////                    Instantiation.
    virtual                         ~Implementor();
                                    Implementor(Tag_t tag);
    Implementor&                    operator=(Implementor const& other);
                                    Implementor(Implementor const& other);

public:     ////                    Accessors.

    /**
     * Return the tag.
     */
    virtual MUINT32                 tag() const;

    /**
     * Return the type.
     */
    virtual MINT32                  type() const;

    /**
     * Check to see whether it is empty (no items) or not.
     */
    virtual MBOOL                   isEmpty() const;

    /**
     * Return the number of items.
     */
    virtual MUINT                   count() const;

    /**
     * Return how many items can be stored without reallocating the backing store.
     */
    virtual MUINT                   capacity() const;

    /**
     * Set the capacity.
     */
    virtual MBOOL                   setCapacity(MUINT size);

public:     ////                    Operations.

    /**
     * Clear all items.
     * Note: Copy-on write.
     */
    virtual MVOID                   clear();

    /**
     * Delete an item at a given index.
     * Note: Copy-on write.
     */
    virtual MERROR                  removeAt(MUINT index);


#define IMETADATA_IENTRY_OPS_DECLARATION(_T) \
    virtual MVOID                   push_back(_T const& item, Type2Type<_T>) \
                                    {  \
                                        mType = TYPE_##_T;\
                                        mStorage_##_T.push_back(item); \
                                    } \
    virtual _T&                     editItemAt(MUINT index, Type2Type<_T>){ return mStorage_##_T.editItemAt(index); } \
    virtual _T const&               itemAt(MUINT index, Type2Type<_T>) const { return reinterpret_cast<const _T&>(mStorage_##_T.itemAt(index)); } \
    Vector<_T>                      mStorage_##_T;

    IMETADATA_IENTRY_OPS_DECLARATION(MUINT8)
    IMETADATA_IENTRY_OPS_DECLARATION(MINT32)
    IMETADATA_IENTRY_OPS_DECLARATION(MFLOAT)
    IMETADATA_IENTRY_OPS_DECLARATION(MINT64)
    IMETADATA_IENTRY_OPS_DECLARATION(MDOUBLE)
    IMETADATA_IENTRY_OPS_DECLARATION(MRational)
    IMETADATA_IENTRY_OPS_DECLARATION(MPoint)
    IMETADATA_IENTRY_OPS_DECLARATION(MSize)
    IMETADATA_IENTRY_OPS_DECLARATION(MRect)
    IMETADATA_IENTRY_OPS_DECLARATION(IMetadata)
    IMETADATA_IENTRY_OPS_DECLARATION(Memory)

#undef  IMETADATA_IENTRY_OPS_DECLARATION

};

/******************************************************************************
 *
 ******************************************************************************/
#define RETURN_TYPE_OPS(_Type, _Ops) \
    _Type == TYPE_MUINT8 ? \
        mStorage_MUINT8._Ops : \
    _Type == TYPE_MINT32 ? \
        mStorage_MINT32._Ops : \
    _Type == TYPE_MFLOAT ? \
        mStorage_MFLOAT._Ops : \
    _Type == TYPE_MINT64 ? \
        mStorage_MINT64._Ops : \
    _Type == TYPE_MDOUBLE ? \
        mStorage_MDOUBLE._Ops : \
    _Type == TYPE_MRational ? \
        mStorage_MRational._Ops : \
    _Type == TYPE_MPoint ? \
        mStorage_MPoint._Ops : \
    _Type == TYPE_MSize ? \
        mStorage_MSize._Ops : \
    _Type == TYPE_Memory ? \
        mStorage_Memory._Ops : \
    _Type == TYPE_MRect ? \
        mStorage_MRect._Ops : \
        mStorage_IMetadata._Ops; \

#define SET_TYPE_OPS(_Type, _Ops, _Val) \
    _Type == TYPE_MUINT8 ? \
        mStorage_MUINT8._Ops(_Val) : \
    _Type == TYPE_MINT32 ? \
        mStorage_MINT32._Ops(_Val) : \
    _Type == TYPE_MFLOAT ? \
        mStorage_MFLOAT._Ops(_Val) : \
    _Type == TYPE_MINT64 ? \
        mStorage_MINT64._Ops(_Val) : \
    _Type == TYPE_MDOUBLE ? \
        mStorage_MDOUBLE._Ops(_Val) : \
    _Type == TYPE_MRational ? \
        mStorage_MRational._Ops(_Val) : \
    _Type == TYPE_MPoint ? \
        mStorage_MPoint._Ops(_Val) : \
    _Type == TYPE_MSize ? \
        mStorage_MSize._Ops(_Val) : \
    _Type == TYPE_Memory ? \
        mStorage_Memory._Ops(_Val) : \
    _Type == TYPE_MRect ? \
        mStorage_MRect._Ops(_Val) : \
        mStorage_IMetadata._Ops(_Val);

#define SRC_DST_OPERATOR(_Type, _Ops, _Src) \
    if(_Type == TYPE_MUINT8) \
        (mStorage_MUINT8 _Ops _Src.mStorage_MUINT8); \
    else if(_Type == TYPE_MINT32) \
        (mStorage_MINT32 _Ops _Src.mStorage_MINT32); \
    else if(_Type == TYPE_MFLOAT) \
        (mStorage_MFLOAT _Ops _Src.mStorage_MFLOAT); \
    else if(_Type == TYPE_MINT64) \
        (mStorage_MINT64 _Ops _Src.mStorage_MINT64); \
    else if(_Type == TYPE_MDOUBLE) \
        (mStorage_MDOUBLE _Ops _Src.mStorage_MDOUBLE); \
    else if(_Type == TYPE_MRational) \
        (mStorage_MRational _Ops _Src.mStorage_MRational); \
    else if(_Type == TYPE_MPoint) \
        (mStorage_MPoint _Ops _Src.mStorage_MPoint); \
    else if(_Type == TYPE_MSize) \
        (mStorage_MSize _Ops _Src.mStorage_MSize); \
    else if(_Type == TYPE_MRect) \
        (mStorage_MRect _Ops _Src.mStorage_MRect); \
    else if(_Type == TYPE_Memory) \
        (mStorage_Memory _Ops _Src.mStorage_Memory); \
    else if(_Type == TYPE_IMetadata) \
        (mStorage_IMetadata _Ops _Src.mStorage_IMetadata);
/******************************************************************************
 *
 ******************************************************************************/
IMetadata::IEntry::Implementor::
Implementor(Tag_t tag)
    : mTag(tag)
    , mType(-1)
#define STORAGE_DECLARATION(_T) \
    , mStorage_##_T()

    STORAGE_DECLARATION(MUINT8)
    STORAGE_DECLARATION(MINT32)
    STORAGE_DECLARATION(MFLOAT)
    STORAGE_DECLARATION(MINT64)
    STORAGE_DECLARATION(MDOUBLE)
    STORAGE_DECLARATION(MRational)
    STORAGE_DECLARATION(MPoint)
    STORAGE_DECLARATION(MSize)
    STORAGE_DECLARATION(MRect)
    STORAGE_DECLARATION(IMetadata)
    STORAGE_DECLARATION(Memory)

#undef STORAGE_DECLARATION
{
}


IMetadata::IEntry::Implementor::
Implementor(IMetadata::IEntry::Implementor const& other)
    : mTag(other.mTag)
    , mType(other.mType)
#define STORAGE_DECLARATION(_T) \
    , mStorage_##_T(other.mStorage_##_T)

    STORAGE_DECLARATION(MUINT8)
    STORAGE_DECLARATION(MINT32)
    STORAGE_DECLARATION(MFLOAT)
    STORAGE_DECLARATION(MINT64)
    STORAGE_DECLARATION(MDOUBLE)
    STORAGE_DECLARATION(MRational)
    STORAGE_DECLARATION(MPoint)
    STORAGE_DECLARATION(MSize)
    STORAGE_DECLARATION(MRect)
    STORAGE_DECLARATION(IMetadata)
    STORAGE_DECLARATION(Memory)

#undef STORAGE_DECLARATION
{
}


IMetadata::IEntry::Implementor&
IMetadata::IEntry::Implementor::
operator=(IMetadata::IEntry::Implementor const& other)
{
    if (this != &other)
    {
        mTag = other.mTag;
        mType = other.mType;
        SRC_DST_OPERATOR(get_mtk_metadata_tag_type(mTag), =, other);
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


IMetadata::IEntry::Implementor::
~Implementor()
{
}


MUINT32
IMetadata::IEntry::Implementor::
tag() const
{
    return mTag;
}

MINT32
IMetadata::IEntry::Implementor::
type() const
{
    return mType;
}

MBOOL
IMetadata::IEntry::Implementor::
isEmpty() const
{
    return get_mtk_metadata_tag_type(mTag) == -1 ?
           MTRUE : RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), isEmpty());
}


MUINT
IMetadata::IEntry::Implementor::
count() const
{
    return get_mtk_metadata_tag_type(mTag) == -1 ?
           0 : RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), size());
}


MUINT
IMetadata::IEntry::Implementor::
capacity() const
{
    return get_mtk_metadata_tag_type(mTag) == -1 ?
           0 : RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), capacity());
}


MBOOL
IMetadata::IEntry::Implementor::
setCapacity(MUINT size)
{
    MERROR ret = get_mtk_metadata_tag_type(mTag) == -1 ?
                 MFALSE : SET_TYPE_OPS(get_mtk_metadata_tag_type(mTag), setCapacity, size);
    return ret == NO_MEMORY ? MFALSE : ret;
}


MVOID
IMetadata::IEntry::Implementor::
clear()
{
      if (get_mtk_metadata_tag_type(mTag) != -1)
        RETURN_TYPE_OPS(get_mtk_metadata_tag_type(mTag), clear());
}


MERROR
IMetadata::IEntry::Implementor::
removeAt(MUINT index)
{
    MERROR ret = get_mtk_metadata_tag_type(mTag) == -1 ?
                  BAD_VALUE : SET_TYPE_OPS(get_mtk_metadata_tag_type(mTag), removeAt, index);
    return ret == BAD_VALUE ? BAD_VALUE : OK;
}


#undef RETURN_TYPE_OPS
#undef SET_TYPE_OPS
#undef SRC_DST_OPERATOR
/******************************************************************************
 *
 ******************************************************************************/
#define AEE_IF_TAG_ERROR(_TAG_) \
    if (_TAG_ == (uint32_t)-1) \
    { \
        CAM_LOGE("tag(%d) error", _TAG_); \
        AEE_ASSERT("tag error"); \
    }

IMetadata::IEntry::
IEntry(Tag_t tag)
    : mpImp(new Implementor(tag))
{
}


IMetadata::IEntry::
IEntry(IMetadata::IEntry const& other)
    : mpImp(new Implementor(*(other.mpImp)))
{
}


IMetadata::IEntry::
~IEntry()
{
    if(mpImp) delete mpImp;
}


IMetadata::IEntry&
IMetadata::IEntry::
operator=(IMetadata::IEntry const& other)
{
    if (this != &other) {
        delete mpImp;
        mpImp = new Implementor(*(other.mpImp));
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


MUINT32
IMetadata::IEntry::
tag() const
{
    return mpImp->tag();
}

MINT32
IMetadata::IEntry::
type() const
{
    return mpImp->type();
}

MBOOL
IMetadata::IEntry::
isEmpty() const
{
    //AEE_IF_TAG_ERROR(tag())
    return mpImp->isEmpty();
}


MUINT
IMetadata::IEntry::
count() const
{
    //AEE_IF_TAG_ERROR(tag())
    return mpImp->count();
}


MUINT
IMetadata::IEntry::
capacity() const
{
    AEE_IF_TAG_ERROR(tag())
    return mpImp->capacity();
}


MBOOL
IMetadata::IEntry::
setCapacity(MUINT size)
{
    AEE_IF_TAG_ERROR(tag())
    return mpImp->setCapacity(size);
}


MVOID
IMetadata::IEntry::
clear()
{
    AEE_IF_TAG_ERROR(tag())
    mpImp->clear();
}


MERROR
IMetadata::IEntry::
removeAt(MUINT index)
{
    AEE_IF_TAG_ERROR(tag())
    return mpImp->removeAt(index);
}


#define ASSERT_CHECK(_defaultT, _T) \
      CAM_LOGE_IF( TYPE_##_T != _defaultT, "tag(%x), type(%d) should be (%d)", tag(), TYPE_##_T, _defaultT); \
      if (TYPE_##_T != _defaultT) { \
          Utils::dumpCallStack(); \
          AEE_ASSERT("type mismatch"); \
      }
#undef  ASSERT_CHECK

#define IMETADATA_IENTRY_OPS_DECLARATION(_T) \
MVOID \
IMetadata::IEntry:: \
push_back(_T const& item, Type2Type<_T> type) \
{ \
    AEE_IF_TAG_ERROR(tag()) \
    mpImp->push_back(item, type); \
} \
_T& \
IMetadata::IEntry:: \
editItemAt(MUINT index, Type2Type<_T> type) \
{ \
    AEE_IF_TAG_ERROR(tag()) \
    return mpImp->editItemAt(index, type); \
} \
_T const& \
IMetadata::IEntry:: \
itemAt(MUINT index, Type2Type<_T> type) const \
{ \
    AEE_IF_TAG_ERROR(tag()) \
    return mpImp->itemAt(index, type); \
}

IMETADATA_IENTRY_OPS_DECLARATION(MUINT8)
IMETADATA_IENTRY_OPS_DECLARATION(MINT32)
IMETADATA_IENTRY_OPS_DECLARATION(MFLOAT)
IMETADATA_IENTRY_OPS_DECLARATION(MINT64)
IMETADATA_IENTRY_OPS_DECLARATION(MDOUBLE)
IMETADATA_IENTRY_OPS_DECLARATION(MRational)
IMETADATA_IENTRY_OPS_DECLARATION(MPoint)
IMETADATA_IENTRY_OPS_DECLARATION(MSize)
IMETADATA_IENTRY_OPS_DECLARATION(MRect)
IMETADATA_IENTRY_OPS_DECLARATION(IMetadata)
IMETADATA_IENTRY_OPS_DECLARATION(IMetadata::Memory)
#undef  IMETADATA_IENTRY_OPS_DECLARATION

#undef  AEE_IF_TAG_ERROR
/******************************************************************************
 *
 ******************************************************************************/
class IMetadata::Implementor
{
public:     ////                        Instantiation.
    virtual                            ~Implementor();
                                        Implementor();
    Implementor&                        operator=(Implementor const& other);
                                        Implementor(Implementor const& other);

public:     ////                        operators
    Implementor&                        operator+=(Implementor const& other);
    Implementor const                   operator+(Implementor const& other);

public:     ////                        Accessors.

    /**
     * Check to see whether it is empty (no entries) or not.
     */
    virtual MBOOL                       isEmpty() const;

    /**
     * Return the number of entries.
     */
    virtual MUINT                       count() const;

public:     ////                        Operations.

    /**
     * Clear all entries.
     * Note: Copy-on write.
     */
    virtual MVOID                       clear();

    /**
     * Delete an entry by tag.
     * Note: Copy-on write.
     */
    virtual MERROR                      remove(Tag_t tag);

    /**
     * Sort all entries for faster find.
     * Note: Copy-on write.
     */
    virtual MERROR                      sort();

    /**
     * Update metadata entry. An entry will be created if it doesn't exist already.
     * Note: Copy-on write.
     */
    virtual MERROR                      update(Tag_t tag, IEntry const& entry);


    /**
    * Get metadata entry by tag for editing.
    * Note: Copy-on write.
    */
    virtual IEntry&                     editEntryFor(Tag_t tag);

    /**
    * Get metadata entry by tag, with no editing.
    */
    virtual IEntry const&               entryFor(Tag_t tag) const;

    /**
     * Get metadata entry by index for editing.
     */
    virtual IEntry&                     editEntryAt(MUINT index);

    /**
     * Get metadata entry by index, with no editing.
     */
    virtual IEntry const&               entryAt(MUINT index) const;

    /**
     * Flatten IMetadata.
     */
    virtual MERROR                      flatten(android::String8 &flattened);

    /**
     * Unflatten IMetadata.
     */
    virtual MERROR                      unflatten(android::String8& flattened);

protected:
    DefaultKeyedVector<Tag_t, IEntry>   mMap;
};


/******************************************************************************
 *
 ******************************************************************************/
IMetadata::Implementor::
Implementor()
    : mMap()
{

}


IMetadata::Implementor::
Implementor(IMetadata::Implementor const& other)
    : mMap(other.mMap)
{
}


IMetadata::Implementor::
~Implementor()
{

}


IMetadata::Implementor&
IMetadata::Implementor::
operator+=(IMetadata::Implementor const& other)
{
    if (this != &other)
    {
        if( mMap.size() >= other.mMap.size() ) {
            for(size_t idx = 0; idx < other.mMap.size(); idx++ )
                mMap.add( other.mMap.keyAt(idx), other.mMap.valueAt(idx) );
        } else {
            auto temp = other.mMap;
            for(size_t idx = 0; idx < mMap.size(); idx++ )
                temp.add( mMap.keyAt(idx), mMap.valueAt(idx) );
            mMap = temp;
        }
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


IMetadata::Implementor const
IMetadata::Implementor::
operator+(IMetadata::Implementor const& other)
{
    return Implementor(*this) += other;
}


IMetadata::Implementor&
IMetadata::Implementor::
operator=(IMetadata::Implementor const& other)
{
    if (this != &other)
    {
        //release mMap'storage
        //assign other.mMap's storage pointer to mMap
        //add 1 to storage's sharebuffer
        mMap = other.mMap;
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


MBOOL
IMetadata::Implementor::
isEmpty() const
{
    return mMap.isEmpty();
}


MUINT
IMetadata::Implementor::
count() const
{
    return mMap.size();
}


MVOID
IMetadata::Implementor::
clear()
{
    mMap.clear();
}


MERROR
IMetadata::Implementor::
remove(Tag_t tag)
{
    return mMap.removeItem(tag);
}


MERROR
IMetadata::Implementor::
sort()
{

    //keyedVector always sorted.
    return OK;
}


MERROR
IMetadata::Implementor::
update(Tag_t tag, IEntry const& entry)
{
    return mMap.add(tag, entry);

}


IMetadata::IEntry&
IMetadata::Implementor::
editEntryFor(Tag_t tag)
{
    return mMap.editValueFor(tag);
}


IMetadata::IEntry const&
IMetadata::Implementor::
entryFor(Tag_t tag) const
{
    return mMap.valueFor(tag);
}


IMetadata::IEntry&
IMetadata::Implementor::
editEntryAt(MUINT index)
{
    return mMap.editValueAt(index);

}


IMetadata::IEntry const&
IMetadata::Implementor::
entryAt(MUINT index) const
{
    return mMap.valueAt(index);

}


template<class T>
static MERROR
print(int type, Vector< T > &storage, android::String8 &str)
{
    switch (type)
    {
        case TYPE_MUINT8:
        case TYPE_Memory:
        case TYPE_MSize:
        case TYPE_MRect:
        case TYPE_MPoint:
        case TYPE_MRational:
        {
            for ( size_t i = 0; i < storage.size(); ++i )
                str += android::String8::format("%d ", (MINT32)storage[i]);
        } break;
        case TYPE_MFLOAT: {
            for ( size_t i = 0; i < storage.size(); ++i )
                str += android::String8::format("%f ", (MFLOAT)storage[i]);
        } break;
        case TYPE_MINT32: {
            for ( size_t i = 0; i < storage.size(); ++i )
                str += android::String8::format("%ld ", (MINT64)storage[i]);
        } break;
        case TYPE_MINT64: {
            for ( size_t i = 0; i < storage.size(); ++i )
                str += android::String8::format("%lld ", (MINT64)storage[i]);
        } break;
        case TYPE_MDOUBLE: {
            for ( size_t i = 0; i < storage.size(); ++i )
                str += android::String8::format("%lf ", (MDOUBLE)storage[i]);
        } break;
        case TYPE_IMetadata: {
        } break;
        default: {
            ALOGW("undefined type %d", type);
            return -EINVAL;
        }
    };
    //
    return OK;
}


template<class T>
static void
ConvertEntry(Vector<T> &storage, IMetadata::IEntry const& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        storage.push_back(entry.itemAt(i, Type2Type< T >() ));
    }
}


static void
ConvertMRect(Vector< MINT32 > &storage, IMetadata::IEntry const& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        MRect src_rect = entry.itemAt(i, Type2Type< MRect >());
        storage.push_back(src_rect.p.x);
        storage.push_back(src_rect.p.y);
        storage.push_back(src_rect.s.w);
        storage.push_back(src_rect.s.h);
    }
}


static void
ConvertMSize(Vector< MINT32 > &storage, IMetadata::IEntry const& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        MSize src_size = entry.itemAt(i, Type2Type< MSize >());
        storage.push_back(src_size.w);
        storage.push_back(src_size.h);
    }
}


static void
ConvertMPoint(Vector< MINT32 > &storage, IMetadata::IEntry const& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        MPoint src_point = entry.itemAt(i, Type2Type< MPoint >());
        storage.push_back(src_point.x);
        storage.push_back(src_point.y);
    }
}


static void
ConvertMRational(Vector< MINT32 > &storage, IMetadata::IEntry const& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        MRational src_rational = entry.itemAt(i, Type2Type< MRational >());
        storage.push_back(src_rational.numerator);
        storage.push_back(src_rational.denominator);
    }
}


static void
ConvertMemory(Vector< MUINT8 > &storage, IMetadata::IEntry const& entry)
{
    for(uint32_t i = 0; i < entry.count(); i++) {
        IMetadata::Memory m = entry.itemAt(i, Type2Type< IMetadata::Memory >());
        storage.push_back(m.size());
        storage.appendVector(m);
    }
}


#define NORMAL_PRINT_TO_STRING(_type_, _src_entry_, _dst_string_) \
if ( TYPE_##_type_ == _src_entry_.type() ) {\
    Vector<_type_> storage; \
    ConvertEntry(storage, _src_entry_); \
    if( print(TYPE_##_type_, storage, _dst_string_) != OK ) { \
        return MFALSE; \
    } \
}


#define SPECIAL_PRINT_TO_STRING(_type_, _vec_type_, _src_entry_, _dst_string_) \
if ( TYPE_##_type_ == _src_entry_.type() ) {\
    Vector<_vec_type_> storage; \
    Convert##_type_(storage, _src_entry_); \
    if( print(TYPE_##_type_, storage, _dst_string_) != OK ) { \
        return MFALSE; \
    } \
}


#define ENTRY_TO_STRING(_src_entry_, _dst_string_) \
    NORMAL_PRINT_TO_STRING(MUINT8,  _src_entry_, _dst_string_); \
    NORMAL_PRINT_TO_STRING(MINT32,  _src_entry_, _dst_string_); \
    NORMAL_PRINT_TO_STRING(MFLOAT,  _src_entry_, _dst_string_); \
    NORMAL_PRINT_TO_STRING(MINT64,  _src_entry_, _dst_string_); \
    NORMAL_PRINT_TO_STRING(MDOUBLE, _src_entry_, _dst_string_); \
    SPECIAL_PRINT_TO_STRING(MRational, MINT32, _src_entry_, _dst_string_); \
    SPECIAL_PRINT_TO_STRING(MRect,     MINT32, _src_entry_, _dst_string_); \
    SPECIAL_PRINT_TO_STRING(MSize,     MINT32, _src_entry_, _dst_string_); \
    SPECIAL_PRINT_TO_STRING(MPoint,    MINT32, _src_entry_, _dst_string_); \
    SPECIAL_PRINT_TO_STRING(Memory,    MUINT8, _src_entry_, _dst_string_); \


MERROR
IMetadata::Implementor::
flatten(android::String8 &flattened)
{
    for (size_t i = 0; i < mMap.size(); ++i) {
        // format "Tag Type Count [data...]"
        IMetadata::IEntry entry = mMap.valueAt(i);
        flattened += android::String8::format("%d %d %d ", entry.tag(), entry.type(), entry.count());
        //
        if ( TYPE_IMetadata == entry.type() ) {
            for( size_t j = 0; j < entry.count(); ++j ) {
                IMetadata* meta = &entry.editItemAt(j, Type2Type< IMetadata >());
                meta->flatten(flattened);
                flattened += android::String8::format("; ");
            }
        } else {
            ENTRY_TO_STRING(entry, flattened);
        }
    }
    //
    return OK;
}

#undef NORMAL_PRINT_TO_STRING
#undef SPECIAL_PRINT_TO_STRING
#undef ENTRY_TO_STRING


static char*
getNextToken(char*& pch)
{
    char *res = pch;
    pch = strtok (NULL, " ");
    //
    return res;
}


template<class T>
static MERROR
Convert2Entry(int type, IMetadata::IEntry& entry, char*& pch, MINT32 count)
{
    switch (type)
    {
        case TYPE_MUINT8: {
            for ( MINT32 i = 0; i < count; ++i ) {
                T data = atoi( getNextToken(pch) );
                entry.push_back(data, Type2Type< T >());
            }
        } break;
        case TYPE_MINT32: {
            for ( MINT32 i = 0; i < count; ++i ) {
                T data = atol( getNextToken(pch) );
                entry.push_back(data, Type2Type< T >());
            }
        } break;
        case TYPE_MFLOAT:
        case TYPE_MDOUBLE: {
            for ( MINT32 i = 0; i < count; ++i ) {
                T data = atof( getNextToken(pch) );
                entry.push_back(data, Type2Type< T >());
            }
        } break;
        case TYPE_MINT64: {
            for ( MINT32 i = 0; i < count; ++i ) {
                T data = atoll( getNextToken(pch) );
                entry.push_back(data, Type2Type< T >());
            }
        } break;
        case TYPE_IMetadata: {
        } break;
        default: {
            ALOGW("undefined type %d", type);
            return -EINVAL;
        }
    };
    return OK;
}


static void
Convert2MRect(IMetadata::IEntry& entry, char*& pch, MINT32 count)
{
    for ( MINT32 i = 0; i < count; ++i ) {
        MPoint p(atoi( getNextToken(pch) ), atoi( getNextToken(pch) ));
        MSize  s(atoi( getNextToken(pch) ), atoi( getNextToken(pch) ));
        //
        MRect rect(p, s);
        entry.push_back(rect, Type2Type< MRect >());
    }
}


template<class T>
static void
Convert2Entry2(IMetadata::IEntry& entry, char*& pch, MINT32 count)
{
    for ( MINT32 i = 0; i < count; ++i ) {
        MINT32 x = atoi( getNextToken(pch) );
        MINT32 y = atoi( getNextToken(pch) );
        //
        T data(x, y);
        entry.push_back(data, Type2Type< T >());
    }
}


static void
Convert2Memory(IMetadata::IEntry& entry, char*& pch, MINT32 count)
{
    for ( MINT32 i = 0; i < count; ++i ) {
        MINT32 vec_count = atoi( getNextToken(pch) );
        //
        IMetadata::Memory mMemory;
        mMemory.resize(vec_count);
        //
        for (MINT32 j = 0; j < vec_count; ++j) {
            MINT32 data = atoi( getNextToken(pch) );
            mMemory.editItemAt(j) = data;
        }
        entry.push_back(mMemory, Type2Type< IMetadata::Memory >());
    }
}

#define NORMAL_STRING_TO_ENTRY(_type_, _real_type_, _src_entry_, _pch_, _count_) \
if ( TYPE_##_type_ == _real_type_ ) { \
    if ( Convert2Entry< _type_ >(TYPE_##_type_, _src_entry_, _pch_, _count_) != OK ) {\
        return MFALSE; \
    } \
}


#define NORMAL_STRING_TO_ENTRY2(_type_, _real_type_, _src_entry_, _pch_, _count_) \
if ( TYPE_##_type_ == _real_type_ ) { \
    Convert2Entry2< _type_ >(_src_entry_, _pch_, _count_); \
}


#define SPECIAL_STRING_TO_ENTRY(_type_, _real_type_, _src_entry_, _pch_, _count_) \
if ( TYPE_##_type_ == _real_type_ ) { \
    Convert2##_type_(_src_entry_, _pch_, _count_); \
}


#define STRING_TO_ENTRY(_type_, _src_entry_, _pch_, _count_) \
    NORMAL_STRING_TO_ENTRY(MUINT8,     _type_, _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY(MINT32,     _type_, _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY(MFLOAT,     _type_, _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY(MINT64,     _type_, _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY(MDOUBLE,    _type_, _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY2(MRational, _type_, _src_entry_, _pch_, _count_); \
    SPECIAL_STRING_TO_ENTRY(MRect,     _type_,  _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY2(MSize,     _type_, _src_entry_, _pch_, _count_); \
    NORMAL_STRING_TO_ENTRY2(MPoint,    _type_, _src_entry_, _pch_, _count_); \
    SPECIAL_STRING_TO_ENTRY(Memory,    _type_, _src_entry_, _pch_, _count_);


MERROR
IMetadata::Implementor::
unflatten( android::String8& flattened)
{
    android::String8 IMeta_str = android::String8(flattened.string());
    char* pch = strtok (const_cast<char*>(IMeta_str.string()), " ");
    //
    while ( pch != NULL ) {
        char* sTag = getNextToken(pch);
        //
        Tag_t Tag  = atoi( sTag );
        int type   = atoi( getNextToken(pch) );
        int count  = atoi( getNextToken(pch) );
        //
        IMetadata::IEntry entry( Tag );
        if ( TYPE_IMetadata == type ) {
            for( size_t j = 0; j < count; ++j ) {
                //char* temp = strstr( strstr(const_cast<char*>(flattened.string()), sTag), getNextToken(pch));
                char* temp = strstr( strstr( strstr( strstr(const_cast<char*>(flattened.string()), sTag)," ")+1," ")+1," ")+1;
                if ( temp != NULL ) flattened.setTo(temp);
                IMetadata meta;
                meta.unflatten(flattened);
                //
                entry.push_back(meta, Type2Type< IMetadata >());
                //
                temp = strstr(const_cast<char*>(flattened.string()), ";");
                ALOGD("temp:%s", temp);
                android::String8 tmp_str = android::String8(temp + 1);
                pch = strtok (const_cast<char*>(tmp_str.string()), " ");
                flattened.setTo(temp + 1);
                ALOGD("flattened:%s", flattened.string());
            }
        } else {
            STRING_TO_ENTRY(type, entry, pch, count);
            //
            if ( pch != NULL && !strcmp(pch, ";") ) {
                mMap.add( Tag, entry);
                return OK;
            }
        }
        mMap.add( Tag, entry);
    }
    //
    return OK;
}


#undef NORMAL_STRING_TO_ENTRY
#undef NORMAL_STRING_TO_ENTRY2
#undef SPECIAL_STRING_TO_ENTRY
#undef STRING_TO_ENTRY



/******************************************************************************
 *
 ******************************************************************************/
IMetadata::
IMetadata()
    : mpImp(new Implementor())
{

}


IMetadata::IMetadata(IMetadata const& other)
    : mpImp(new Implementor(*(other.mpImp)))
{
}


IMetadata::
~IMetadata()
{
     if(mpImp) delete mpImp;

}


IMetadata&
IMetadata::operator=(IMetadata const& other)
{
    if (this != &other) {
        delete mpImp;
        mpImp = new Implementor(*(other.mpImp));
    }
    else {
        CAM_LOGW("this(%p) == other(%p)", this, &other);
    }

    return *this;
}


IMetadata&
IMetadata::operator+=(IMetadata const& other)
{
    *mpImp += *other.mpImp;
    return *this;
}


IMetadata const
IMetadata::operator+(IMetadata const& other)
{
    return IMetadata(*this) += other;
}


MBOOL
IMetadata::
isEmpty() const
{
    return mpImp->isEmpty();
}


MUINT
IMetadata::
count() const
{
    return mpImp->count();
}


MVOID
IMetadata::
clear()
{
    mpImp->clear();
}


MERROR
IMetadata::
remove(Tag_t tag)
{
    return mpImp->remove(tag) >= 0 ? OK : BAD_VALUE;
}


MERROR
IMetadata::
sort()
{
    return mpImp->sort();
}


MERROR
IMetadata::
update(Tag_t tag, IMetadata::IEntry const& entry)
{
    MERROR ret = mpImp->update(tag, entry);  //keyedVector has two possibilities: BAD_VALUE/NO_MEMORY
    return ret >= 0 ? (MERROR)OK : (MERROR)ret;
}


IMetadata::IEntry&
IMetadata::
editEntryFor(Tag_t tag)
{
    return mpImp->editEntryFor(tag);
}


IMetadata::IEntry const&
IMetadata::
entryFor(Tag_t tag) const
{
    return mpImp->entryFor(tag);
}

IMetadata::IEntry&
IMetadata::
editEntryAt(MUINT index)
{
    return mpImp->editEntryAt(index);

}


IMetadata::IEntry const&
IMetadata::
entryAt(MUINT index) const
{
    return mpImp->entryAt(index);
}

MERROR
IMetadata::
flatten(android::String8 &flattened)
{
    return mpImp->flatten(flattened);
}


MERROR
IMetadata::
unflatten(android::String8& flattened)
{
    return mpImp->unflatten(flattened);
}