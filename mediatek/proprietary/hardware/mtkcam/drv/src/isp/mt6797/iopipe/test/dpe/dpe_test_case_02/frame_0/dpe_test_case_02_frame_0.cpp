#include "dpe_test_case_02_frame_0.h"
unsigned int dpe_test_case_02_frame_0_golden_dpe_confo_l_size = dpe_test_case_02_frame_0_dpe_confo_l_frame_00_14_size;
char* dpe_test_case_02_frame_0_golden_dpe_confo_l_frame = &dpe_test_case_02_frame_0_dpe_confo_l_frame_00_14[0];

unsigned int dpe_test_case_02_frame_0_golden_dpe_confo_r_size = dpe_test_case_02_frame_0_dpe_confo_r_frame_00_14_size;
char* dpe_test_case_02_frame_0_golden_dpe_confo_r_frame = &dpe_test_case_02_frame_0_dpe_confo_r_frame_00_14[0];

unsigned int dpe_test_case_02_frame_0_golden_dpe_dvo_l_size = dpe_test_case_02_frame_0_dpe_dvo_l_frame_00_14_size;
char* dpe_test_case_02_frame_0_golden_dpe_dvo_l_frame = &dpe_test_case_02_frame_0_dpe_dvo_l_frame_00_14[0];

unsigned int dpe_test_case_02_frame_0_golden_dpe_dvo_r_size = dpe_test_case_02_frame_0_dpe_dvo_r_frame_00_14_size;
char* dpe_test_case_02_frame_0_golden_dpe_dvo_r_frame = &dpe_test_case_02_frame_0_dpe_dvo_r_frame_00_14[0];

unsigned int dpe_test_case_02_frame_0_golden_dpe_respo_l_size = dpe_test_case_02_frame_0_dpe_respo_l_frame_00_14_size;
char* dpe_test_case_02_frame_0_golden_dpe_respo_l_frame = &dpe_test_case_02_frame_0_dpe_respo_l_frame_00_14[0];

unsigned int dpe_test_case_02_frame_0_golden_dpe_respo_r_size = dpe_test_case_02_frame_0_dpe_respo_r_frame_00_14_size;
char* dpe_test_case_02_frame_0_golden_dpe_respo_r_frame = &dpe_test_case_02_frame_0_dpe_respo_r_frame_00_14[0];


void getdpe_test_case_02_frame_0GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2
)
{
*golden_dpe_confo_l_frame = (unsigned long)&dpe_test_case_02_frame_0_dpe_confo_l_frame_00_14[0];
*golden_dpe_confo_r_frame = (unsigned long)&dpe_test_case_02_frame_0_dpe_confo_r_frame_00_14[0];
*golden_dpe_dvo_l_frame = (unsigned long)&dpe_test_case_02_frame_0_dpe_dvo_l_frame_00_14[0];
*golden_dpe_dvo_r_frame = (unsigned long)&dpe_test_case_02_frame_0_dpe_dvo_r_frame_00_14[0];
*golden_dpe_respo_l_frame = (unsigned long)&dpe_test_case_02_frame_0_dpe_respo_l_frame_00_14[0];
*golden_dpe_respo_r_frame = (unsigned long)&dpe_test_case_02_frame_0_dpe_respo_r_frame_00_14[0];
}
