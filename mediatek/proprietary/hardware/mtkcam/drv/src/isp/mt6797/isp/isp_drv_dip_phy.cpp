/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#include <utils/Errors.h>
#include <utils/Mutex.h>    // For android::Mutex.
#include <cutils/log.h>
#include <cutils/properties.h>  // For property_get().
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/mman-proprietary.h>
#include <cutils/atomic.h>
#include <common/include/common.h>

#include "camera_isp.h"
#include "isp_drv_dip_phy.h"


#undef   DBG_LOG_TAG                        // Decide a Log TAG for current file.
#define  DBG_LOG_TAG        "{IspDrvDipPhy} "
#include "drv_log.h"                        // Note: DBG_LOG_TAG/LEVEL will be used in header file, so header must be included after definition.
DECLARE_DBG_LOG_VARIABLE(isp_drv_dip_phy);

// Clear previous define, use our own define.
#undef LOG_VRB
#undef LOG_DBG
#undef LOG_INF
#undef LOG_WRN
#undef LOG_ERR
#undef LOG_AST
#define LOG_VRB(fmt, arg...)        do { if (isp_drv_dip_phy_DbgLogEnable_VERBOSE) { BASE_LOG_VRB(fmt, ##arg); } } while(0)
#define LOG_DBG(fmt, arg...)        do { if (isp_drv_dip_phy_DbgLogEnable_DEBUG  ) { BASE_LOG_DBG(fmt, ##arg); } } while(0)
#define LOG_INF(fmt, arg...)        do { if (isp_drv_dip_phy_DbgLogEnable_INFO   ) { BASE_LOG_INF(fmt, ##arg); } } while(0)
#define LOG_WRN(fmt, arg...)        do { if (isp_drv_dip_phy_DbgLogEnable_WARN   ) { BASE_LOG_WRN(fmt, ##arg); } } while(0)
#define LOG_ERR(fmt, arg...)        do { if (isp_drv_dip_phy_DbgLogEnable_ERROR  ) { BASE_LOG_ERR(fmt, ##arg); } } while(0)
#define LOG_AST(cond, fmt, arg...)  do { if (isp_drv_dip_phy_DbgLogEnable_ASSERT ) { BASE_LOG_AST(cond, fmt, ##arg); } } while(0)


IspDrvDipPhy::IspDrvDipPhy()
{
    DBG_LOG_CONFIG(drv, isp_drv_dip_phy);
    LOG_VRB("getpid[0x%08x],gettid[0x%08x]", getpid() ,gettid());
    m_UserCnt = 0;

    m_pIspDrvImp = NULL;

}


static IspDrvDipPhy    gDipDrvObj[ISP_DIP_MODULE_IDX_MAX];


IspDrv* IspDrvDipPhy::createInstance(ISP_HW_MODULE hwModule)
{
    MUINT32 moduleIdx;

    if(hwModule<DIP_MAX && hwModule>=DIP_A){
        moduleIdx = hwModule - DIP_START;
        LOG_DBG("moduleIdx(%d),hwModule(%d)",moduleIdx,hwModule);
    } else {
        LOG_ERR("[Error]hwModule(%d) out of the range",hwModule);
        return NULL;
    }

    gDipDrvObj[moduleIdx].m_pIspDrvImp = (IspDrvImp*)IspDrvImp::createInstance(hwModule);
    gDipDrvObj[moduleIdx].m_hwModule = hwModule;
    //
    return (IspDrv*)&gDipDrvObj[moduleIdx];
}



MBOOL IspDrvDipPhy::init(const char* userName)
{
    MBOOL Result = MTRUE;
    //
    android::Mutex::Autolock lock(this->IspRegMutex);
    //
    LOG_INF("+,m_UserCnt(%d),curUser(%s).", this->m_UserCnt,userName);
    //
    if(strlen(userName)<1)
    {
        LOG_ERR("Plz add userName if you want to use isp driver\n");
        return MFALSE;
    }
    //
    if(this->m_UserCnt > 0)
    {
        android_atomic_inc(&this->m_UserCnt);
        LOG_INF("-,m_UserCnt(%d)", this->m_UserCnt);
        return Result;
    }

    if(this->m_pIspDrvImp->init("isp_drv_dip_phy") == MFALSE){
        Result = MFALSE;
        goto EXIT;
    }
    // load dip hw init setting
    if(this->loadInitSetting() == MFALSE){
        Result = MFALSE;
        LOG_ERR("load dip hw init setting fail\n");
        goto EXIT;
    }

    //check wheather kernel is existed or not
    if (this->m_Fd < 0) {    // 1st time open failed.
        LOG_ERR("ISP kernel is not existed\n");
        Result = MFALSE;
        goto EXIT;
    }

    //
    android_atomic_inc(&this->m_UserCnt);

EXIT:

    LOG_DBG("-,ret(%d),mInitCount(%d)", Result, this->m_UserCnt);
    return Result;
}

//-----------------------------------------------------------------------------
MBOOL IspDrvDipPhy::uninit(const char* userName)
{
    MBOOL Result = MTRUE;

    //
    android::Mutex::Autolock lock(this->IspRegMutex);
    //
    LOG_INF("+,m_UserCnt(%d),curUser(%s)", this->m_UserCnt,userName);
    //
    if(strlen(userName)<1)
    {
        LOG_ERR("Plz add userName if you want to uninit isp driver\n");
        return MFALSE;
    }

    //
    if(this->m_UserCnt <= 0)
    {
        LOG_ERR("no more user in isp_drv_dip_phy , curUser(%s)",userName);
        goto EXIT;
    }
    // More than one user
    android_atomic_dec(&this->m_UserCnt);

    if(this->m_UserCnt > 0)    // If there are still users, exit.
        goto EXIT;
    //
    if(this->m_pIspDrvImp->uninit("isp_drv_dip_phy") == MFALSE){
        Result = MFALSE;
        goto EXIT;
    }
EXIT:

    LOG_DBG("-,ret(%d),m_UserCnt(%d)", Result, this->m_UserCnt);
    return Result;
}

#define FD_CHK()({\
    MINT32 Ret=0;\
    if(this->m_Fd < 0){\
        LOG_ERR("no isp device");\
        Ret = -1;\
    }\
    Ret;\
})

void IspDrvDipPhy::destroyInstance(void)
{
    this->m_pIspDrvImp = NULL;
}

MBOOL IspDrvDipPhy::waitIrq(ISP_WAIT_IRQ_ST* pWaitIrq)
{
    return this->m_pIspDrvImp->waitIrq(pWaitIrq);
}

MBOOL IspDrvDipPhy::clearIrq(ISP_CLEAR_IRQ_ST* pClearIrq)
{
    return this->m_pIspDrvImp->clearIrq(pClearIrq);
}

MBOOL IspDrvDipPhy::registerIrq(ISP_REGISTER_USERKEY_STRUCT* pRegIrq)
{
    return this->m_pIspDrvImp->registerIrq(pRegIrq);
}

MBOOL IspDrvDipPhy::readRegs(ISP_DRV_REG_IO_STRUCT*  pRegIo,MUINT32 Count,MINT32 caller)
{
    return this->m_pIspDrvImp->readRegs(pRegIo, Count, caller);
}

MUINT32 IspDrvDipPhy::readReg(MUINT32 Addr,MINT32 caller)
{
    return this->m_pIspDrvImp->readReg(Addr, caller);
}

MBOOL IspDrvDipPhy::writeRegs(ISP_DRV_REG_IO_STRUCT*  pRegIo,MUINT32 Count,MINT32 caller)
{
    return this->m_pIspDrvImp->writeRegs(pRegIo, Count, caller);
}

MBOOL IspDrvDipPhy::writeReg(MUINT32 Addr,unsigned long Data,MINT32 caller)
{
    return this->m_pIspDrvImp->writeReg(Addr, Data, caller);
}

MBOOL IspDrvDipPhy::signalIrq(ISP_WAIT_IRQ_ST* pWaitIrq)
{
    LOG_INF("IRQ SIGNAL:hwModule:0x%x, userKey:0x%x, status:0x%x",this->m_hwModule,pWaitIrq->UserKey,pWaitIrq->Status);
    ISP_WAIT_IRQ_STRUCT wait;

    memcpy(&wait.EventInfo,pWaitIrq,sizeof(ISP_WAIT_IRQ_ST));
    switch(this->m_hwModule){
        case DIP_A:
            wait.Type = ISP_IRQ_TYPE_INT_DIP_A_ST;
            break;
        default:
            LOG_ERR("unsupported hw hwModule:0x%x\n",this->m_hwModule);
            return MFALSE;
            break;
    }

    if(ioctl(this->m_pIspDrvImp->m_Fd,ISP_FLUSH_IRQ_REQUEST,&wait) < 0){
        LOG_ERR("signal IRQ fail(irq:0x%x,status:0x%x)",wait.Type,wait.EventInfo.Status);
        return MFALSE;
    }
    return MTRUE;
}


MBOOL IspDrvDipPhy::getDeviceInfo(E_DEVICE_INFO eCmd,MUINT8* pData)
{
    MBOOL rst = MTRUE;

    switch(eCmd){
        case _GET_DIP_DBG_INFO:
            //dumpDbgLog((IspDumpDbgLogDipPackage*)pData);
            break;
        case _GET_SOF_CNT:
        case _GET_DMA_ERR:
        case _GET_INT_ERR:
        case _GET_DROP_FRAME_STATUS:
        default:
            LOG_ERR("unsupported cmd:0x%x",eCmd);
            return MFALSE;
        break;
    }
    return rst;
}

MBOOL IspDrvDipPhy::setDeviceInfo(E_DEVICE_INFO eCmd,MUINT8* pData)
{
    ISP_P2_BUFQUE_STRUCT p2bufQ;

    switch(eCmd){
        case _SET_DBG_INT:
            if(ioctl(this->m_pIspDrvImp->m_Fd,ISP_DEBUG_FLAG,(unsigned char*)pData) < 0){
                LOG_ERR("kernel log enable error\n");
                return MFALSE;
            }
            break;
        case _SET_DIP_BUF_INFO:
            p2bufQ.ctrl = static_cast<ISP_P2_BUFQUE_CTRL_ENUM>(pData[0]);
            p2bufQ.property = static_cast<ISP_P2_BUFQUE_PROPERTY>(pData[1]);
            p2bufQ.processID = 0;//static_cast<MUINT32>(pData[2]);
            p2bufQ.callerID = static_cast<MUINT32>(pData[3]) | (static_cast<MUINT32>(pData[4])<<8) | (static_cast<MUINT32>(pData[5])<<16) | (static_cast<MUINT32>(pData[6])<<24);
            p2bufQ.frameNum = static_cast<MINT32>(pData[7]);
            p2bufQ.cQIdx = static_cast<MINT32>(pData[8]);
            p2bufQ.dupCQIdx = static_cast<MINT32>(pData[9]);
            p2bufQ.burstQIdx = static_cast<MINT32>(pData[10]);
            p2bufQ.timeoutIns = static_cast<MUINT32>(pData[11]);
            LOG_DBG("p2bufQ(%d_%d_%d_0x%x_%d_%d_%d_%d_%d)",p2bufQ.ctrl,p2bufQ.property,p2bufQ.processID,p2bufQ.callerID,\
                p2bufQ.frameNum,p2bufQ.cQIdx ,p2bufQ.dupCQIdx,p2bufQ.burstQIdx,p2bufQ.timeoutIns);
            if(ioctl(this->m_pIspDrvImp->m_Fd,ISP_P2_BUFQUE_CTRL,&p2bufQ) < 0){
                LOG_ERR("ISP_P2_BUFQUE_CTRL(%d) error\n",p2bufQ.ctrl);
                return MFALSE;
            }
            break;
        default:
            LOG_ERR("unsupported cmd:0x%x",eCmd);
            return MFALSE;
        break;
    }
    return MTRUE;
}


MBOOL IspDrvDipPhy::loadInitSetting(void)
{
    LOG_INF("loadInitSetting size(%d)",ISP_DIP_INIT_SETTING_COUNT);


    this->m_pIspDrvImp->writeRegs(mIspDipInitReg, ISP_DIP_INIT_SETTING_COUNT, DIP_A);

#if 0
    //debug check
    LOG_INF("DIP_X_IMGI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x41C,DIP_A));
    LOG_INF("DIP_X_IMGI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x420,DIP_A));
    LOG_INF("DIP_X_IMGI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x424,DIP_A));

    LOG_INF("DIP_X_IMGBI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x44C,DIP_A));
    LOG_INF("DIP_X_IMGBI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x450,DIP_A));
    LOG_INF("DIP_X_IMGBI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x454,DIP_A));

    LOG_INF("DIP_X_IMGCI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x47C,DIP_A));
    LOG_INF("DIP_X_IMGCI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x480,DIP_A));
    LOG_INF("DIP_X_IMGCI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x484,DIP_A));

    LOG_INF("DIP_X_UFDI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x5CC,DIP_A));
    LOG_INF("DIP_X_UFDI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x5D0,DIP_A));
    LOG_INF("DIP_X_UFDI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x5D4,DIP_A));

    LOG_INF("DIP_X_LCEI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x59C,DIP_A));
    LOG_INF("DIP_X_LCEI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x5A0,DIP_A));
    LOG_INF("DIP_X_LCEI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x5A4,DIP_A));

    LOG_INF("DIP_X_VIPI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x4AC,DIP_A));
    LOG_INF("DIP_X_VIPI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x4B0,DIP_A));
    LOG_INF("DIP_X_VIPI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x4B4,DIP_A));

    LOG_INF("DIP_X_VIP2I_CON(0x%x)",this->m_pIspDrvImp->readReg(0x4DC,DIP_A));
    LOG_INF("DIP_X_VIP2I_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x4E0,DIP_A));
    LOG_INF("DIP_X_VIP2I_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x4E4,DIP_A));

    LOG_INF("DIP_X_VIP3I_CON(0x%x)",this->m_pIspDrvImp->readReg(0x50C,DIP_A));
    LOG_INF("DIP_X_VIP3I_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x510,DIP_A));
    LOG_INF("DIP_X_VIP3I_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x514,DIP_A));

    LOG_INF("DIP_X_DMGI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x53C,DIP_A));
    LOG_INF("DIP_X_DMGI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x540,DIP_A));
    LOG_INF("DIP_X_DMGI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x544,DIP_A));

    LOG_INF("DIP_X_DEPI_CON(0x%x)",this->m_pIspDrvImp->readReg(0x56C,DIP_A));
    LOG_INF("DIP_X_DEPI_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x570,DIP_A));
    LOG_INF("DIP_X_DEPI_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x574,DIP_A));

    LOG_INF("DIP_X_MFBO_CON(0x%x)",this->m_pIspDrvImp->readReg(0x36C,DIP_A));
    LOG_INF("DIP_X_MFBO_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x370,DIP_A));
    LOG_INF("DIP_X_MFBO_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x374,DIP_A));

    LOG_INF("DIP_X_IMG3O_CON(0x%x)",this->m_pIspDrvImp->readReg(0x2AC,DIP_A));
    LOG_INF("DIP_X_IMG3O_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x2B0,DIP_A));
    LOG_INF("DIP_X_IMG3O_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x2B4,DIP_A));

    LOG_INF("DIP_X_IMG3BO_CON(0x%x)",this->m_pIspDrvImp->readReg(0x2DC,DIP_A));
    LOG_INF("DIP_X_IMG3BO_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x2E0,DIP_A));
    LOG_INF("DIP_X_IMG3BO_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x2E4,DIP_A));

    LOG_INF("DIP_X_IMG3CO_CON(0x%x)",this->m_pIspDrvImp->readReg(0x30C,DIP_A));
    LOG_INF("DIP_X_IMG3CO_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x310,DIP_A));
    LOG_INF("DIP_X_IMG3CO_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x314,DIP_A));

    LOG_INF("DIP_X_FEO_CON(0x%x)",this->m_pIspDrvImp->readReg(0x33C,DIP_A));
    LOG_INF("DIP_X_FEO_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x340,DIP_A));
    LOG_INF("DIP_X_FEO_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x344,DIP_A));

    LOG_INF("DIP_X_IMG2O_CON(0x%x)",this->m_pIspDrvImp->readReg(0x24C,DIP_A));
    LOG_INF("DIP_X_IMG2O_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x250,DIP_A));
    LOG_INF("DIP_X_IMG2O_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x254,DIP_A));

    LOG_INF("DIP_X_IMG2BO_CON(0x%x)",this->m_pIspDrvImp->readReg(0x27C,DIP_A));
    LOG_INF("DIP_X_IMG2BO_CON2(0x%x)",this->m_pIspDrvImp->readReg(0x280,DIP_A));
    LOG_INF("DIP_X_IMG2BO_CON3(0x%x)",this->m_pIspDrvImp->readReg(0x284,DIP_A));

    LOG_INF("loadInitSetting size -");
#endif    
    return MTRUE;
}



