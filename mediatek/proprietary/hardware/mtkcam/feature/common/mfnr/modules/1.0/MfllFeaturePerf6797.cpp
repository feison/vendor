#include "MfllFeaturePerf.h"
#include "MfllLog.h"

#include <perfservicenative/PerfServiceNative.h>

#include <linux/mt_sched.h>

// STL
#include <mutex>
#include <future>

#define LOG_TAG "MtkCam/MfllCore/Perf"

using namespace mfll;

static int                  g_refCount = 0;
static std::mutex           g_mutex;
static std::future<void>    g_launchEnableCPU;
static std::future<void>    g_launchDisableCPU;

// {{{ PerfServiceHanlder
class PerfServiceHanlder {
public:
    int m_sceneHandle;
    std::mutex m_mutex;

public:
    void initPerfService(void)
    {
        std::unique_lock<std::mutex> _l(m_mutex);
        mfllAutoLogFunc();
        int r = PerfServiceNative_userRegScn();
        if (r <= 0) {
            mfllLogE("register perf service scenario failed");
            return;
        }
        // config LL core (CA53 x4 up to 1.5G)
        PerfServiceNative_userRegScnConfig(
                m_sceneHandle, CMD_SET_CLUSTER_CPU_CORE_MIN, 0, 4, 0, 0);        // 4 x L
        PerfServiceNative_userRegScnConfig(
                m_sceneHandle, CMD_SET_CLUSTER_CPU_FREQ_MIN, 0, 1391000, 0, 0);  // L freq = 1.391G

        // config L core (CA53 x4 up to 1.95G)
        PerfServiceNative_userRegScnConfig(
                m_sceneHandle, CMD_SET_CLUSTER_CPU_CORE_MIN, 1, 4, 0, 0);        // 4 x L
        PerfServiceNative_userRegScnConfig(
                m_sceneHandle, CMD_SET_CLUSTER_CPU_FREQ_MIN, 1, 1950000, 0, 0);  // L freq = 1.95G

        // config big core (CA72 x2 up to 2.288G)
        PerfServiceNative_userRegScnConfig(
                m_sceneHandle, CMD_SET_CLUSTER_CPU_CORE_MIN, 2, 2, 0, 0);        // 2 x BIG
        PerfServiceNative_userRegScnConfig(
                m_sceneHandle, CMD_SET_CLUSTER_CPU_FREQ_MIN, 2, 2288000, 0, 0);  // Big freq = 2.288G

        PerfServiceNative_userEnable(m_sceneHandle);
        m_sceneHandle = r;
    };

    void uninitPerfService(void)
    {
        std::unique_lock<std::mutex> _l(m_mutex);
        mfllAutoLogFunc();
        if (m_sceneHandle >= 0) {
            PerfServiceNative_userDisable(m_sceneHandle);
            PerfServiceNative_userUnreg(m_sceneHandle);
        }
    };
    void affineMotionEstimation(int index)
    {
        /**
         *  Motion Estimation uses only a single thread to process it's operation,
         *  hence we have to affine every job to a core, and it runs parallelly.
         */
        std::unique_lock<std::mutex> _l(m_mutex);

        if (index > 9)
            return;

        unsigned int targetCpu = 1 << (9-index); // 10 bits, HSB <-> LSB = BIG <-> LL
        const unsigned int allCpu = 0x3FF; // 10 bits with 1

        /* making CPU mask */
        cpu_set_t cpuset;   CPU_ZERO(&cpuset);
        for (unsigned int mask = 1, cpu_no = 0; mask < allCpu; mask <<= 1, cpu_no++) {
            if (mask & targetCpu) {
                CPU_SET(cpu_no, &cpuset);
            }
        }

        pid_t tid = gettid();
        mfllLogD("affine ME(%d), tid=%d", index, tid);
        auto status = mt_sched_setaffinity(tid, sizeof(cpu_set_t), &cpuset);
        if (status != 0) {
            mfllLogE("mt_sched_setaffinity tid(%d) failed with %#x", tid, status);
        }
    };

    void unAffinieMotionEstimation(int index)
    {
        std::unique_lock<std::mutex> _l(m_mutex);
        pid_t tid = gettid();
        mfllLogD("unaffine ME(%d), tid=%d", index, tid);
        auto status = mt_sched_exitaffinity(tid);
        if (status != 0) {
            mfllLogE("mt_sched_exitaffinity tid(%d) failed with %#x", tid, status);
        }
    };
}; // class PerfServiceHanlder }}}
static PerfServiceHanlder g_perfServiceHandler;

static std::vector<enum EventType> EVENTS_TO_LISTEN_INITIALIZER(void)
{
    std::vector<enum EventType> v;
    #define LISTEN(x) v.push_back(x)
    LISTEN(EventType_MotionEstimation);
    LISTEN(EventType_MotionCompensation);
    #undef LISTEN
    return v;
}
static vector<enum EventType> g_eventsToListen = EVENTS_TO_LISTEN_INITIALIZER();


MfllFeaturePerf::MfllFeaturePerf(void)
{
    std::unique_lock<std::mutex> _l(g_mutex);
    if (g_refCount++ <= 0) {
        g_launchEnableCPU = std::async(std::launch::async, []()->void{
                g_perfServiceHandler.initPerfService();
            });
    }
}

MfllFeaturePerf::~MfllFeaturePerf(void)
{
    std::unique_lock<std::mutex> _l(g_mutex);
    if (--g_refCount <= 0) {
        g_perfServiceHandler.uninitPerfService();
    }
}

void MfllFeaturePerf::onEvent(enum EventType t, MfllEventStatus_t &status, void *mfllCore, void *param1, void *param2)
{
    if (status.ignore != 0)
        return;

    int index = (int)(long long)param1;

    switch(t) {
    case EventType_MotionEstimation:
        g_perfServiceHandler.affineMotionEstimation(index);
        break;
    case EventType_MotionCompensation:
        break;
    default:
        break;
    }
}

void MfllFeaturePerf::doneEvent(enum EventType t, MfllEventStatus_t &status, void *mfllCore, void *param1, void *param2)
{
    if (status.ignore != 0)
        return;

    int index = (int)(long long)param1;

    switch(t) {
    case EventType_MotionEstimation:
        g_perfServiceHandler.unAffinieMotionEstimation(index);
        break;
    case EventType_MotionCompensation:
        break;
    default:
        break;
    }
}

vector<enum EventType> MfllFeaturePerf::getListenedEventTypes(void)
{
    return g_eventsToListen;
}
