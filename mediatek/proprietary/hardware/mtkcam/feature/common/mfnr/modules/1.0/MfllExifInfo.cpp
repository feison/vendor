#include "MfllLog.h"
#include "MfllExifInfo.h"

#include <camera_custom_nvram.h>


using namespace mfll;

IMfllExifInfo* IMfllExifInfo::createInstance(void)
{
    return reinterpret_cast<IMfllExifInfo*>(new MfllExifInfo);
}

void IMfllExifInfo::destroyInstance(void)
{
    decStrong((void*)this);
}

//-----------------------------------------------------------------------------

MfllExifInfo::MfllExifInfo()
{
    m_dataMap[MF_TAG_VERSION] = 2; // version 3.2
}

MfllExifInfo::~MfllExifInfo()
{
}

enum MfllErr MfllExifInfo::init()
{
    std::unique_lock<std::mutex> _l(m_mutex);
    return MfllErr_Ok;
}

enum MfllErr MfllExifInfo::updateInfo(const MfllCoreDbgInfo_t &dbgInfo)
{
    std::unique_lock<std::mutex> _l(m_mutex);
    m_dataMap[MF_TAG_MAX_FRAME_NUMBER] = dbgInfo.frameCapture;
    m_dataMap[MF_TAG_PROCESSING_NUMBER] = dbgInfo.frameBlend;

    /* iso & exposure */
    m_dataMap[MF_TAG_EXPOSURE] = dbgInfo.exp;
    m_dataMap[MF_TAG_ISO] = dbgInfo.iso;
    m_dataMap[MF_TAG_AIS_EXPOSURE] = dbgInfo.ori_iso;
    m_dataMap[MF_TAG_AIS_ISO] = dbgInfo.ori_exp;
    m_dataMap[MF_TAG_AIS_CALCULATED_EXPOSURE] = dbgInfo.exp;
    m_dataMap[MF_TAG_AIS_CALCULATED_ISO] = dbgInfo.iso;

    m_dataMap[MF_TAG_RAW_WIDTH] = dbgInfo.width;
    m_dataMap[MF_TAG_RAW_HEIGHT] = dbgInfo.height;
    m_dataMap[MF_TAG_BSS_ENABLE] = dbgInfo.bss_enable;
    m_dataMap[MF_TAG_MEMC_SKIP] = dbgInfo.memc_skip;
    m_dataMap[MF_TAG_MFB_MODE] = dbgInfo.shot_mode;
    return MfllErr_Ok;
}

enum MfllErr MfllExifInfo::updateInfo(IMfllNvram *pNvram)
{
    size_t chunkSize;
    const char *pChunk = pNvram->getChunk(&chunkSize);
    if (pChunk == NULL) {
        mfllLogE("get NVRAM chunk failed");
        return MfllErr_UnexpectedError;
    }
    char *pMutableChunk = const_cast<char*>(pChunk);

    /* reading NVRAM */
    NVRAM_CAMERA_FEATURE_STRUCT *n = reinterpret_cast<NVRAM_CAMERA_FEATURE_STRUCT*>(pMutableChunk);
    m_dataMap[MF_TAG_MFNR_ISO_TH]               = n->mfll.mfll_iso_th;
    m_dataMap[MF_TAG_AIS_ISO_TH0]               = n->mfll.mfll_iso_th;
    m_dataMap[MF_TAG_AIS_EXPOSURE_TH0]          = n->mfll.ais_exp_th;
    m_dataMap[MF_TAG_AIS_EXPOSURE_TH1]          = n->mfll.ais_exp_th;
    m_dataMap[MF_TAG_AIS_ADVANCED_ENABLE]       = n->mfll.ais_advanced_tuning_en;
    m_dataMap[MF_TAG_AIS_ADVANCED_MAX_EXPOSURE] = n->mfll.ais_advanced_max_exposure;
    m_dataMap[MF_TAG_AIS_ADVANCED_MAX_ISO]      = n->mfll.ais_advanced_max_iso;
    return MfllErr_Ok;
}

enum MfllErr MfllExifInfo::updateInfo(unsigned int key, uint32_t value)
{
    std::unique_lock<std::mutex> _l(m_mutex);
    m_dataMap[key] = value;
    return MfllErr_Ok;
}

uint32_t MfllExifInfo::getInfo(unsigned int key)
{
    std::unique_lock<std::mutex> _l(m_mutex);
    return m_dataMap[key];
}

const std::map<unsigned int, uint32_t>& MfllExifInfo::getInfoMap()
{
    return m_dataMap;
}

unsigned int MfllExifInfo::getVersion()
{
    return (unsigned int)m_dataMap[MF_TAG_VERSION];
}
