/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "PROC"

#include "HDR.h"
#include "HDRProc.h"
#include "common/hdr/Platform.h"

#include <aaa_hal_common.h>

#include <common/hdr/utils/Debug.h>

using namespace NSCam;
using namespace NS3Av3;

// ---------------------------------------------------------------------------

IHDRProc& IHDRProc::createInstance()
{
    return HDRProc::getInstance();
}

// ---------------------------------------------------------------------------

ANDROID_SINGLETON_STATIC_INSTANCE(HDRProc);

HDRProc::HDRProc()
{
    mvHDR.clear();
}

HDRProc::~HDRProc()
{
    uninitLocked();
}

MBOOL HDRProc::init(MINT32 openID, HDRHandle& handle)
{
    AutoMutex l(mHDRlock);

    sp<NSCam::HDR> obj = new NSCam::HDR("hdr", 0, openID);
    if (obj == NULL)
    {
        HDR_LOGE("[HDRProcInit] init HDRProc failed");
        return MFALSE;
    }

    // add HDR instance
    {
        handle.id = reinterpret_cast<MUINTPTR>(obj.get());
        mvHDR.add(handle.id, obj);
        HDR_LOGD("add HDR(%#08" PRIuPTR ")", handle.id);
    }

    HDR_LOGD("[HDRProcInit] init HDRProc done for camera(%d)", openID);

    return MTRUE;
}

MBOOL HDRProc::uninit(const HDRHandle& handle)
{
    AutoMutex l(mHDRlock);

    return uninitLocked(&handle);
}

MBOOL HDRProc::uninitLocked(const HDRHandle* handle)
{
    if (mvHDR.isEmpty())
        return MTRUE;

    if (handle == NULL)
    {
        // clear all HDR instances
        mvHDR.clear();
        HDR_LOGD("remove all HDR instances");
    }
    else
    {
        // clear specific HDR instance
        mvHDR.removeItem(handle->id);
        HDR_LOGD("remove HDR(%#08" PRIuPTR ")", handle->id);
    }

    return MTRUE;
}

MBOOL HDRProc::setParam(
        const HDRHandle& handle,
        MUINT32 paramId, MUINTPTR iArg1, MUINTPTR iArg2)
{
    if ((paramId <= HDRProcParam_Begin) || (paramId >= HDRProcParam_Num))
    {
        HDR_LOGE("[HDRProc::setParam] invalid paramId:%d", paramId);
        return MFALSE;
    }

    AutoMutex l(mHDRlock);

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    return mvHDR.valueFor(handle.id)->setParam(paramId, iArg1, iArg2);
}

MBOOL HDRProc::getParam(
        const HDRHandle& handle,
        MUINT32 paramId, MUINT32 & rArg1, MUINT32 & rArg2)
{
    if ((paramId <= HDRProcParam_Begin) || (paramId >= HDRProcParam_Num))
    {
        HDR_LOGE("[HDRProc::getParam] invalid paramId:%d", paramId);
        return MFALSE;
    }

    AutoMutex l(mHDRlock);

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    return mvHDR.valueFor(handle.id)->getParam(paramId, rArg1, rArg2);
}

MBOOL HDRProc::setShotParam(
        const HDRHandle& handle,
        MSize& pictureSize, MSize& postviewSize, MRect& cropRegion)
{
    MBOOL ret = MTRUE;

    HDRProc_ShotParam param = {pictureSize, postviewSize, cropRegion};

    HDR_LOGD("[HDRProc::setShotParam] size(%dx%d) postview(%dx%d) crop(%d,%d,%dx%d)",
            pictureSize.w, pictureSize.h,
            // TODO: check if postivew setting can be removed
            postviewSize.w, postviewSize.h,
            cropRegion.leftTop().x, cropRegion.leftTop().y,
            cropRegion.width(), cropRegion.height());

    {
        AutoMutex l(mHDRlock);

        if (mvHDR.indexOfKey(handle.id) < 0)
        {
            HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
            return MFALSE;
        }

        if (!mvHDR.valueFor(handle.id)->setShotParam(&param))
        {
            HDR_LOGE("[HDRProc::setShotParam] setShotParam failed");
            ret = MFALSE;
        }
    }

    return ret;
}

MBOOL HDRProc::setJpegParam(
        const HDRHandle& handle,
        MSize& jpegSize, MSize& thumbnailSize, MINT32 orientation)
{
    MBOOL ret = MTRUE;

    HDRProc_JpegParam param = {jpegSize, thumbnailSize, orientation};

    HDR_LOGD("[HDRProc::setJpegParam] jpeg(%dx%d) thumbnail(%dx%d) orientation(%d)",
            jpegSize.w, jpegSize.h, thumbnailSize.w, thumbnailSize.h, orientation);

    {
        AutoMutex l(mHDRlock);

        if (mvHDR.indexOfKey(handle.id) < 0)
        {
            HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
            return MFALSE;
        }

        if (!mvHDR.valueFor(handle.id)->setJpegParam(&param))
        {
            HDR_LOGE("[HDRProcSetJpegParam] setJpegParam failed");
            ret = MFALSE;
        }
    }

    return ret;
}

MBOOL HDRProc::prepare(const HDRHandle& handle)
{
    AutoMutex l(mHDRlock);

    MBOOL ret = MTRUE;

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    const sp<NSCam::HDR>& obj(mvHDR.valueFor(handle.id));

    ret = obj->updateInfo();
    if (MTRUE != ret)
    {
        HDR_LOGE("[HDRProcPrepare] updateInfo failed");
        goto lbExit;
    }

    ret = obj->prepare();
    if (MTRUE != ret)
    {
        HDR_LOGE("[HDRProcPrepare] createSourceAndSmallImg fail");
    }

lbExit:
    return ret;
}

MBOOL HDRProc::addInputFrame(
        const HDRHandle& handle,
        MINT32 frameIndex, const sp<IImageBuffer>& inBuffer)
{
    AutoMutex l(mHDRlock);

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    return mvHDR.valueFor(handle.id)->addInputFrame(frameIndex, inBuffer);
}

MBOOL HDRProc::addOutputFrame(
        const HDRHandle& handle,
        HDROutputType type, sp<IImageBuffer>& outBuffer)
{
    AutoMutex l(mHDRlock);

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    return mvHDR.valueFor(handle.id)->addOutputFrame(type, outBuffer);
}

MBOOL HDRProc::start(const HDRHandle& handle)
{
    AutoMutex l(mHDRlock);

    MBOOL ret = MTRUE;

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    ret = mvHDR.valueFor(handle.id)->process();
    if (MTRUE != ret)
    {
        HDR_LOGE("[HDRProcStart] process failed");
    }

    return ret;
}

MBOOL HDRProc::release(const HDRHandle& handle)
{
    AutoMutex l(mHDRlock);

    MBOOL ret = MTRUE;

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return MFALSE;
    }

    ret = mvHDR.valueFor(handle.id)->release();
    if (MTRUE != ret)
    {
        HDR_LOGE("[HDRProcRelease] release failed");
    }

    return ret;
}

MBOOL HDRProc::getHDRCapInfo(
        const HDRHandle& handle,
        MINT32& i4FrameNum,
        Vector<MUINT32>& vu4Eposuretime,
        Vector<MUINT32>& vu4SensorGain,
        Vector<MUINT32>& vu4FlareOffset)
{
    MBOOL ret = MTRUE;
    Vector<NS3Av3::CaptureParam_T> rCap3AParam;

    {
        AutoMutex l(mHDRlock);

        if (mvHDR.indexOfKey(handle.id) < 0)
        {
            HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
            return MFALSE;
        }

        mvHDR.valueFor(handle.id)->getCaptureInfo(rCap3AParam, i4FrameNum);
    }

    HDR_LOGD("[HDRProc::getHDRCapInfo] hdrFrameNum(%d)", i4FrameNum);

    Vector<NS3Av3::CaptureParam_T>::iterator it = rCap3AParam.begin();
    while (it != rCap3AParam.end())
    {
        HDR_LOGD("=================\n" \
                "[HDRProc::getHDRCapInfo] u4Eposuretime(%u) " \
                " u4AfeGain(%u) u4IspGain(%u) u4RealISO(%u) u4FlareOffset(%u)",
                it->u4Eposuretime, it->u4AfeGain, it->u4IspGain,
                it->u4RealISO, it->u4FlareOffset);

        vu4Eposuretime.push_back(it->u4Eposuretime);
        vu4SensorGain.push_back(it->u4AfeGain);
        vu4FlareOffset.push_back(it->u4FlareOffset);

        it++;
    }

    return ret;
}

MVOID HDRProc::setCompleteCallback(
        const HDRHandle& handle,
        HDRProcCompleteCallback_t completeCB, MVOID* user)
{
    AutoMutex l(mHDRlock);

    if (mvHDR.indexOfKey(handle.id) < 0)
    {
        HDR_LOGE("cannot find HDR(%#08" PRIuPTR ")", handle.id);
        return;
    }

    mvHDR.valueFor(handle.id)->setCompleteCallback(completeCB, user);
}
