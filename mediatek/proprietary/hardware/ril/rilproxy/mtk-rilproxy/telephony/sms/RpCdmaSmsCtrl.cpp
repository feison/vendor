/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include "RpCdmaSmsCtrl.h"

/*****************************************************************************
 * Class RpSmsCtrl
 *****************************************************************************/
RFX_IMPLEMENT_CLASS("RpCdmaSmsCtrl", RpCdmaSmsCtrl, RpSmsCtrlBase);

RpCdmaSmsCtrl::RpCdmaSmsCtrl() {
    setName(String8("RpCdmaSmsCtrl"));
}

void RpCdmaSmsCtrl::onInit() {
    RpSmsCtrlBase::onInit(); // Required: invoke super class implementation

    log(String8("onInit"));
    const int request_id_list[] = {
        RIL_REQUEST_CDMA_SEND_SMS
    };
    registerToHandleRequest(request_id_list, (sizeof(request_id_list)/sizeof(int)));
}

bool RpCdmaSmsCtrl::onHandleRequest(const sp<RfxMessage>& message) {
    switch (message->getId()) {
        case RIL_REQUEST_CDMA_SEND_SMS:
            onCdmaSmsSend(message);
            return true;
        default:
            break;
    }
    return false;
}

bool RpCdmaSmsCtrl::isCtSimCard(void) {
    bool ret = false;
    int cdma_card_type = getStatusManager()->getIntValue(RFX_STATUS_KEY_CDMA_CARD_TYPE);
    if (cdma_card_type == CT_4G_UICC_CARD ||
            cdma_card_type == CT_UIM_SIM_CARD ||
            cdma_card_type == CT_3G_UIM_CARD) {
        ret = true;
    }
    return ret;
}

void RpCdmaSmsCtrl::onCdmaSmsSend(const sp<RfxMessage>& message) {
    if (isCtSimCard()) {
        Parcel *p = message->getParcel();
        int32_t teleServiceId = p->readInt32();
        /*
         * According to the spec of China Telecom, it needs the teleservice
         * id of long sms is TELESERVICE_WMT(0x1002). However, AOSP will use
         * TELESERVICE_WEMT(0x1005) as the teleservice id in SmsMessage.java.
         * In fact, most of China Telecom's network will work fine for 0x1005.
         * Only in Senzhen we will meet the problem that the message center cannot
         * handle the teleservice id 0x1005, so we need to convert the teleservice
         * id from 0x1005 to 0x1002 to make sure the long sms can work in all China
         * Telecom's network.
         */
        if (teleServiceId == TELESERVICE_WEMT) {
            message->resetParcelDataStartPos();
            p->writeInt32(TELESERVICE_WMT);
        }
    }
    requestToRild(message);
}