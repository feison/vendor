/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/* //hardware/viatelecom/ril/viatelecom-withuim-ril/viatelecom-withuim-ril.c
**
** Copyright 2009, Viatelecom Inc.
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

#include <telephony/mtk_ril.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <alloca.h>
#include <getopt.h>
#include <sys/socket.h>
#include <cutils/sockets.h>
#include <termios.h>
#include "atchannel.h"
#include "oem-support.h"
#include "ril_callbacks.h"
#include <utils/Log.h>

#include <cutils/properties.h>
#include <netdb.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <linux/route.h>

 // for BT_SAP
#include <hardware/ril/librilutils/proto/sap-api.pb.h>
#include "pb_decode.h"
#include "pb_encode.h"


#define STK_CHANNEL_CTX getRILChannelCtxFromToken(t)


#define SETUP_MENU_CMD_DETAIL_CODE "81030125"
#define SETUP_EVENT_LIST_CMD_DETAIL_CODE "81030105"

static void requestUTKProfileDownload(void *data, size_t datalen, RIL_Token t);
static void requestUTKCallConfirmed(void *data, size_t datalen, RIL_Token t);
static void requestUTKSendEnvelopeCommand(void *data, size_t datalen, RIL_Token t);
static void requestUTKSendTerminalResponse(void *data, size_t datalen, RIL_Token t);
static void requestUTKGetLocalInfo(void *data, size_t datalen, RIL_Token t);
static void requestUTKRefresh(void *data, size_t datalen, RIL_Token t);
static void requestUTKSetupMenu(void *data, size_t datalen, RIL_Token t);
static void requestSTKQueryMenu(void *data, size_t datalen, RIL_Token t);

static int checkStkCommandType(char *cmd_str);

//Cache proactive command start
static bool aIs_stk_service_running = false;
static bool aIs_proac_cmd_queued = false;
static bool aIs_event_notify_queued = false;
static char* pProactive_cmd[5] = {0};

int queued_proc_cmd_num = 0;

void setStkFlag(bool flag, bool source) {
    source = flag;
    LOGD("setStkFlag[%d].", source);
}

char getUtkFlag(bool source) {
    LOGD("getUtkFlag[%d].", source);
    return source;
}

char* getUtkCachedProCmdData(char** source, int number) {
    return *(source + number);
}

void setUtkCachedProCmdData(char** source, char* pCmd, int number) {
    *(source + number) = pCmd;
    LOGD("setUtkCachedProCmdData number:%d, [%s].",
        number, *(source + number));
}
void setUtkServiceRunningFlag(bool flag) {
    aIs_stk_service_running = flag;
    LOGD("setUtkServiceRunningFlag value:[%d].", aIs_stk_service_running);
}

void resetUtkStatus() {
    setUtkServiceRunningFlag(false);
    queued_proc_cmd_num = 0;
}
void setUtkProCmdQueuedFlag(bool flag) {
    aIs_proac_cmd_queued = flag;
    LOGD("setUtkProCmdQueuedFlag value:[%d].", aIs_proac_cmd_queued);
}
//Cache proactive command end

int rilUtkMain(int request, void *data, size_t datalen, RIL_Token t)
{
    switch (request) {
        case RIL_REQUEST_REPORT_STK_SERVICE_IS_RUNNING:
            requestUTKProfileDownload(data, datalen, t);
            break;
        case RIL_REQUEST_STK_HANDLE_CALL_SETUP_REQUESTED_FROM_SIM:
            requestUTKCallConfirmed(data, datalen, t);
            break;
        case RIL_REQUEST_STK_SEND_ENVELOPE_COMMAND:
            requestUTKSendEnvelopeCommand(data, datalen, t);
            break;
        case RIL_REQUEST_STK_SEND_TERMINAL_RESPONSE:
            requestUTKSendTerminalResponse(data, datalen, t);
            break;
        default:
            return 0; /* no matched request */
    }
    return 1;
}

int rilUtkUnsolicited(const char *s, const char *sms_pdu)
{
    if (strStartsWith(s, "+CRSM:")) {
        char* proactive_cmd = NULL;

        ParseAtcmdCRSM(s, &proactive_cmd);

        if (!proactive_cmd) {
            RIL_onUnsolicitedResponse(RIL_UNSOL_STK_SESSION_END, NULL, 0);
        } else {
            RIL_onUnsolicitedResponse(RIL_UNSOL_STK_PROACTIVE_COMMAND,
                    proactive_cmd, strlen(proactive_cmd));
            free(proactive_cmd);
        }
        return 1;
    } else if (strStartsWith(s, "+UTKURC:")) {
        char* proactive_command = NULL;
        ParseUtkProcmdStr(s, &proactive_command);

        int str_len = 0;
        if (NULL != proactive_command) {
            str_len = strlen(proactive_command);
        }
        char result[5] = {0};
        memcpy(result, proactive_command + (str_len - 4), 4);
        memset(proactive_command + (str_len - 4), 0, 4);
        if (NULL != proactive_command) {
            LOGD("NULL != proactive_command");
            RIL_onUnsolicitedResponse (
                RIL_UNSOL_STK_PROACTIVE_COMMAND,
                proactive_command, strlen(proactive_command));
            free(proactive_command);
        }
        return 1;
    } else if (strStartsWith(s, "+UTKIND:")) {
        char* proactive_cmd = NULL;
        int type_pos = 0;
        ParseUtkRawData(s, &proactive_cmd);
        char *temp_str = NULL;
        if (!proactive_cmd ) {
            RIL_onUnsolicitedResponse(RIL_UNSOL_STK_SESSION_END, NULL, 0);
        } else {
            int str_len = strlen(proactive_cmd);
            temp_str = (char*)calloc(1, str_len + 1);
            memset(temp_str, 0x0, str_len + 1);
            memcpy(temp_str, proactive_cmd, str_len);

            bool isStkServiceRunning = false;
            isStkServiceRunning = getUtkFlag(aIs_stk_service_running);
            LOGD("rilUtkUnsolicited check %d.",isStkServiceRunning);

            if (false == isStkServiceRunning) {
                setUtkProCmdQueuedFlag(true);
                setUtkCachedProCmdData(pProactive_cmd, temp_str, queued_proc_cmd_num);
                LOGD("UTK service is not running yet.[%s],number[%d]",temp_str, queued_proc_cmd_num);
                queued_proc_cmd_num++;
                free(proactive_cmd);
                return 1;
            } else {
                if (temp_str[2] <= '7') {
                    type_pos = 10;
                } else {
                    type_pos = 12;
                }

                if (checkStkCommandType(&(temp_str[type_pos])) == CMD_REFRESH) {
                    onSimRefresh(&(temp_str[type_pos - 6]));
                }
                if (NULL != temp_str) {
                    free(temp_str);
                }
                RIL_onUnsolicitedResponse(RIL_UNSOL_STK_PROACTIVE_COMMAND, proactive_cmd, strlen(proactive_cmd));
            }
            free(proactive_cmd);
       }
        return 1;
    } else if (strStartsWith(s, "+UTKNOTIFY:")) {
        char* proactive_cmd = NULL;
        ParseUtkRawData(s, &proactive_cmd);
        if ( proactive_cmd ){
            if (strStartsWith(proactive_cmd, "81")){
                //ignore
                LOGD("ignore +UTKNOTIFY = %s", proactive_cmd);
            } else {
                RIL_onUnsolicitedResponse(RIL_UNSOL_STK_PROACTIVE_COMMAND, proactive_cmd, strlen(proactive_cmd));
            }

            free(proactive_cmd);
        }
        return 1;
    }
    else if (strStartsWith(s, "+UTKCALL:")) {
        //not used
        return 1;
    } else if (strStartsWith(s, "+EUTKST:")) {
        // remote SIM switch,need reset utk cache flag.
        if (aIs_stk_service_running == true) {
            resetUtkStatus();
        }
        return 1;
    }
    return 0;
}

static int checkStkCommandType(char *cmd_str) {
    char temp_str[3] = {0};
    char *end;
    int cmd_type = 0;
    memcpy(temp_str, cmd_str, 2);
    cmd_type = strtoul(temp_str, &end, 16);
    cmd_type = 0x7F & cmd_type;
    return cmd_type;
}

void onSimRefresh(char* urc) {
    int *cmd = NULL;
    int cmd_length = 0;
    cmd_length = 2 * sizeof(int);
    cmd = (int *)calloc(1, cmd_length);
    switch(urc[9]) { // urc point to cmd_deatil tag urc[9] mean refresh type
        case '0':
            cmd[0] = SIM_INIT_FULL_FILE_CHANGE;
            break;
        case '1':
            cmd[0] = SIM_FILE_UPDATE;
            break;
        case '2':
            cmd[0] = SIM_INIT_FILE_CHANGE;
            break;
        case '3':
            cmd[0] = SIM_INIT;
            break;
        case '4':
            cmd[0] = SIM_RESET;
            break;
        case '5':
            cmd[0] = APP_INIT;
            break;
        case '6':
            cmd[0] = SESSION_RESET;
            break;
        default:
            LOGD("Refresh type does not support.");
            free(cmd);
            return;
   }
    cmd[1] = 0;
    RIL_onUnsolicitedResponse(
        RIL_UNSOL_SIM_REFRESH,
        cmd, strlen(cmd));
    free(cmd);
}

static void requestUTKProfileDownload(void *data, size_t datalen, RIL_Token t) {
  setUtkServiceRunningFlag(true);
  int err = at_send_command("AT+UTKPD", NULL, STK_CHANNEL_CTX);
  LOGD("requestUTKProfileDownload err=%d", err);

  LOGD("STK service is running is_proac_cmd_queued[%d].",
  getUtkFlag(aIs_proac_cmd_queued));
  if(true == getUtkFlag(aIs_proac_cmd_queued)) {
      int i = 0;
      for (i = 0; i < 5 ; i++) {
          char *cmd = (char *)getUtkCachedProCmdData(pProactive_cmd, i);
          LOGD("Queued Proactive Cmd:[%s].", cmd);
          if(NULL != cmd) {
              int type_pos = 0;
              char *tempStr = NULL;
              int str_len = strlen(cmd);
              tempStr = (char*)calloc(1, str_len + 1);
              memset(tempStr, 0x0, str_len + 1);
              memcpy(tempStr, cmd, str_len);
              if (tempStr[2] <= '7') {
                  type_pos = 10;
              } else {
                  type_pos = 12;
              }

              if (checkStkCommandType(&(tempStr[type_pos])) == CMD_REFRESH) {
                  onSimRefresh(&(tempStr[type_pos - 6]));
              }
              if (NULL != tempStr) {
                  free(tempStr);
              }
              RIL_onUnsolicitedResponse(RIL_UNSOL_STK_PROACTIVE_COMMAND, cmd, strlen(cmd));
              LOGD("clear queud command");
              setUtkCachedProCmdData(pProactive_cmd, NULL, i);
          }
      }
      setUtkProCmdQueuedFlag(false);
  }

  if(err < 0) {
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
  } else {
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
  }
}

static void requestUTKCallConfirmed(void *data, size_t datalen, RIL_Token t)
{
  char *cmd = NULL;
  int err = 0;

  asprintf(&cmd, "AT+UTKCONF=%d", ((int *)data)[0]);
  err = at_send_command(cmd, NULL, STK_CHANNEL_CTX);
  free(cmd);

  if(err < 0)
  {
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
  }
  else
  {
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
  }
}

static void requestUTKSendEnvelopeCommand(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    int err;
    char *cmd = NULL;
    char* cmd_data = (char*)data;

    //asprintf(&cmd, "at+crsm=194,0,0,0,0,%d,\"%s\"", strlen(cmd_data) / 2, cmd_data);
    asprintf(&cmd, "AT+UTKENV=%s", cmd_data);
    err = at_send_command(cmd, NULL, STK_CHANNEL_CTX);
    free(cmd);

    if ( err == 0 )
        RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    else
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
}

static void requestUTKSendTerminalResponse(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    int err;
    char *cmd = NULL;
    char* cmd_data = (char*)data;
    //The TR of SETUP_EVENT_LIST and SETUP_MENU will return at MD.
    if (strStartsWith((char*)data, SETUP_EVENT_LIST_CMD_DETAIL_CODE) ||
           strStartsWith((char*)data,SETUP_MENU_CMD_DETAIL_CODE)) {
        LOGD("Ignore TR of set up event list and set up menu.");
        RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
        return;
    }
    //asprintf(&cmd, "at+crsm=20,0,0,0,0,%d,\"%s\"", strlen(cmd_data) / 2, cmd_data);
    asprintf(&cmd, "AT+UTKTERM=%s", cmd_data);
    err = at_send_command(cmd, NULL, STK_CHANNEL_CTX);
    free(cmd);

    if ( err == 0 )
        RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    else
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
}

/// M: [C2K][IR] Support CT 3g dual Mode card IR feature. @{
/* Switch UTK/STK mode. */
int switchStkUtkMode(int mode, RIL_Token t) {
    int err = 0;
    ATResponse *p_response = NULL;
    LOGD("ril_utk, switchStkUtkMode(), mode=%d.", mode);
    // AT+EUTK
    char* cmd;
    err = asprintf(&cmd, "AT+EUTK=%d", mode);
    LOGD("ril_utk, switchStkUtkMode(), send command %s.", cmd);
    err = at_send_command(cmd, &p_response, STK_CHANNEL_CTX);
    free(cmd);
    at_response_free(p_response);
    p_response = NULL;
    return err;
}
/// M: [C2K][IR] Support CT 3g dual Mode card IR feature. @}

//Add for BT SAP end
char* StkbtSapMsgIdToString(MsgId msgId) {
    switch (msgId) {
        case MsgId_RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS:
            return "BT_SAP_CARD_READER_STATUS";
        default:
            return "BT_SAP_UNKNOWN_MSG_ID";
    }
}

/* Response value for Card Reader status
 * bit 8: Card in reader is powered on or not (powered on, this bit=1)
 * bit 7: Card inserted or not (Card inserted, this bit=1)
 * bit 6: Card reader is ID-1 size or not (our device is not ID-1 size, so this bit =0)
 * bit 5: Card reader is present or not (for our device, this bit=1)
 * bit 4: Card reader is removable or not (for our device, it is not removable, so this bit=0)
 * bit 3-1: Identifier of the Card reader (for our device: ID=0)
 * normal case, card reader status of our device = 0x11010000 = 0xD0
 * default case, card reader status of our device = 0x00010000 = 0x10
 */

void requestBtSapGetCardStatus(void *data, size_t datalen, RIL_Token t,
        RIL_SOCKET_ID rid) {
    RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_REQ *req = NULL;
        (RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_REQ*)data;
    RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_RSP rsp;
    BtSapStatus status = -1;

    LOGD("[BTSAP] requestBtSapGetCardStatus start, (%d)", rid);

    req = (RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_REQ*)
        malloc(sizeof(RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_REQ));
    memset(req, 0, sizeof(RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_REQ));

    status = queryBtSapStatus(rid);
    LOGD("[BTSAP] requestBtSapGetCardStatus status : %d", status);

    //decodeStkBtSapPayload(MsgId_RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS, data, datalen, req);
    memset(&rsp, 0, sizeof(RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_RSP));
    rsp.CardReaderStatus = BT_SAP_CARDREADER_RESPONSE_DEFAULT;
    rsp.response = RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_RSP_Response_RIL_E_SUCCESS;
    rsp.has_CardReaderStatus = true;   //always true

    if (isSimInserted(rid)) {
        LOGD("[BTSAP] requestBtSapGetCardStatus, Sim inserted");
        rsp.CardReaderStatus = rsp.CardReaderStatus | BT_SAP_CARDREADER_RESPONSE_SIM_INSERT;
    }

    if (status == BT_SAP_CONNECTION_SETUP || status == BT_SAP_ONGOING_CONNECTION
        || status == BT_SAP_POWER_ON) {
        rsp.CardReaderStatus = rsp.CardReaderStatus | BT_SAP_CARDREADER_RESPONSE_READER_POWER;
    }

    LOGD("[BTSAP] requestBtSapGetCardStatus, CardReaderStatus result : %x", rsp.CardReaderStatus);
    sendStkBtSapResponseComplete(t, RIL_E_SUCCESS, MsgId_RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS,
        &rsp);
    free(req);

    RLOGD("[BTSAP] requestBtSapGetCardStatus end");
}

void decodeStkBtSapPayload(MsgId msgId, void *src, size_t srclen, void *dst) {
    pb_istream_t stream;
    const pb_field_t *fields = NULL;

    RLOGD("[BTSAP] decodeStkBtSapPayload start (%s)", StkbtSapMsgIdToString(msgId));
    if (dst == NULL || src == NULL) {
        RLOGE("[BTSAP] decodeStkBtSapPayload, dst or src is NULL!!");
        return;
    }

    switch (msgId) {
        case MsgId_RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS:
            fields = RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_REQ_fields;
            break;
        default:
            RLOGE("[BTSAP] decodeStkBtSapPayload, MsgId is mistake!");
            return;
    }

    stream = pb_istream_from_buffer((uint8_t *)src, srclen);
    if (!pb_decode(&stream, fields, dst) ) {
        RLOGE("[BTSAP] decodeStkBtSapPayload, Error decoding protobuf buffer : %s", PB_GET_ERROR(&stream));
    } else {
        RLOGD("[BTSAP] decodeStkBtSapPayload, Success!");
    }
}

void sendStkBtSapResponseComplete(RIL_Token t, RIL_Errno ret, MsgId msgId, void *data) {
    const pb_field_t *fields = NULL;
    size_t encoded_size = 0;
    uint32_t written_size = 0;
    size_t buffer_size = 0;
    pb_ostream_t ostream;
    bool success = false;
    ssize_t written_bytes;
    int i = 0;

    RLOGD("[BTSAP] sendStkBtSapResponseComplete, start (%s)", StkbtSapMsgIdToString(msgId));

    switch (msgId) {
        case MsgId_RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS:
            fields = RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS_RSP_fields;
            break;
        default:
            RLOGE("[BTSAP] sendStkBtSapResponseComplete, MsgId is mistake!");
            return;
    }

    if ((success = pb_get_encoded_size(&encoded_size, fields, data)) &&
            encoded_size <= INT32_MAX) {
        buffer_size = encoded_size;
        uint8_t buffer[buffer_size];
        ostream = pb_ostream_from_buffer(buffer, buffer_size);
        success = pb_encode(&ostream, fields, data);

        if(success) {
            RLOGD("[BTSAP] sendStkBtSapResponseComplete, Size: %d (0x%x) Size as written: 0x%x",
                encoded_size, encoded_size, written_size);
            // Send response
            RIL_SAP_onRequestComplete(t, ret, buffer, buffer_size);
        } else {
            RLOGE("[BTSAP] sendStkBtSapResponseComplete, Encode failed!");
        }
    } else {
        RLOGE("Not sending response type %d: encoded_size: %u. encoded size result: %d",
        msgId, encoded_size, success);
    }
}

extern int rilStkBtSapMain(int request, void *data, size_t datalen, RIL_Token t,
        RIL_SOCKET_ID rid) {
    switch (request) {
        case MsgId_RIL_SIM_SAP_TRANSFER_CARD_READER_STATUS:
            requestBtSapGetCardStatus(data, datalen, t, rid);
            break;
        default:
            return 0;
            break;
    }

    return 1;
}
//Add for BT SAP end

