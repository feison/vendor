package com.mediatek.connectivity;


/**
  * main class to eMBMS const variables.
  *
  */
public class CdsEmbmsConsts {
    public final static String EMBMS_STATUS = "eMbmsStatus";
    public final static String EMBMS_AUTO_ENABLED = "eMbmsAutoEnabled";
    public final static String EMBMS_SESSION_INFO = "eSessionInfo";

    public final static String EMBMS_FILE = "embms_data";

    public final static String SEP = ":";

    public static final int MAX_SESSION_NUM = 8;
}

