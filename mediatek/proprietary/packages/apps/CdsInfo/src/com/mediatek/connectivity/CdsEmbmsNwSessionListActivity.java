package com.mediatek.connectivity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mediatek.connectivity.CdsEmbmsService.EmbmsBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity for eMBMS sessions list.
 *
 */
public class CdsEmbmsNwSessionListActivity extends Activity {
    private static final String TAG = "CdsEmbms";

    private CdsEmbmsNwListAdapter mListAdapter;
    private ListView mListView;
    private List<CdsEmbmsSessionEntry> mListDataChild;
    private CdsEmbmsService mService;

    private int mLastExpandedPosition = -1;
    private boolean mBound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cds_embms_nw_group_list);
        mListView = (ListView) findViewById(R.id.list);
        Intent intent = this.getIntent();
        String data = intent.getStringExtra("data");

        // preparing list data
        prepareListData(data);
        mListAdapter = new CdsEmbmsNwListAdapter(this, mListDataChild);

        // setting list adapter
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemClick:" + position);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, CdsEmbmsService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    /**
     * Parse data entry.
     *
     */
   private CdsEmbmsSessionEntry parseDataEntry(String data) {
        //data = +EMSLUI: 3,1,"27AEA223415","3A",0
        CdsEmbmsSessionEntry entry = null;

        data = data.replace(':', ',');
        data = data.replace('\"', ' ');
        String tmpData[] = data.split(",");

        if (tmpData.length == 6) {
            String tmgi = tmpData[3].trim(); // tmgi.
            String sessionId = tmpData[4].trim(); // sessionId.
            int status = 0;
            try {
                status = Integer.parseInt(tmpData[5]); // status.
            } catch (NumberFormatException ne) {
                ne.printStackTrace();
            }
            entry = new CdsEmbmsSessionEntry(tmgi, sessionId, status);
        }
        return entry;
   }

    /*
     * Preparing the list data.
     */
    private void prepareListData(String data) {
        mListDataChild = new ArrayList<CdsEmbmsSessionEntry>();

        String[] tmpSessions = data.split("\n");
        for (int i = 0; i < tmpSessions.length; i++) {
            // Adding header data.
            CdsEmbmsSessionEntry entry = parseDataEntry(tmpSessions[i]);
            mListDataChild.add(entry);
        }
    }

    /** Defines callbacks for service binding, passed to bindService(). */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            Log.i(TAG, "onServiceConnected");
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            EmbmsBinder binder = (EmbmsBinder) service;
            mService = binder.getService();
            mListAdapter.setSrvBinder(mService);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}