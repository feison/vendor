package com.mediatek.connectivity;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

/**
  * main class to handle eMBMS test activity.
  *
  */
public class CdsEmbmsNwListAdapter extends BaseAdapter
                                  implements View.OnClickListener {
    private static final String TAG = "Cdsembms";

    private LayoutInflater mInflater;
    private Activity mContext;
    private List<CdsEmbmsSessionEntry> mDataList;
    private CdsEmbmsSessionEntry mCurrnetChild;
    private int mCurrentGroupPos;
    private CdsEmbmsService mService;

    /**
     *
     * @param context Activity
     * @param dataList The data source of list.
     * @hiden
     */
    public CdsEmbmsNwListAdapter(Activity context, List<CdsEmbmsSessionEntry> dataList) {
        this.mContext = context;
        this.mDataList = dataList;

        mInflater = (LayoutInflater) mContext.getSystemService(
                                    Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.cds_embms_nw_group_item,
                    null);
        }
        CdsEmbmsSessionEntry child = (CdsEmbmsSessionEntry) mDataList.get(position);

        if (child == null) {
            return convertView;
        }

        Button btn = (Button) convertView.findViewById(R.id.embmsSessionActivate);
        btn.setFocusable(false);
        btn.setOnClickListener(new OnItemClickListener(position));
        btn.setEnabled(true);

        btn = (Button) convertView.findViewById(R.id.embmsSessionDeactivate);
        btn.setFocusable(false);
        btn.setOnClickListener(new OnItemClickListener(position));
        btn.setEnabled(true);

        EditText txt = (EditText) convertView.findViewById(R.id.tmgi);
        txt.setText(child.getTmgi());
        txt.setEnabled(false);

        txt = (EditText) convertView.findViewById(R.id.sessionid);
        txt.setText(child.getSessionId());
        txt.setEnabled(false);

        String groupName = "Session-" + (position + 1) + ":";
        TextView txtView = (TextView) convertView.findViewById(R.id.embmsSession);
        if (child.getSessionStatus() == 1) {
            groupName += "Activated";
        } else {
            groupName += "Deactivated";
        }
        txtView.setText(groupName);

        //convertView.setOnClickListener(new OnItemClickListener(position));
        return convertView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
    }

    private void activateSession(boolean isActivated, CdsEmbmsSessionEntry child) {
        if (mService != null && child != null) {
            mService.activateEmbmsSession(isActivated, child.toCmdNwString());
        }
    }

    public void setSrvBinder(CdsEmbmsService srv) {
        mService = srv;
    }

    /**
      * Class for item click.
      *
      */
    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            Log.i(TAG, "mPosition:" + mPosition);
            mCurrnetChild = (CdsEmbmsSessionEntry) mDataList.get(mPosition);

            switch(id) {
                case R.id.embmsSessionActivate:
                    activateSession(true, mCurrnetChild);
                    break;
                case R.id.embmsSessionDeactivate:
                    activateSession(false, mCurrnetChild);
                    break;
                default:
                    break;
            }

        }
    }

}