package com.mediatek.connectivity;


/**
 * main class to handle eMBMS test activity.
 *
 */
public class CdsEmbmsSessionEntry {
    boolean mIsEnabled;
    String  mTmgi;
    String  mSessionId;
    String  mSaiList;
    String  mFreqList;
    int     mStatus;

    /**
     * @hide
     */
    public CdsEmbmsSessionEntry() {
        mIsEnabled = false;
        mTmgi = "";
        mSessionId = "";
        mSaiList = "";
        mFreqList = "";
        mStatus = 0;
    }

    /**
     * @param tmgi The value of TMGI.
     * @param sessionId The session ID.
     * @param status The status of session.
     * @hide
     */
    public CdsEmbmsSessionEntry(String tmgi, String sessionId,
                                int status) {
        mIsEnabled = false;
        mTmgi = tmgi;
        mSessionId = sessionId;
        mStatus = status;
        mSaiList = "";
        mFreqList = "";
    }

    /**
     * @param tmgi The value of TMGI.
     * @param sessionId The session ID.
     * @param saiList The list of SAI.
     * @param freqList The list of frequency.
     * @hide
     */
    public CdsEmbmsSessionEntry(String tmgi, String sessionId,
                                String saiList, String freqList) {
        mTmgi = tmgi;
        mSessionId = sessionId;
        mSaiList = saiList;
        mFreqList = freqList;
        mStatus = 0;
        mIsEnabled = false;
    }

    public boolean getIsEnabled() {
        return mIsEnabled;
    }

    public void setIsEnabled(boolean enabled) {
        mIsEnabled = enabled;
    }

    public String getTmgi() {
        return mTmgi;
    }

    public void setTmgi(String tmgi) {
        mTmgi = tmgi;
    }

    public String getSessionId() {
        return mSessionId;
    }

    public void setSessionId(String sessiondId) {
        mSessionId = sessiondId;
    }

    /*
     * Provide to get the list of SAI.
     * @return the list of SAI.
     */
    public String getSaiList() {
        return mSaiList;
    }

    /*
     * Provide to set the list of SAI.
     * @param sailist: the list of SAI.
     */
    public void setSaiList(String sailist) {
        mSaiList = sailist;
    }

    public String getFreqList() {
        return mFreqList;
    }

    public void setFreqList(String freqlist) {
        mFreqList = freqlist;
    }

    /**
     * To check the TMGI is empty or not.
     *
     * @return true if TMGI is non-empty.
     */
    public boolean isValidated() {
        if (mTmgi.length() > 0) {
            return true;
        }

        return false;
    }

    public int getSessionStatus() {
        return mStatus;
    }

    /**
     *  Utility function to convert from class to AT command string.
     *
     *  @return the AT command string.
     */
    public String toCmdString() {
        boolean bHasSai = false;
        StringBuffer buffer = new StringBuffer();
        buffer.append("\"" + mTmgi + "\"");

        if (isNonEmpty(mSessionId)) {
            buffer.append(",\"" + mSessionId + "\"");
        }

        if (isNonEmpty(mSaiList)) {
            String[] sai = mSaiList.split(",");
            int saiCount = sai.length;

            if (isNonEmpty(mSessionId)) {
                buffer.append(",," + saiCount + ",");
            } else {
                buffer.append(",,," + saiCount + ",");
            }

            for (int i = 0; i < saiCount; i++) {
                buffer.append(sai[i] + ",");
            }
            bHasSai = true;
        }

        if (isNonEmpty(mFreqList)) {
            String[] freqs = mFreqList.split(",");
            int freqCount = freqs.length;
            buffer.append(freqCount + ",");

            for (int i = 0; i < freqCount; i++) {
                buffer.append(freqs[i] + ",");
            }

            buffer.deleteCharAt(buffer.length() - 1);
        } else if (bHasSai) {
            buffer.append("0,0");
        }

        return buffer.toString();
    }

    /**
     *  Utility function to convert from class to AT command string.
     *
     *  @return the AT command string.
     */
    public String toCmdNwString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\"" + mTmgi + "\"");

        if (isNonEmpty(mSessionId)) {
            buffer.append(",\"" + mSessionId + "\"");
        }
        return buffer.toString();
    }

    private boolean isNonEmpty(String txtValue) {
        return (txtValue != null && txtValue.length() > 0);
    }
}