package com.mediatek.connectivity;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
  * main class to handle eMBMS test activity.
  *
  */
public class CdsEmbmsListAdapter extends BaseExpandableListAdapter
                                  implements View.OnClickListener {
    private static final String TAG = "CdsEmbms";

    private LayoutInflater mInflater;
    private Activity mContext;
    private List<String> mHeaders;
    private List<CdsEmbmsSessionEntry> mChildData;
    private CdsEmbmsSessionEntry mCurrnetChild;
    private int mCurrentGroupPos;
    private CdsEmbmsService mService;

    /**
     *
     * @param context Activity
     * @param laptops The header information fo list.
     * @param laptopCollections Each eMBMS session.
     * @hiden
     */
    public CdsEmbmsListAdapter(Activity context, List<String> laptops,
            List<CdsEmbmsSessionEntry> laptopCollections) {
        this.mContext = context;
        this.mHeaders = laptops;
        this.mChildData = laptopCollections;

        mInflater = (LayoutInflater) mContext.getSystemService(
                                    Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Log.i(TAG, "getChild:" + groupPosition);
        mCurrentGroupPos = groupPosition;
        return mChildData.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
        boolean isLastChild, View convertView, ViewGroup parent) {
        mCurrnetChild = (CdsEmbmsSessionEntry)
                                            getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.cds_embms_child_item, parent, false);
        }
        EditText editTxt = (EditText) convertView.findViewById(R.id.tmgi);
        editTxt.setText(mCurrnetChild.getTmgi());
        editTxt.addTextChangedListener(new GenericTextWatcher(editTxt));
        editTxt = (EditText) convertView.findViewById(R.id.sessionid);
        editTxt.setText(mCurrnetChild.getSessionId());
        editTxt.addTextChangedListener(new GenericTextWatcher(editTxt));
        editTxt = (EditText) convertView.findViewById(R.id.sailist);
        editTxt.setText(mCurrnetChild.getSaiList());
        editTxt.addTextChangedListener(new GenericTextWatcher(editTxt));
        editTxt = (EditText) convertView.findViewById(R.id.freqlist);
        editTxt.setText(mCurrnetChild.getFreqList());
        editTxt.addTextChangedListener(new GenericTextWatcher(editTxt));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String groupName = (String) getGroup(groupPosition);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.cds_embms_group_item,
                    null);
        }
        CdsEmbmsSessionEntry child = (CdsEmbmsSessionEntry) mChildData.get(groupPosition);

        CheckBox chkBox = (CheckBox) convertView.findViewById(R.id.embmsSessionStatus);
        chkBox.setFocusable(false);
        chkBox.setOnClickListener(this);
        chkBox.setChecked(child.getIsEnabled());
        chkBox.setEnabled(isExpanded);

        Button btn = (Button) convertView.findViewById(R.id.embmsSessionActivate);
        btn.setFocusable(false);
        btn.setOnClickListener(this);
        btn.setEnabled(isExpanded);

        btn = (Button) convertView.findViewById(R.id.embmsSessionDeactivate);
        btn.setFocusable(false);
        btn.setOnClickListener(this);
        btn.setEnabled(isExpanded);

        btn = (Button) convertView.findViewById(R.id.embmsSessionSave);
        btn.setFocusable(false);
        btn.setOnClickListener(this);
        btn.setEnabled(isExpanded);

        TextView txtView = (TextView) convertView.findViewById(R.id.embmsSession);
        txtView.setText(groupName);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch(id) {
            case R.id.embmsSessionActivate:
                activateSession(true, mCurrnetChild);
                break;
            case R.id.embmsSessionDeactivate:
                activateSession(false, mCurrnetChild);
                break;
            case R.id.embmsSessionSave:
                saveEmbmsSessionInfo();
                break;
            case R.id.embmsSessionStatus:
                mCurrnetChild.setIsEnabled(((CheckBox) v).isChecked());
                mChildData.set(mCurrentGroupPos, mCurrnetChild);
                break;
            default:
                break;
        }
    }

    private void activateSession(boolean isActivated, CdsEmbmsSessionEntry child) {
        mService.activateEmbmsSession(isActivated, child.toCmdString());
    }

    private void saveEmbmsSessionInfo() {
        Log.i(TAG, "saveEmbmsSessionInfo:" + mCurrentGroupPos);
        CdsEmbmsSessionEntry child = mChildData.get(mCurrentGroupPos);

        if (!child.isValidated()) {
            Toast toast = Toast.makeText(mContext, mContext.getString(R.string.error1),
                            Toast.LENGTH_SHORT);
            toast.show();
            Log.e(TAG, "Empty TMGI");
            return;
        } else if (!isValidatedFormat(child.getSaiList())) {
            Toast toast = Toast.makeText(mContext, mContext.getString(R.string.error2),
                            Toast.LENGTH_SHORT);
            toast.show();
            Log.e(TAG, "SAL list is not well format:" + child.getSaiList());
            return;
        } else if (!isValidatedFormat(child.getFreqList())) {
            Toast toast = Toast.makeText(mContext, mContext.getString(R.string.error2),
                            Toast.LENGTH_SHORT);
            toast.show();
            Log.e(TAG, "SAL list is not well format:" + child.getFreqList());
            return;
        }

        SharedPreferences dataStore = mContext.getSharedPreferences(
                                        CdsEmbmsConsts.EMBMS_FILE, 0);
        SharedPreferences.Editor dataEditor = dataStore.edit();
        StringBuffer sessionInfo = new StringBuffer();
        sessionInfo.append(child.getIsEnabled()).append(CdsEmbmsConsts.SEP);
        sessionInfo.append(child.getTmgi()).append(CdsEmbmsConsts.SEP);
        sessionInfo.append(child.getSessionId()).append(CdsEmbmsConsts.SEP);
        sessionInfo.append(child.getSaiList()).append(CdsEmbmsConsts.SEP);
        sessionInfo.append(child.getFreqList()).append(CdsEmbmsConsts.SEP);
        String sessionName = CdsEmbmsConsts.EMBMS_SESSION_INFO + mCurrentGroupPos;
        Log.i(TAG, "saveEmbmsSessionInfo[2]:" + sessionInfo.toString());
        dataEditor.putString(sessionName, sessionInfo.toString());
        dataEditor.commit();
    }

    private boolean isValidatedFormat(String txtValue) {
        if (txtValue.length() > 0) {
            final Pattern pattern = Pattern.compile("^[0-9]*[,]?");
            final Matcher matcher = pattern.matcher(txtValue);
            //return matcher.matches();
        }
        return true;
    }

    /**
     *
     * @hiden
     */
    private class GenericTextWatcher implements TextWatcher {
        private View mView;

        private GenericTextWatcher(View view) {
            this.mView = view;
        }

        public void beforeTextChanged(CharSequence c, int start, int before, int count) {}
        public void onTextChanged(CharSequence s, int start, int count, int after) {}
        public void afterTextChanged(Editable c) {
            switch(mView.getId()) {
                case R.id.tmgi:
                    mCurrnetChild.setTmgi(c.toString());
                    break;
                case R.id.sessionid:
                    mCurrnetChild.setSessionId(c.toString());
                    break;
                case R.id.sailist:
                    mCurrnetChild.setSaiList(c.toString());
                    break;
                case R.id.freqlist:
                    mCurrnetChild.setFreqList(c.toString());
                    break;
                default:
                    break;
            }
            mChildData.set(mCurrentGroupPos, mCurrnetChild);
        }
    }

    public void setSrvBinder(CdsEmbmsService srv) {
        mService = srv;
    }

}