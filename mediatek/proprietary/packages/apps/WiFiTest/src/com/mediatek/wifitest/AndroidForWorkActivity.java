package com.mediatek.wifitest;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static junit.framework.Assert.assertTrue;

public class AndroidForWorkActivity extends Activity implements View.OnClickListener {
    private final String TAG = "AndroidForWorkActivity";
    private WifiManager mWifiManager;

    private static final int DURATION = 10000;
    // Thread
    private Handler mHandler = null;
    private HandlerThread  mHandlerThread = null;

    private static class MySync {
        int expectedState = STATE_NULL;
    }
    private static MySync mMySync = new MySync();
    private static final int STATE_NULL = 0;
    private static final int STATE_WIFI_CHANGING = 1;
    private static final int STATE_WIFI_ENABLED = 2;
    private static final int STATE_WIFI_DISABLED = 3;
    private static final int STATE_SCANNING = 4;
    private static final int STATE_SCAN_RESULTS_AVAILABLE = 5;
    private static final int TIMEOUT_MSEC = 6000;
    private static final int WAIT_MSEC = 60;

    View connectBtn ;
    View modifyBtn ;
    View removeBtn ;
    TextView netIdTxt;
    TextView statusTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_for_work);

        mWifiManager = (WifiManager) this.getApplicationContext()
            .getSystemService(Context.WIFI_SERVICE);
        if (!mWifiManager.isWifiEnabled()) {
            setWifiEnabled(true);
            sleep();
        }
        assertTrue(mWifiManager.isWifiEnabled());

        connectBtn = findViewById(R.id.button_connect);
        modifyBtn = findViewById(R.id.button_modify);
        removeBtn = findViewById(R.id.button_remove);

        connectBtn.setOnClickListener(this);
        modifyBtn.setOnClickListener(this);
        removeBtn.setOnClickListener(this);

        netIdTxt = (TextView) findViewById(R.id.text_netId);
        statusTxt = (TextView) findViewById(R.id.textView_status);
        statusTxt.setMovementMethod(new ScrollingMovementMethod());

    }

    @Override
    public void onClick(View v) {
        EditText ssidText = (EditText) findViewById(R.id.editText);
        EditText pskText = (EditText) findViewById(R.id.editText2);
        EditText modifyTargetSsidText = (EditText) findViewById(R.id.editText_modifyTargetSsid);
        EditText modifyPskText = (EditText) findViewById(R.id.editText_modifyPsk);
        EditText removeNetId = (EditText) findViewById(R.id.editText_removeSsid);
        switch(v.getId()) {
            case R.id.button_connect:
                log(TAG, "connect start");
                conncetNetwork(ssidText.getText().toString(), pskText.getText().toString());
                log(TAG, "connect end");
                break;
            case R.id.button_modify:
                log(TAG, "modify start");
                modifyNetwork_psk(modifyTargetSsidText.getText().toString(),
                        modifyPskText.getText().toString());
                log(TAG, "modify end");
                break;
            case R.id.button_remove:
                log(TAG, "remove start");
                removeNetwork(removeNetId.getText().toString());
                log(TAG, "remove end");
                break;
            default:
                log(TAG, "default");
        }
    }
    void recordStatus(String statusStr) {
        log(TAG, statusStr);
        statusTxt.append("\n" + getReadableTime() + " " + statusStr);
    }
    String getReadableTime() {
        long cur = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(new Date(cur));
    }
    void log(String tag, String txt) {
        System.out.println(tag + ": " + txt);
    }
    private void conncetNetwork(String ssid, String psk) {

        WifiConfiguration config = new WifiConfiguration();
        config.SSID = ssid;
        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        config.preSharedKey = '"' + psk + '"';

        int oldNetId = getNetIdFromSsid(ssid);
        if (oldNetId != -1) {
            recordStatus("ssid " + config.SSID + " already exist, netId: "
                + oldNetId + ", enable this network");
            if (mWifiManager.enableNetwork(oldNetId, true)) {
                recordStatus("Enable network success " + config.SSID + ", netId: " + oldNetId);
            } else {
                recordStatus("Enable network fail " + config.SSID + ", netId: " + oldNetId);
            }
        } else {
            int netId = mWifiManager.addNetwork(config);
            if (netId != -1) {
                recordStatus("Add network success " + config.SSID + ", netId: " + netId);
                mWifiManager.enableNetwork(netId, true);
            } else {
                recordStatus("Unable to add network " + config.SSID + ", netId: " + netId);
            }
            netIdTxt.setText(Integer.toString(netId));
        }
    }
    private void modifyNetwork_psk(String ssid, String psk) {
        int netId = getNetIdFromSsid(ssid);
        WifiConfiguration config = new WifiConfiguration();
        config.networkId = netId;
        config.preSharedKey = '"' + psk + '"';

        netId = mWifiManager.updateNetwork(config);
        if (netId != -1) {
            recordStatus("Modify network success " + ssid + ", netId: " + netId);
            Toast.makeText(this, "Modify Success", Toast.LENGTH_LONG).show();
        } else {
            recordStatus("Unable to modify network " + ssid + ", netId: " + netId);
            Toast.makeText(this, "Modify Fail", Toast.LENGTH_LONG).show();
        }
    }
    private void removeNetwork(String ssid) {
        int netId = getNetIdFromSsid(ssid);
        if (mWifiManager.removeNetwork(netId)) {
            recordStatus("Remove network success " + ssid + ", netId: " + netId);
            Toast.makeText(this, "Remove Success", Toast.LENGTH_LONG).show();
        } else {
            recordStatus("Unable to remove network " + ssid + ", netId: " + netId);
            Toast.makeText(this, "Remove Fail", Toast.LENGTH_LONG).show();
        }
    }
    private int getNetIdFromSsid(String ssid) {
        ArrayList<WifiConfiguration> list = new ArrayList<>(mWifiManager.getConfiguredNetworks());
        for (WifiConfiguration config:list) {
            if (config.SSID.equals('\"' + ssid + '\"')) {
                log(TAG, "found SSID: " + ssid);
                return config.networkId;
            }
        }
        return -1;
    }
    private void sleep() {
        mHandlerThread = new HandlerThread("sleep");
        mHandlerThread.start();

        mHandler = new Handler(mHandlerThread.getLooper());
        mHandler.post(mSleepRunnable);
    }

    private Runnable mSleepRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                Thread.sleep(DURATION);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };
    private void setWifiEnabled(boolean enable) {
        synchronized (mMySync) {
            assertTrue(mWifiManager.setWifiEnabled(enable));
            if (mWifiManager.isWifiEnabled() != enable) {
                mMySync.expectedState = STATE_WIFI_CHANGING;
                long timeout = System.currentTimeMillis() + TIMEOUT_MSEC;
                int expectedState = (enable ? STATE_WIFI_ENABLED : STATE_WIFI_DISABLED);
                while (System.currentTimeMillis() < timeout
                        && mMySync.expectedState != expectedState) {
                    try {
                        mMySync.wait(WAIT_MSEC);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
