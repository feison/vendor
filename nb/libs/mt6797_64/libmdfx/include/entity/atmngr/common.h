/*
    This is an internal header file. 
    Here are common definitions for AT Manager and ATI. 
*/

#ifndef __ENTITY_ATMNGR_COMMON_H__
#define __ENTITY_ATMNGR_COMMON_H__

#include "context/task.h"
#include "event.h"
#include "context.h"
#include "ctnr/chain.h"
// ==== Legacy ====
#include "entity.h"
// ==== Legacy ====

// Compiler flags, NEED_TO_BE_NOTICED, set by the compiler
// N/A

// Type definitions
// => AT Manager
typedef struct atmngr_subscribe_req atmngr_subscribe_req_t;
typedef struct atmngr_subscribe_req* atmngr_subscribe_req_ptr_t;
typedef struct atmngr_act_node atmngr_act_node_t;
typedef struct atmngr_act_node* atmngr_act_node_ptr_t;
typedef struct atmngr_act_tbl_entry atmngr_act_tbl_entry_t;
typedef struct atmngr_act_tbl_entry* atmngr_act_tbl_entry_ptr_t;
typedef void (*atmngr_act_fp_t) (const char *atc, void *arg);
// => ATI
// N/A

// Macros
#define ATMNGR_PREFIX_SIZE    (32)
#define ATMNGR_URC_HDR  ("\r\n")
#define ATMNGR_URC_TRL   ("\r\n")
#define ATMNGR_URC_DELIM   (":")
// => AT Manager
// N/A
// => ATI
#define ATI_AT_PATH_1   ("/dev/radio/malcmd1")
#define ATI_AT_PATH_2   ("/dev/radio/malcmd2")
#define ATI_AT_PATH_3   ("/dev/radio/malcmd3")
#define ATI_AT_PATH_4   ("/dev/radio/malcmd4")
#define ATI_URC_PATH    ("/dev/radio/malurc")

// Functions
// => AT Manager
extern void* atmngr_urc_hdl (task_ptr_t task_ptr, event_ptr_t event_ptr, void *arg);
// => ATI
extern void* ati_urc_conn_hdl (void* arg);

// Implementation
// => AT Manager
struct atmngr_subscribe_req
{
	char prefix[ATMNGR_PREFIX_SIZE];
    context_id_t context_id;
    event_id_t event_id;
    bool does_intercept;
};

struct atmngr_act_node
{
    context_id_t context_id;
    event_id_t event_id;
    bool does_intercept;
    atmngr_act_fp_t act_fp;
};

struct atmngr_act_tbl_entry
{
    char prefix[ATMNGR_PREFIX_SIZE]; // NEED_TO_BE_NOTICED, NOT necessary
    bool does_intercept;
    size_t intercept_cnt;  // NEED_TO_BE_NOTICED, a variable-length variable
    chain_t act_list;
};
// => ATI
// N/A

// ==== Legacy ====
typedef struct ati_entity_priv atmngr_priv_t;
typedef struct ati_entity_priv* atmngr_priv_ptr_t;

struct ati_entity_priv
{
    thrd_lock_t mutex;
    entity_ptr_t entity_ptr;
// ==== Legacy ====
    task_t atc_task;
    bool is_bypass;
// ==== Legacy ====
    map_t atc_act_tbl;
    char *prefix_buf;
    size_t prefix_buf_size;
    // URC
    task_t urc_hdl;
    thrd_t urc_conn;
    mailbox_addr_t urc_conn_mailbox_addr;
    map_t urc_act_tbl;

/*
    task_t dati_task;
    atc_hdl_arg_t atc_hdl_arg;        // NEED_TO_BE_NOTICED, the following should be separated to an individaul structure of Data Manager
*/
};
// ==== Legacy ====

#endif
