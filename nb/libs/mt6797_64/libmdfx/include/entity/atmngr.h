/*
    Here are APIs for AT Manager. 
*/

#ifndef __ENTITY_ATMNGR_H__
#define __ENTITY_ATMNGR_H__

#include "context.h"
#include "event.h"

// Compiler flags, NEED_TO_BE_NOTICED, set by the compiler
// N/A

// Type definitions
// N/A

// Macros
// => AT Manager
#define ATMNGR_OP_SUBSCRIBE_URC (1)
#define ATMNGR_OP_UNSUBSCRIBE_URC (2)
#define ATMNGR_OP_INTERCEPT_URC (4)

// Functions
extern int mdfx_atmngr_subscribe_urc (context_ptr_t context_ptr, const char *prefix, event_id_t event_id, int op);

// Implementation
// N/A

#endif
